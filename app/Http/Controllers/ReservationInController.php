<?php

namespace App\Http\Controllers;

use App\Models\ReservationIn;
use App\Http\Requests\StoreReservationInRequest;
use App\Http\Requests\UpdateReservationInRequest;

class ReservationInController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(StoreReservationInRequest $request)
    {
        //
    }

    /**
     * Display the specified resource.
     */
    public function show(ReservationIn $reservationIn)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(ReservationIn $reservationIn)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(UpdateReservationInRequest $request, ReservationIn $reservationIn)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(ReservationIn $reservationIn)
    {
        //
    }
}

<?php

namespace App\Http\Controllers;

use App\Models\User;
use App\Models\WishList;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class WishListController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $account = User::find(Auth::id());
        if ($account) {
            $wishList = $account->wishlist;
            if (!$wishList) {
                return response()->json([
                    'Data' => null,
                ]);
            }
            $services = $wishList->services()->with('images')->paginate(10);
            if ($services->isEmpty()) {
                return response()->json([
                    'Data' => null,
                ]);
            }
            $list = $services->map(function ($service) {
                return [
                    'id' => $service->id,
                    'serviceable_type' => class_basename($service->serviceable_type),
                    'first_image' => $service->images->first()->path ?? null,
                ];
            });
            $paginationData = [
                'total_pages' => $services->lastPage(),
                'current_page' => $services->currentPage(),
                'next_page_url' => $services->nextPageUrl(),
                'prev_page_url' => $services->previousPageUrl(),
            ];
            $data = array_merge($list->toArray(), $paginationData);
            return response()->json([
                'Data' => $data,
            ]);
        }
        return response()->json([
            'Data' => null,
        ]);

    }


    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store($service_id)
    {
        $account = User::find(Auth::id());
        if (!$account->wishlist) {
            $wishlist = new Wishlist();
            $account->wishlist()->save($wishlist);
        } else {
            $wishlist = $account->wishlist;
        }
        $wishlistService = $wishlist->services()->wherePivot('service_id', $service_id)->first();
        if($wishlistService){
            $massage="Service Deleted From Wishlist Successfully";
        }else{
            $massage="Service Added To Wishlist Successfully";
        }
        $wishlist->services()->toggle($service_id);
        return response()->json([
            'Data' => $wishlist,
            $massage
        ]);
    }


    /**
     * Display the specified resource.
     */
    public function show(WishList $wishList)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(WishList $wishList)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, WishList $wishList)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(WishList $wishList)
    {
        //
    }
}

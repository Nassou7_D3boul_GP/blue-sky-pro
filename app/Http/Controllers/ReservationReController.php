<?php

namespace App\Http\Controllers;

use App\Models\ReservationRe;
use App\Http\Requests\StoreReservationReRequest;
use App\Http\Requests\UpdateReservationReRequest;

class ReservationReController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(StoreReservationReRequest $request)
    {
        //
    }

    /**
     * Display the specified resource.
     */
    public function show(ReservationRe $reservationRe)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(ReservationRe $reservationRe)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(UpdateReservationReRequest $request, ReservationRe $reservationRe)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(ReservationRe $reservationRe)
    {
        //
    }
}

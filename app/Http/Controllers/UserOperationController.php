<?php

namespace App\Http\Controllers;

use App\Models\UserOperation;
use Illuminate\Http\Request;

class UserOperationController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     */
    public function show(UserOperation $userOperation)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(UserOperation $userOperation)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, UserOperation $userOperation)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(UserOperation $userOperation)
    {
        //
    }
}

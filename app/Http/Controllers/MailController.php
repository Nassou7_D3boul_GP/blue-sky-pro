<?php

namespace App\Http\Controllers;

use App\Mail\SendMail;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Symfony\Component\HttpFoundation\Response;

class MailController extends Controller
{
    public function index(Request $request)
    {
        $request->validate([
            'email' => 'required|email|unique:users'
        ]);
        $mailData = [
            'code' => $code = random_int(10000, 99999)
        ];
        Mail::to($request->email)->send(new SendMail($mailData));
        return response()->json([
            'Message' => 'The email has been sent successfully ',
            'code' => $code,
            'Code' => Response::HTTP_OK,
        ]);
    }

}

<?php

namespace App\Http\Controllers;

use App\Models\Ad;
use Illuminate\Http\Request;

class AdController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $pagination = Ad::query()
            ->select('id', 'Title', 'Description', 'Link')
            ->whereHas('image', function ($query) {
                $query->where('imageable_type', 'App\Models\Ad');
            })
            ->paginate(10);

        $ads = $pagination->getCollection()->map(function ($item) {
            $item->imagePath = $item->image->Path;
            unset($item->image);
            return $item;
        });
        $paginationData = [
            'total_pages' => $pagination->lastPage(),
            'current_page' => $pagination->currentPage(),
            'next_page_url' => $pagination->nextPageUrl(),
            'prev_page_url' => $pagination->previousPageUrl(),
        ];
        $data = array_merge($ads->toArray(), $paginationData);
        return response()->json([
            'Data' => $data,
        ]);
    }


    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     */
    public function show($ad_id)
    {
        $ad = Ad::find($ad_id);
        if (isset($ad) == 1) {
            $image = $ad->image->first()->Path;
            $ad->makeHidden(['created_at', 'updated_at', 'image']);
            $list = [
                'ad' => $ad,
                'imagePath' => $image,
            ];
            return response()->json([
                'Data' => $list,
            ]);
        }
        return response()->json([
            'Data' => null,
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(Ad $ad)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, Ad $ad)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Ad $ad)
    {
        //
    }
}

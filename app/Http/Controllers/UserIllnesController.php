<?php

namespace App\Http\Controllers;

use App\Models\UserIllnes;
use Illuminate\Http\Request;

class UserIllnesController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     */
    public function show(UserIllnes $userIllnes)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(UserIllnes $userIllnes)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, UserIllnes $userIllnes)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(UserIllnes $userIllnes)
    {
        //
    }
}

<?php

namespace App\Http\Controllers;

use App\Models\Illnes;
use Illuminate\Http\Request;

class IllnesController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     */
    public function show(Illnes $illnes)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(Illnes $illnes)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, Illnes $illnes)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Illnes $illnes)
    {
        //
    }
}

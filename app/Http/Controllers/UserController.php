<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Spatie\Permission\Models\Role;

class UserController extends Controller
{
    public function index(Request $request)
    {
        $data = User::latest()->paginate(5);
        $roles = Role::pluck('name', 'name')->all();

        return view('users.index')->with(['data' => $data, 'roles' => $roles]);
    }

    public function create()
    {
        $roles = Role::pluck('name', 'name')->all();
        return view('users.create', compact('roles'));
    }

    public function store(Request $request)
    {
        $request->validate([
            'FirstName' => 'required',
            'LastName' => 'required',
            'email' => 'required|email|unique:users,email',
            'password' => 'required|same:confirm-password',
            'roles' => 'required'
        ]);

        $input = $request->all();
        $input['password'] = Hash::make($input['password']);

        $user = User::create($input);
        $user->assignRole($request->input('roles'));

        return redirect()->route('users.index')
            ->with('success', 'User created successfully');
    }

    public function show($id)
    {
        $user = User::find($id);
        return view('users.show', compact('user'));
    }

    public function edit($id)
    {
        $user = User::find($id);
        $roles = Role::pluck('name', 'name')->all();
        $userRole = $user->roles->pluck('name', 'name')->all();

        return view('users.edit', compact('user', 'roles', 'userRole'));
    }

    public function update(Request $request, $id)
    {
//        $this->validate($request, [
//            'name' => 'required',
//            'email' => 'required|email|unique:users,email,'.$id,
//            'password' => 'same:confirm-password',
//            'roles' => 'required'
//        ]);

        $input = $request->all();
        if (!empty($input['password'])) {
            $input['password'] = Hash::make($input['password']);
        } else {
            $input = Arr::except($input, array('password'));
        }

        $user = User::find($id);
        $user->update($input);
        DB::table('model_has_roles')->where('model_id', $id)->delete();

        $user->assignRole($request->input('roles'));

        return redirect()->route('users.index')
            ->with('success', 'User updated successfully');
    }

    public function destroy($id)
    {
        User::find($id)->delete();
        return redirect()->route('users.index')
            ->with('success', 'User deleted successfully');
    }

    public function UserBlock(Request $request)
    {

        $user = User::find($request->user_id);

        if (isset($user) == 1) {

            $user->update([
                'Blocked' => $request->blocked
            ]);

            if ($request->blocked == true) {
                $message = 'User has been blocked successfully';
            } else
                $message = 'User has been unblocked successfully';


            return response()->json([
                'Status' => 'Success',
                'Message' => $message,
                'account' => $user
            ]);
        }

        return response()->json([
            'Status' => 'Failed',
            'Message' => 'User not found',
        ]);

    }

    public function search(Request $request)
    {
        $search = $request->get('search');

        $blocked = $search == "blocked" ? 1 : ($search == "active" ? 0 : null);

        $users = User::where(function ($query) use ($search, $blocked) {
            $query->where('FirstName', 'like', '%'.$search.'%')
                ->orWhere('LastName', 'like', '%'.$search.'%')
                ->orWhereRaw("CONCAT(FirstName, ' ', LastName) like '%$search%'")
                ->orWhere('email', 'like', '%'.$search.'%')
                ->orWhere('Phone', 'like', '%'.$search.'%');
            if ($blocked !== null) {
                $query->orWhere('Blocked', $blocked);
            }
        })
            ->get();

        return response()->json($users);
    }
}

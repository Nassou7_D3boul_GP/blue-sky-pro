<?php

namespace App\Http\Controllers;

use App\Models\ReservationTaxi;
use App\Http\Requests\StoreReservationTaxiRequest;
use App\Http\Requests\UpdateReservationTaxiRequest;

class ReservationTaxiController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(StoreReservationTaxiRequest $request)
    {
        //
    }

    /**
     * Display the specified resource.
     */
    public function show(ReservationTaxi $reservationTaxi)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(ReservationTaxi $reservationTaxi)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(UpdateReservationTaxiRequest $request, ReservationTaxi $reservationTaxi)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(ReservationTaxi $reservationTaxi)
    {
        //
    }
}

<?php

namespace App\Http\Controllers;

use App\Models\Notification;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class NotificationController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $account = User::where('id',Auth::id())->first();
        if (!$account) {
            return response()->json([
                'Data' => 'User not Found',
            ]);
        }
        $notifications = $account->notifications;
        $notifications->makeHidden(['created_at', 'updated_at','user_id']);
        $list = $notifications->map(function ($item) {
            $item->recived_at = $item->created_at->diffForHumans();
            return $item;
        });
        return response()->json([
            'Data' =>$list,
        ]);
    }
    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     */
    public function show(Notification $notification)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(Notification $notification)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, Notification $notification)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Notification $notification)
    {
        //
    }
}

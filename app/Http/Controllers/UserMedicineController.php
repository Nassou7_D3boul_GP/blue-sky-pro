<?php

namespace App\Http\Controllers;

use App\Models\UserMedicine;
use Illuminate\Http\Request;

class UserMedicineController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     */
    public function show(UserMedicine $userMedicine)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(UserMedicine $userMedicine)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, UserMedicine $userMedicine)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(UserMedicine $userMedicine)
    {
        //
    }
}

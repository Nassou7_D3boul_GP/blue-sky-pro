<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Taxi extends Model
{
    use HasFactory;

    protected $table = 'taxis';
    protected $primaryKey = 'id';
    public $timestamps = false;
    protected $fillable = [
        'Name',
        'EcoVip',
        'CarNumber',
        'Color',
        'Phone',
        'Type',
    ];

    public function reservition()
    {
        return $this->hasMany(ReservationTaxi::class, 'taxi_id');
    }

    public function service()
    {
        return $this->morphOne(Service::class, 'serviceable', 'serviceable_type', 'serviceable_id');
    }
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Day extends Model
{
    use HasFactory;

    protected $table = 'days';
    protected $primaryKey = 'id';
    public $timestamps = false;
    protected $fillable = [
        'restaurant_id',
        'Name',
    ];

    public function times()
    {
        return $this->hasMany(Time::class, 'day_id');
    }

    public function restaurant()
    {
        return $this->belongsTo(Restaurant::class, 'restaurant_id');
    }

}

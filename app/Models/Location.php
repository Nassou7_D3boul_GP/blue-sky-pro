<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Location extends Model
{
    use HasFactory;

    protected $table = 'locations';
    protected $primaryKey = 'id';
    public $timestamps = false;
    protected $fillable = [
        'service_id',
        'Latitude',
        'Longitude'
    ];

    public function service()
    {
        return $this->belongsTo(Service::class, 'service_id');
    }
}

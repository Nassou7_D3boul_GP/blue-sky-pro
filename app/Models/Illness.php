<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Illness extends Model
{
    use HasFactory;

    protected $table = 'illnesses';
    protected $primaryKey = 'id';
    public $timestamps = false;
    protected $fillable = ['Name'];

    public function users()
    {
        return $this->belongsToMany(User::class, 'user_illness', 'illnesses_id', 'user_id');
    }
}

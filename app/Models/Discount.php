<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Discount extends Model
{
    use HasFactory;

    protected $table = 'discounts';
    protected $primaryKey = 'id';
    public $timestamps = false;
    protected $fillable = [
        'service_id',
        'Status',
        'Value'
    ];

    public function service()
    {
        return $this->belongsTo(Service::class, 'service_id');
    }
}

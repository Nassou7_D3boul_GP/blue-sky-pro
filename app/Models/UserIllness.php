<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class UserIllness extends Model
{
    use HasFactory;

    protected $table = 'user_illness';
    protected $primaryKey = 'id';
    public $timestamps = false;
    protected $fillable = [
        'user_id',
        'illness_id'
    ];

//    public function illness()
//    {
//        return $this->belongsTo(Illness::class, 'illness_id');
//    }
//
//    public function user()
//    {
//        return $this->belongsTo(User::class, 'user_id');
//    }

}

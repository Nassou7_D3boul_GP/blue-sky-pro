<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Restaurant extends Model
{
    use HasFactory;

    protected $table = 'restaurants';
    protected $primaryKey = 'id';
    public $timestamps = false;
    protected $fillable = [
        'ClientMax',
        'NumTable',
    ];

    public function menus()
    {
        return $this->hasMany(Menu::class, 'restaurant_id');
    }

    public function days()
    {
        return $this->hasMany(Day::class, 'restaurant_id');
    }

    public function reservation()
    {
        return $this->hasMany(ReservationRe::class, 'restaurant_id');
    }

    public function service()
    {
        return $this->morphOne(Service::class, 'serviceable', 'serviceable_type', 'serviceable_id');
    }
}

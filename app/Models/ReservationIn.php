<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ReservationIn extends Model
{
    use HasFactory;


    protected $table = 'reservation_ins';
    protected $primaryKey = 'id';
    public $timestamps = false;
    protected $fillable = [
        'insurance_id',
        'user_id',
        'CancelAt',
    ];

    public function insurance()
    {
        return $this->belongsTo(Insurance::class, 'insurance_id');
    }

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

}

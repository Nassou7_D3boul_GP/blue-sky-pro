<?php

namespace App\Models;

// use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Spatie\Permission\Traits\HasRoles;
use PHPOpenSourceSaver\JWTAuth\Contracts\JWTSubject;

class User extends Authenticatable implements JWTSubject
{
    use HasFactory, Notifiable, HasRoles;

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'FirstName',
        'LastName',
        'email',
        'Phone',
        'Birthdate',
        'Country',
        'Address',
        'Blocked',
        'password',
    ];

    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array<int, string>
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * Get the attributes that should be cast.
     *
     * @return array<string, string>
     */
    protected function casts(): array
    {
        return [
            'email_verified_at' => 'datetime',
            'password' => 'hashed',
        ];
    }

    public function bankCard()
    {
        return $this->hasOne(BankCard::class, 'user_id');
    }

    public function wishlist()
    {
        return $this->hasOne(WishList::class, 'user_id');
    }

    public function image()
    {
        return $this->morphOne(Image::class, 'imageable', 'imageable_type', 'imageable_id');
    }

    public function notifications()
    {
        return $this->hasMany(Notification::class, 'user_id');
    }

    public function reservationHotels()
    {
        return $this->hasMany(ReservationHotel::class, 'user_id');
    }

    public function reservationIns()
    {
        return $this->hasMany(ReservationIn::class, 'user_id');
    }

    public function reviews()
    {
        return $this->hasMany(Review::class, 'user_id');
    }

    public function reservationTaxis()
    {
        return $this->hasMany(ReservationTaxi::class, 'user_id');
    }

    public function reservationRes()
    {
        return $this->hasMany(ReservationRe::class, 'user_id');
    }

    public function illnesses()
    {
        return $this->belongsToMany(Illness::class, 'user_illness', 'user_id', 'illnesses_id');
    }

    public function operations()
    {
        return $this->belongsToMany(Operation::class, 'user_operations', 'user_id', 'operation_id');
    }

    public function medicines()
    {
        return $this->belongsToMany(Medicine::class, 'user_medicines', 'user_id', 'medicine_id');
    }

    public function getJWTIdentifier()
    {
        return $this->getKey();
    }

    public function getJWTCustomClaims()
    {
        return [];
    }
}

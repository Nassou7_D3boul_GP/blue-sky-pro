<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Time extends Model
{
    use HasFactory;

    protected $table = 'times';
    protected $primaryKey = 'id';
    public $timestamps = false;
    protected $fillable = [
        'day_id',
        'From',
        'To',
    ];

    public function day()
    {
        return $this->belongsTo(Day::class, 'day_id');
    }
}

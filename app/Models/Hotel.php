<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Hotel extends Model
{
    use HasFactory;


    protected $table = 'hotels';
    protected $primaryKey = 'id';
    public $timestamps = false;
    protected $fillable = [
        'NumRoom',
    ];

    public function options()
    {
        return $this->hasMany(Option::class, 'hotel_id');
    }

    public function hotelRooms()
    {
        return $this->hasMany(HotelRoom::class, 'hotel_id');
    }

    public function reservation()
    {
        return $this->hasMany(ReservationHotel::class, 'hotel_id');
    }

    public function service()
    {
        return $this->morphOne(Service::class, 'serviceable', 'serviceable_type', 'serviceable_id');
    }

}

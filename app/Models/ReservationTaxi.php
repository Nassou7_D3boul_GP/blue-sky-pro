<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ReservationTaxi extends Model
{
    use HasFactory;


    protected $table = 'reservation_taxis';
    protected $primaryKey = 'id';
    public $timestamps = false;
    protected $fillable = [
        'taxi_id',
        'user_id',
        'Time',
        'Distance',
        'Cost',
        'StartPoint',
        'EndPoint',
        'CancelAt',
    ];

    public function taxi()
    {
        return $this->belongsTo(Taxi::class, 'taxi_id');
    }

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }
}

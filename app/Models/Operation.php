<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Operation extends Model
{
    use HasFactory;

    protected $table = 'operations';
    protected $primaryKey = 'id';
    public $timestamps = false;
    protected $fillable = [
        'Name'
    ];

    public function users()
    {
        return $this->belongsToMany(User::class, 'user_operations', 'operation_id', 'user_id');
    }
}

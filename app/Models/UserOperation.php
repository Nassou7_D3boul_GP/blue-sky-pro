<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class UserOperation extends Model
{
    use HasFactory;

    protected $table = 'user_operations';
    protected $primaryKey = 'id';
    public $timestamps = false;
    protected $fillable = [
        'user_id',
        'operation_id'
    ];

//    public function operation()
//    {
//        return $this->belongsTo(Operation::class, 'operation_id');
//    }
//
//    public function user()
//    {
//        return $this->belongsTo(User::class, 'user_id');
//    }
}

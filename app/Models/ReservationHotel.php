<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ReservationHotel extends Model
{
    use HasFactory;

    protected $table = 'reservation_hotels';
    protected $primaryKey = 'id';
    public $timestamps = false;
    protected $fillable =
        [
            'CheckIn',
            'CheckOut',
            'Status',
            'DiscountValue',
            'CancelAt',
            'hotel_id',
            'user_id',
            'Price',
        ];

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    public function hotel()
    {
        return $this->belongsTo(Hotel::class, 'hotel_id');
    }
}

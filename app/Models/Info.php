<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Info extends Model
{
    use HasFactory;

    protected $table = 'infos';
    protected $primaryKey = 'id';
    public $timestamps = false;
    protected $fillable = [
        'service_id',
        'Status',
        'Name',
    ];

    public function service()
    {
        return $this->belongsTo(Service::class, 'service_id');
    }
}

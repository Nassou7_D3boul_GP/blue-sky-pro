<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Menu extends Model
{
    use HasFactory;


    protected $table = 'menus';
    protected $primaryKey = 'id';
    public $timestamps = false;
    protected $fillable = [
        'restaurant_id',
        'Description',
        'Price',
        'Name',
    ];

    public function restaurant()
    {
        return $this->belongsTo(Restaurant::class, 'restaurant_id');
    }
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ReservationRe extends Model
{
    use HasFactory;

    protected $table = 'reservation_res';
    protected $primaryKey = 'id';
    public $timestamps = false;
    protected $fillable = [
        'restaurant_id',
        'user_id',
        'SpecialRequest',
        'Time',
        'Date',
        'Status',
        'CancelAt',
        'ClientNumber',
    ];

    public function restaurant()
    {
        return $this->belongsTo(Restaurant::class, 'restaurant_id');
    }

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }
}

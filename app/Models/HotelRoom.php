<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class HotelRoom extends Model
{
    use HasFactory;

    protected $table = 'hotel_rooms';
    protected $primaryKey = 'id';
    public $timestamps = false;
    protected $fillable = [
        'hotel_rooms',
        'Description',
        'Name',
        'Price',
    ];

    public function hotel()
    {
        return $this->belongsTo(Hotel::class, 'hotel_id');
    }
}

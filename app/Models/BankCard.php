<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class BankCard extends Model
{
    use HasFactory;

    protected $table = 'bank_cards';
    protected $primaryKey = 'id';
    public $timestamps = false;
    protected $fillable = [
        'user_id',
        'Money',
        'CardNumber',
        'CVV',
        'ExpiredDate'
    ];

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }
}

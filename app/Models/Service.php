<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Service extends Model
{
    use HasFactory;

    protected $table = 'services';
    protected $primaryKey = 'id';
    public $timestamps = false;
    protected $fillable = [
        'Description',
        'Phone',
        'Telephone',
        'email',
        'Name',
        'Address',
        'serviceable_id',
        'serviceable_type',
    ];

    public function serviceable()
    {
        return $this->morphTo(__FUNCTION__, 'serviceable_type', 'serviceable_id');
    }

    public function discounts()
    {
        return $this->hasMany(Discount::class, 'service_id');
    }

    public function reviwes()
    {
        return $this->hasMany(Review::class, 'service_id');
    }

    public function infos()
    {
        return $this->hasMany(Info::class, 'service_id');
    }

    public function images()
    {
        return $this->morphMany(Image::class, 'imgable', 'imageable_type', 'imageable_id');
    }

    public function wish_lists()
    {
        return $this->belongsToMany(WishList::class, 'wishlist_service', 'service_id', 'wishList_id');
    }

    public function packages()
    {
        return $this->belongsToMany(Package::class, 'package_service', 'service_id', 'package_id');
    }

    public function location()
    {
        return $this->hasOne(Location::class, 'service_id');
    }

}

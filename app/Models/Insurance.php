<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Insurance extends Model
{
    use HasFactory;


    protected $table = 'insurances';
    protected $primaryKey = 'id';
    public $timestamps = false;
    protected $fillable = [
        'Duration',
        'Cost',
        'Description',
    ];

    public function service()
    {
        return $this->morphOne(Service::class, 'serviceable', 'serviceable_type', 'serviceable_id');
    }

    public function reservition()
    {
        return $this->hasMany(ReservationIn::class, 'insurance_id');
    }
}

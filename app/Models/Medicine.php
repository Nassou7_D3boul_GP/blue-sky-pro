<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Medicine extends Model
{
    use HasFactory;

    protected $table = 'medicines';
    protected $primaryKey = 'id';
    public $timestamps = false;
    protected $fillable = [
        'Name'
    ];

    public function users()
    {
        return $this->belongsToMany(User::class, 'user_medicines', 'medicine_id', 'user_id');
    }
}

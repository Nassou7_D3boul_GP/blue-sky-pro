<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class UserMedicine extends Model
{
    use HasFactory;

    protected $table = 'user_medicines';
    protected $primaryKey = 'id';
    public $timestamps = false;
    protected $fillable = [
        'user_id',
        'medicine_id'
    ];
//    public function medicine()
//    {
//        return $this->belongsTo(Medicine::class, 'medicine_id');
//    }
//
//    public function user()
//    {
//        return $this->belongsTo(User::class, 'user_id');
//    }
}

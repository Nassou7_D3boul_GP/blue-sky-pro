<?php

namespace App\Traits;

trait Response
{
    protected function successWithoutData($message): \Illuminate\Http\JsonResponse
    {
        return response()->json([
            "success" => true,
            "code" => ResponseCode::SUCCESS,
            "message" => $message
        ]);
    }

    protected function successWithData($message, $data): \Illuminate\Http\JsonResponse
    {
        return response()->json([
            "success" => true,
            "code" => ResponseCode::SUCCESS,
            "data" => $data,
            "message" => $message,
        ]);
    }

    protected function errorResponse($message, $code = ResponseCode::GENERAL_ERROR): \Illuminate\Http\JsonResponse
    {
        return response()->json([
            "success" => false,
            "code" => $code,
            "message" => $message
        ], 422);
    }
}

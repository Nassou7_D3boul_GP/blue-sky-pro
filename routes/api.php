<?php

use App\Http\Controllers\AdController;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\MailController;
use App\Http\Controllers\NotificationController;
use App\Http\Controllers\WishListController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

Route::get('/user', function (Request $request) {
    return $request->user();
})->middleware('auth:sanctum');

Route::post('/sendCode', [MailController::class, 'index']);

Route::controller(AuthController::class)->group(function () {
    Route::post('login', 'login');
    Route::post('register', 'register');
    Route::post('logout', 'logout');
});
Route::group(['middleware' => ['auth:api']], function () {
    Route::prefix('ads/')->group(function () {
        Route::get('/Ads', [AdController::class, 'index']);
        Route::get('/Ads/{AdId}', [AdController::class, 'show']);
    });
    Route::prefix('notification/')->group(function () {
        Route::get('/notifications', [NotificationController::class, 'index']);
    });
    Route::group(['prefix' => 'wish_list'], function () {
        Route::get('wishlistAdd/{id}', [WishListController::class, 'store']);
        Route::get('wishlists', [WishListController::class, 'index']);
    });
});

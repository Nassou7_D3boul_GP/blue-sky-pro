
@extends('layouts.appDash')


@section('content')
<div class="row">
    <div class="col-lg-12 margin-tb mb-4">
        <div class="pull-left">
{{--            <h2>Show User</h2>--}}
            <div class="float-end">
                <a class="btn bg-danger-subtle text-danger" href="{{ route('users.index') }}"> Back</a>
            </div>
        </div>
    </div>
</div>

    <div class="card">
        <div class="card-header text-bg-primary">
            <h5 class="mb-0 text-white">View User</h5>
        </div>
        <form class="form-horizontal">
            <div class="form-body">
                <div class="card-body">
                    <h5 class="card-title mb-0">Personal Info</h5>
                </div>
                <hr class="m-0">
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group row">
                                <label class="form-label text-end col-md-3">First Name:</label>
                                <div class="col-md-9">
                                    <p class="form-control-static">{{$user->FirstName}}</p>
                                </div>
                            </div>
                        </div>
                        <!--/span-->
                        <div class="col-md-6">
                            <div class="form-group row">
                                <label class="form-label text-end col-md-3">Last Name:</label>
                                <div class="col-md-9">
                                    <p class="form-control-static">{{$user->LastName}}</p>
                                </div>
                            </div>
                        </div>
                        <!--/span-->
                    </div>
                    <!--/row-->
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group row">
                                <label class="form-label text-end col-md-3">Status:</label>
                                <div class="col-md-9">
                                    @if($user->Blocked == 0)
                                        <span class="badge bg-success-subtle text-success">Active</span>
                                    @else
                                        <span class="badge bg-danger-subtle text-danger">Blocked</span>
                                    @endif
                                </div>
                            </div>
                        </div>
                        <!--/span-->
                        <div class="col-md-6">
                            <div class="form-group row">
                                <label class="form-label text-end col-md-3">Date of Birth:</label>
                                <div class="col-md-9">
                                    <p class="form-control-static">{{$user->Birthdate}}</p>
                                </div>
                            </div>
                        </div>
                        <!--/span-->
                    </div>
                    <!--/row-->
                    <div class="row">
                        <!--/span-->
                        <div class="col-md-6">
                            <div class="form-group row">
                                <label class="form-label text-end col-md-3">Roles:</label>
                                <div class="col-md-9">
                                    @if(!empty($user->getRoleNames()))
                                        @foreach($user->getRoleNames() as $v)
                                            <p class="form-control-static">{{ $v }}</p>
                                        @endforeach
                                    @endif
                                </div>
                            </div>
                        </div>
                        <!--/span-->
                        <div class="col-md-6">
                            <div class="form-group row">
                                <label class="form-label text-end col-md-3">Date of Join:</label>
                                <div class="col-md-9">
                                    <p class="form-control-static">{{$user->created_at}}</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!--/row-->

                </div>
                <hr class="m-0">
                <div class="card-body">
                    <h5 class="card-title mb-0">Address</h5>
                </div>
                <hr class="m-0">
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group row">
                                <label class="form-label text-end col-md-3">Address:</label>
                                <div class="col-md-9">
                                    <p class="form-control-static">
                                        {{$user->Address}}
                                    </p>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group row">
                                <label class="form-label text-end col-md-3">Email:</label>
                                <div class="col-md-9">
                                    <p class="form-control-static">{{$user->email}}</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <!--/span-->
                        <div class="col-md-6">
                            <div class="form-group row">
                                <label class="form-label text-end col-md-3">Country:</label>
                                <div class="col-md-9">
                                    <p class="form-control-static">{{$user->Country}}</p>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group row">
                                <label class="form-label text-end col-md-3">Phone:</label>
                                <div class="col-md-9">
                                    <p class="form-control-static">{{$user->Phone}}</p>
                                </div>
                            </div>
                        </div>
                        <!--/span-->



                        <!--/span-->
                    </div>
                    <!--/row-->
                    <div class="row">


                        <!--/span-->
                    </div>
                </div>
                <div class="form-actions border-top">
                    <div class="card-body">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="row">
                                    <div class="col-md-offset-3 col-md-9">
                                        <a href="{{ route('users.edit',$user->id) }}" class="btn btn-primary">
                                            <iconify-icon icon="solar:document-add-broken" width="27"
                                                          height="27"></iconify-icon>                                        Edit
                                        </a>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6"></div>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
@endsection

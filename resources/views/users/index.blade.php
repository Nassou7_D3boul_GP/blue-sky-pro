@extends('layouts.appDash')

@section('content')


    <div class="row">
        <div class="card-body">
            <div class=" d-flex mb-2 align-items-center ">
                <div>
                    <h5>User Management</h5>
                </div>


                <div class="ms-auto flex-shrink-0">
                    <input type="search" class="form-control"  id="myInputUsers" placeholder="Search...">
                </div>
                <div class="ms-auto flex-shrink-0">
                    <a href="{{ route('users.create') }}" class="btn bg-primary-subtle text-primary btn-sm" title="View Code"
{{--                            data-bs-toggle="modal" data-bs-target="#view-code2-modal"--}}
                    >
                        <iconify-icon icon="solar:add-circle-broken" width="30" height="30"></iconify-icon>
                        Create New User
                    </a>
                    <!-- Code Modal -->
                    <div id="view-code2-modal" class="modal fade" tabindex="-1" aria-hidden="true">
                        <div class="modal-dialog modal-dialog-scrollable modal-lg">
                            <div class="modal-content">


                                <div class="modal-header border-bottom">
                                    <h5 class="modal-title" id="exampleModalLabel">
                                        Create New User
                                    </h5>
                                    <button type="button" class="btn-close" data-bs-dismiss="modal"
                                            aria-label="Close"></button>
                                </div>
                                <div class="modal-body p-4">
                                    <div class="card">

                                    </div>
                                </div>

                            </div>
                            <!-- /.modal-content -->
                        </div>
                        <!-- /.modal-dialog -->
                    </div>
                    <!-- /.modal -->
                </div>
            </div>
            @if ($message = Session::get('success'))
                <div class="alert alert-success my-2">
                    <p>{{ $message }}</p>
                </div>
            @endif
            <div class="table-responsive border rounded-1" data-bs-theme="">

                <table class="table text-nowrap customize-table mb-0 align-middle " id="myTable">
                    <thead class="text-dark fs-4">
                    <tr>
                        <th>
                            <h6 class="fs-4 fw-semibold mb-0">Name</h6>
                        </th>
                        <th>
                            <h6 class="fs-4 fw-semibold mb-0">Email</h6>
                        </th>
                        <th>
                            <h6 class="fs-4 fw-semibold mb-0">Phone</h6>
                        </th>
                        <th>
                            <h6 class="fs-4 fw-semibold mb-0">Status</h6>
                        </th>
                        <th>
                            <h6 class="fs-4 fw-semibold mb-0">Action</h6>
                        </th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach ($data as $key => $user)

                        {{--                                    <tr>--}}
                        {{--                                        <td>{{ $user->name }}</td>--}}
                        {{--                                        <td>{{ $user->email }}</td>--}}
                        {{--                                        <td>--}}
                        {{--                                            @if(!empty($user->getRoleNames()))--}}
                        {{--                                                @foreach($user->getRoleNames() as $v)--}}
                        {{--                                                    <label class="badge badge-secondary text-dark">{{ $v }}</label>--}}
                        {{--                                                @endforeach--}}
                        {{--                                            @endif--}}
                        {{--                                        </td>--}}
                        {{--                                        <td>--}}
                        {{--                                            --}}
                        {{--                                            <a class="btn btn-info" href="{{ route('users.show',$user->id) }}">Show</a>--}}
                        {{--                                            <a class="btn btn-primary" href="{{ route('users.edit',$user->id) }}">Edit</a>--}}
                        {{--                                            <a class="btn btn-success" href="{{ route('users.destroy',$user->id) }}"> Delete</a>--}}
                        {{--                                        </td>--}}
                        {{--                                    </tr>--}}
                        <tr>
                            <th class="userId" hidden>{{ $user->id }}</th>

                            <td>
                                <div class="d-flex align-items-center">
                                    <img src="{{asset('/assets/images/logos/user.jpg')}}" class="rounded-circle"
                                         width="40" height="40">
                                    <div class="ms-3">
                                        <h6 class="fs-4 fw-semibold mb-0">{{$user->FirstName}}  {{$user->LastName}}</h6>
                                        {{--                                                    <span class="fw-normal">Web Designer</span>--}}
                                        @if(!empty($user->getRoleNames()))
                                            @foreach($user->getRoleNames() as $v)
                                                {{--                                                <label class="badge badge-secondary text-dark">{{ $v }}</label>--}}
                                                <span class="fw-normal">{{ $v }}</span>

                                            @endforeach
                                        @endif
                                    </div>
                                </div>
                            </td>
                            <td>
                                <p class="mb-0 fw-normal fs-4">{{$user->email}}</p>
                            </td>
                            <td>
                                <div class="d-flex align-items-center">

                                    {{$user->Phone}}


                                    {{--                                                <a href="javascript:void(0)"--}}
                                    {{--                                                   class="text-bg-secondary text-white fs-6 round-40 rounded-circle me-n2 card-hover border border-2 border-white d-flex align-items-center justify-content-center">--}}
                                    {{--                                                    S--}}
                                    {{--                                                </a>--}}
                                    {{--                                                <a href="javascript:void(0)"--}}
                                    {{--                                                   class="text-bg-danger text-white fs-6 round-40 rounded-circle me-n2 card-hover border border-2 border-white d-flex align-items-center justify-content-center">--}}
                                    {{--                                                    D--}}
                                    {{--                                                </a>--}}
                                </div>
                            </td>
                            <td>
                                @if($user->Blocked == 0)
                                    <span class="badge bg-success-subtle text-success blocked">Active</span>
                                @else
                                    <span class="badge bg-danger-subtle text-danger blocked">Blocked</span>
                                @endif
                            </td>
                            <td>
                                {{--                                            <h6 class="fs-4 fw-semibold mb-0">$3.9k</h6>--}}
                                <div class="dropdown dropstart">
                                    <a href="javascript:void(0)" class="text-muted" id="dropdownMenuButton"
                                       data-bs-toggle="dropdown" aria-expanded="false">
                                        <iconify-icon icon="solar:hamburger-menu-linear" width="27"
                                                      height="27"></iconify-icon>
                                    </a>
                                    <ul class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                        <li>
                                            <a class="dropdown-item d-flex align-items-center gap-3"
{{--                                               href="javascript:void(0)" --}}
                                               href="{{ route('users.show',$user->id) }}">
                                                {{--                                                            <iconify-icon icon="solar:plus-line-duotone"></iconify-icon>--}}
                                                {{--                                                            <i class="fs-4 ti ti-plus"></i>--}}
                                                <iconify-icon icon="solar:info-circle-broken" width="27"
                                                              height="27"></iconify-icon>
                                                Show
                                            </a>
                                        </li>

                                        <li>
                                            <a class="dropdown-item d-flex align-items-center gap-3"
                                               href="{{ route('users.edit',$user->id) }}">
                                                <iconify-icon icon="solar:document-add-broken" width="27"
                                                              height="27"></iconify-icon>
                                                Edit
                                            </a>
                                        </li>
                                        <li>
                                            <form action="{{ route('users.destroy',$user->id) }}" class="dropdown-item d-flex align-items-center gap-3 m-0" method="POST">
                                                @csrf
                                                @method('DELETE')
{{--                                            <a class="dropdown-item d-flex align-items-center gap-3"--}}
{{--                                               href="">--}}
                                                <iconify-icon icon="solar:trash-bin-trash-broken" width="27"
                                                              height="27"></iconify-icon>

                                                        <button type="submit" class="dropdown-item  d-flex align-content-between p-0 ">Delete</button>

{{--                                            </a>--}}
                                            </form>
                                        </li>
                                    </ul>
                                </div>
                            </td>
                        </tr>

                    @endforeach



                    {{--                    <tr>--}}
                    {{--                        <td>--}}
                    {{--                            <div class="d-flex align-items-center">--}}
                    {{--                                <img src="../assets/images/profile/user-3.jpg" class="rounded-circle"--}}
                    {{--                                     width="40" height="40">--}}
                    {{--                                <div class="ms-3">--}}
                    {{--                                    <h6 class="fs-4 fw-semibold mb-0">Sunil Joshi</h6>--}}
                    {{--                                    <span class="fw-normal">Web Designer</span>--}}
                    {{--                                </div>--}}
                    {{--                            </div>--}}
                    {{--                        </td>--}}
                    {{--                        <td>--}}
                    {{--                            <p class="mb-0 fw-normal fs-4">Elite Admin</p>--}}
                    {{--                        </td>--}}
                    {{--                        <td>--}}
                    {{--                            <div class="d-flex align-items-center">--}}
                    {{--                                <a href="javascript:void(0)"--}}
                    {{--                                   class="text-bg-secondary text-white fs-6 round-40 rounded-circle me-n2 card-hover border border-2 border-white d-flex align-items-center justify-content-center">--}}
                    {{--                                    S--}}
                    {{--                                </a>--}}
                    {{--                                <a href="javascript:void(0)"--}}
                    {{--                                   class="text-bg-danger text-white fs-6 round-40 rounded-circle me-n2 card-hover border border-2 border-white d-flex align-items-center justify-content-center">--}}
                    {{--                                    D--}}
                    {{--                                </a>--}}
                    {{--                            </div>--}}
                    {{--                        </td>--}}
                    {{--                        <td>--}}
                    {{--                            <span class="badge bg-success-subtle text-success">Active</span>--}}
                    {{--                        </td>--}}
                    {{--                        <td>--}}
                    {{--                            <h6 class="fs-4 fw-semibold mb-0">$3.9k</h6>--}}
                    {{--                        </td>--}}
                    {{--                    </tr>--}}
                    </tbody>
                </table>
            </div>
        </div>

        @if(!isset($paginate))
            <div class="d-flex justify-content-center my-4">
                {!! $data->links() !!}
            </div>
        @endif
    </div>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.3/jquery.min.js"></script>

    <script>
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content'),
            }
        });
    </script>
    <script>

        $(document).ready(function () {
            $('.blocked').click(function () {

                let blocked;
                var currentRow = $(this).closest("tr");
                var  user_id = currentRow.find("th:eq(0)").text();

                console.log(user_id)

                if (currentRow.find("td:eq(3)").text().trim() == 'Active') {
                    blocked = 1;

                } else  {
                    blocked = 0;

                }
                console.log(blocked)

            $.ajax({
                    url: "{{route('UserBlock')}}",
                    data: {
                        "_token": "{{ csrf_token() }}",
                        blocked: blocked,
                         user_id:  user_id
                    },
                    type: "POST",
                    success: function (response) {
                        JSON.stringify(response);
                        console.log(response)
                        setTimeout(function () {
                            location.reload();
                        }, 200);
                    }
                });
            });
        });


        $(document).ready(function () {
            // $("#myInputUsers").on("keyup", function () {
            //     var value = $(this).val().toLowerCase();
            //     $("#myTable tr").filter(function () {
            //         $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
            //         $("tr:first").fadeIn();
            //     });
            // });
            $(document).ready(function(){
                $('#myInputUsers').on('keyup', function(){
                    var search = $(this).val();

                    $.ajax({
                        type: 'get',
                        url: '{{ route("search") }}',
                        data: {'search': search},
                        success: function(data){
                            // $('#myTable tr:not(:first-child)').empty();
                            $('#myTable tbody').empty();
                            $.each(data, function(index, user){
                                var roles = '';
                                console.log(user.roles)
                                if(user.roles != null && user.roles.length > 0) {
                                    $.each(user.roles, function(i, role){
                                        roles += '<span class="fw-normal">' + role.name + '</span> ';
                                    });
                                }
                                var blockedStatus = user.Blocked == 0 ? '<span class="badge bg-success-subtle text-success blocked">Active</span>' : '<span class="badge bg-danger-subtle text-danger blocked">Blocked</span>';
                                var html = '<tr>' +
                                    '<th class="userId" hidden>' + user.id + '</th>' +
                                    '<td>' +
                                    '<div class="d-flex align-items-center">' +
                                    '<img src="{{asset('/assets/images/logos/user.jpg')}}" class="rounded-circle" width="40" height="40">' +
                                    '<div class="ms-3">' +
                                    '<h6 class="fs-4 fw-semibold mb-0">' + user.FirstName + ' ' + user.LastName + '</h6>' +
                                    roles +
                                    '</div>' +
                                    '</div>' +
                                    '</td>' +
                                    '<td>' +
                                    '<p class="mb-0 fw-normal fs-4">' + user.email + '</p>' +
                                    '</td>' +
                                    '<td>' +
                                    '<div class="d-flex align-items-center">' +
                                    user.Phone +
                                    '</div>' +
                                    '</td>' +
                                    '<td>' + blockedStatus + '</td>' +
                                    '<td>' +
                                    '<div class="dropdown dropstart">' +
                                    '<a href="javascript:void(0)" class="text-muted" id="dropdownMenuButton" data-bs-toggle="dropdown" aria-expanded="false">' +
                                    '<iconify-icon icon="solar:hamburger-menu-linear" width="27" height="27"></iconify-icon>' +
                                    '</a>' +
                                    '<ul class="dropdown-menu" aria-labelledby="dropdownMenuButton">' +
                                    '<li>' +
                                    '<a class="dropdown-item d-flex align-items-center gap-3" href="{{ route('users.show', ':id') }}">Show</a>'.replace(':id', user.id) +
                                    '</li>' +
                                    '<li>' +
                                    '<a class="dropdown-item d-flex align-items-center gap-3" href="{{ route('users.edit', ':id') }}">Edit</a>'.replace(':id', user.id) +
                                    '</li>' +
                                    '<li>' +
                                    '<form action="{{ route('users.destroy', ':id') }}" class="dropdown-item d-flex align-items-center gap-3 m-0" method="POST">'.replace(':id', user.id) +
                                    '@csrf' +
                                    '@method('DELETE')' +
                                    '<button type="submit" class="dropdown-item d-flex align-content-between p-0">Delete</button>' +
                                    '</form>' +
                                    '</li>' +
                                    '</ul>' +
                                    '</div>' +
                                    '</td>' +
                                    '</tr>';

                                $('#myTable').append(html);
                            });
                        }
                    });
                });
            });

            {{--$('#myInputUsers').on('keyup', function(){--}}
            {{--    var search = $(this).val();--}}

            {{--    $.ajax({--}}
            {{--        type: 'get',--}}
            {{--        url: '{{ route("search") }}',--}}
            {{--        data: {'search': search},--}}
            {{--        success: function(data){--}}
            {{--            $('#myTable').empty();--}}
            {{--            $.each(data, function(index, user){--}}
            {{--                $('#myTable').append(--}}
            {{--                    --}}
            {{--                    --}}
            {{--                    --}}
            {{--                    '<li>'+user.FirstName+' '+user.LastName+'</li>'--}}
            {{--                );--}}
            {{--            });--}}
            {{--        }--}}
            {{--    });--}}
            {{--});--}}
        });
    </script>
@endsection

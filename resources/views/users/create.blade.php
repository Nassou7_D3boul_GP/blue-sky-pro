@extends('layouts.appDash')

@section('content')
    <div class="row">
        <div class="col-lg-12 margin-tb mb-4">
            <div class="pull-left">
                <h2>
                    <div class="float-end">
                        <a class="btn bg-danger-subtle text-danger" href="{{ route('users.index') }}"> Back</a>
                    </div>
                </h2>
            </div>
        </div>
    </div>

    @if (count($errors) > 0)
        <div class="alert alert-danger">
            <strong>Whoops!</strong> There were some problems with your input.<br><br>
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
    <form action="{{ route('users.store') }}" method="POST">
        @csrf
            <div class="card">
                <div class="card-header text-bg-primary">
                    <h5 class="mb-0 text-white">Create New User</h5>
                </div>
                <div class="px-4 py-3 border-bottom">
                    <h4 class="card-title mb-0"></h4>
                </div>
                <div class="card-body p-4 border-bottom">
                    <h5 class="fs-4 fw-semibold mb-4">Account Details</h5>
                    <div class="row">
                        <div class="col-lg-6">
                            <div class="mb-4">
                                <label class="form-label">Email</label>
                                <div class="input-group">
                                    <input type="text" name="email" class="form-control">
                                    <span class="input-group-text px-6" id="basic-addon1">@example.com</span>
                                </div>
                            </div>
                            <div>
                                <label class="form-label">Role</label>
                                <select class="form-select multiple" multiple name="roles[]">
                                    @foreach ($roles as $role)
                                        <option value="{{ $role }}">{{ $role }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <div class="mb-4">
                                <label class="form-label">Password</label>
                                <div class="input-group">
                                    <input type="password" name="password" class="form-control">
                          </span>
                                </div>
                            </div>

                            <div>
                                <label class="form-label">Confirm Password</label>
                                <div class="input-group">
                                    <input type="password" class="form-control" name="confirm-password">
                          </span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="card-body p-4">
                    <h5 class="fs-4 fw-semibold mb-4">Personal Info</h5>
                    <div class="row">
                        <div class="col-lg-6">
                            <div class="mb-4">
                                <label for="exampleInputfirstname4" class="form-label">First Name</label>
                                <input type="text" class="form-control" name="FirstName" id="exampleInputfirstname4">
                            </div>
                            <div class="mb-4">
                                <label class="form-label">Country</label>
                                <select class="form-select" aria-label="Default select example" name="Country">
                                    <option selected="" value="Syria">Syria</option>
                                    <option value="United Kingdom">United Kingdom</option>
                                    <option value="UAE">UAE</option>
                                </select>
                            </div>
                            <div class="mb-4">
                                <label for="exampleInputBirthDate" class="form-label">Birth Date</label>
                                <input id="exampleInputBirthDate" name="Birthdate" class="form-control" type="date">
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <div class="mb-4">
                                <label for="exampleInputlastname" class="form-label">Last Name</label>
                                <input type="text" class="form-control" name="LastName" id="exampleInputlastname">
                            </div>
                            <div class="mb-4">
                                <label class="form-label">Address</label>
                                <input type="text" class="form-select" aria-label="Default select example" name="Address">
                            </div>
                            <div class="mb-4">
                                <label for="exampleInputphoneno" class="form-label">Phone no</label>
                                <input type="text" maxlength="10" class="form-control" id="exampleInputphoneno" name="Phone"
                                       placeholder="0912 123 123">
                            </div>
                        </div>
                        <div class="col-12">
                            <div class="d-flex justify-content-center gap-3">
                                <button class="btn btn-primary" type="submit">Submit</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
    </form>

@endsection

<!DOCTYPE html>
<html lang="en" dir="ltr" data-bs-theme="light" data-color-theme="Blue_Theme" data-layout="vertical"><head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Required meta tags -->

    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <!-- Favicon icon-->
    <link rel="shortcut icon" type="image/png" href="{{asset('assets/images/logos/logo.png')}}">

    <!-- Core Css -->
    <link rel="stylesheet" href="{{asset('/404.blade_files/styles.css')}}">

    <title>BlueSkyDash - 403 (Forbidden)</title>
    <script src="chrome-extension://pbcpfbcibpcbfbmddogfhcijfpboeaaf/inject.js"></script></head>

<body>
<!-- Preloader -->
<div class="preloader" style="display: none;">
    <img src="{{asset('/404.blade_files/favicon.png')}}" alt="loader" class="lds-ripple img-fluid">
</div>
<div id="main-wrapper">
    <div class="position-relative overflow-hidden min-vh-100 w-100 d-flex align-items-center justify-content-center">
        <div class="d-flex align-items-center justify-content-center w-100">
            <div class="row justify-content-center w-100">
                <div class="col-lg-4">
                    <div class="text-center">
                        <img src="{{asset('/404.blade_files/errorimg.svg')}}" alt="matdash-img" class="img-fluid" width="500">
                        <h1 class="fw-semibold mb-7 fs-9">Forbidden!!!</h1>
                        <h4 class="fw-semibold mb-7">We are sorry , but your account is blocked .</h4>
                        <a class="btn btn-primary" href="{{url('/login')}}" role="button">Go Back to Login Page</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="dark-transparent sidebartoggler"></div>
<!-- Import Js Files -->
<script src="{{asset('/404.blade_files/bootstrap.bundle.min.js.download')}}"></script>
<script src="{{asset('/404.blade_files/simplebar.min.js.download')}}"></script>
<script src="{{asset('/404.blade_files/app.init.js.download')}}"></script>
<script src="{{asset('/404.blade_files/theme.js.download')}}"></script>
<script src="{{asset('/404.blade_files/app.min.js.download')}}"></script>
<script src="{{asset('/404.blade_files/sidebarmenu.js.download')}}"></script>

<!-- solar icons -->
<script src="{{asset('/404.blade_files/iconify-icon.min.js.download')}}"></script>


<amino-inspect></amino-inspect></body></html>

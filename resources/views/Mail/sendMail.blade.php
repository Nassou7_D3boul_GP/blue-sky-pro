<!DOCTYPE html>
<html>
<head>
    <title>Welcome to BlueSky</title>
    <style>
        body {
            font-family: Arial, sans-serif;
            background-color: #f4f4f4;
            margin: 0;
            padding: 0;
        }
        .container {
            max-width: 600px;
            margin: 0 auto;
            padding: 20px;
            background-color: #ffffff;
            border-radius: 10px;
            box-shadow: 0px 0px 20px rgba(0, 0, 0, 0.1);
        }
        h1 {
            color: #3d78e5;
            text-align: center;
            margin-bottom: 30px;
        }
        p {
            font-size: 16px;
            line-height: 1.5;
            color: #333333;
        }
        .verification-code {
            font-size: 40px;
            color: #3d78e5;
            text-align: center;
            margin-bottom: 20px;
        }
        .thank-you {
            font-size: 18px;
            text-align: center;
            margin-top: 30px;
            color: #777777;
        }
    </style>
</head>
<body>
<div class="container">
    <h1>Welcome to BlueSky</h1>
    <p>Your BlueSky Verification code is:</p>
    <p class="verification-code">{{$mailData['code']}}</p>
    <p>Enter it in the BlueSky verify email screen to create your account.</p>
    <p>If you are not trying to create a BlueSky account, you may ignore this email.</p>
    <p class="thank-you">Thank you for joining our app</p>
</div>
</body>
</html>

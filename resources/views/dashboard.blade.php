@extends('layouts.appDash')

@section('content')
    <div class="row">
        <div class="col-lg-5">

{{--        @include('profile.edit')--}}


        <!-- -------------------------------------------- -->
            <!-- Welcome Card -->
            <!-- -------------------------------------------- -->
            <div class="card text-bg-primary">
                <div class="card-body">
                    <div class="row">
                        <div class="col-sm-7">
                            <div class="d-flex flex-column h-100">
                                <div class="hstack gap-3">
                          <span
                              class="d-flex align-items-center justify-content-center round-48 bg-white rounded flex-shrink-0">
                            <iconify-icon icon="solar:course-up-outline" class="fs-7 text-muted"></iconify-icon>
                          </span>
                                    <h5 class="text-white fs-6 mb-0">Welcome Back
                                        <br>{{Auth::user()->FirstName}}
                                    </h5>
                                </div>
                                <div class="mt-4 mt-sm-auto">
                                    <div class="row">
                                        <div class="col-6">
                                            <span class="opacity-75">Budget</span>
                                            <h4 class="mb-0 text-white mt-1 text-nowrap">$98,450</h4>
                                        </div>
                                        <div class="col-6 border-start border-light"
                                             style="--bs-border-opacity: .15;">
                                            <span class="opacity-75">Expense</span>
                                            <h4 class="mb-0 text-white mt-1 text-nowrap">$2,440</h4>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-5 text-center text-md-end">
                            <img src="{{asset('/assets/images/welcome-bg.png')}}" alt="welcome"
                                 class="img-fluid mb-n7 mt-2" width="180">
                        </div>
                    </div>


                </div>
            </div>
            <div class="row">
                <!-- -------------------------------------------- -->
                <!-- Customers -->
                <!-- -------------------------------------------- -->
                <div class="col-md-6">
                    <div class="card bg-secondary-subtle overflow-hidden shadow-none">
                        <div class="card-body p-4">
                            <span class="text-dark">Customers</span>
                            <div class="hstack gap-6 align-items-end mt-1">
                                <h5 class="card-title fw-semibold mb-0 fs-7 mt-1">36,358</h5>
                                <span class="fs-11 text-dark fw-semibold">-12%</span>
                            </div>
                        </div>
                        <div id="customers" style="min-height: 71px;">
                            <div id="apexchartscustomers"
                                 class="apexcharts-canvas apexchartscustomers apexcharts-theme-light"
                                 style="width: 214px; height: 71px;">
                                <svg id="SvgjsSvg1792" width="214" height="71"
                                     xmlns="http://www.w3.org/2000/svg" version="1.1"
                                     xmlns:xlink="http://www.w3.org/1999/xlink"
                                     xmlns:svgjs="http://svgjs.dev" class="apexcharts-svg"
                                     xmlns:data="ApexChartsNS" transform="translate(0, 0)"
                                     style="background: transparent;">
                                    <foreignObject x="0" y="0" width="214" height="71">
                                        <div class="apexcharts-legend" xmlns="http://www.w3.org/1999/xhtml"
                                             style="max-height: 35.5px;"></div>
                                    </foreignObject>
                                    <rect id="SvgjsRect1796" width="0" height="0" x="0" y="0" rx="0" ry="0"
                                          opacity="1" stroke-width="0" stroke="none" stroke-dasharray="0"
                                          fill="#fefefe"></rect>
                                    <g id="SvgjsG1828" class="apexcharts-yaxis" rel="0"
                                       transform="translate(-18, 0)"></g>
                                    <g id="SvgjsG1794" class="apexcharts-inner apexcharts-graphical"
                                       transform="translate(0, 1)">
                                        <defs id="SvgjsDefs1793">
                                            <clipPath id="gridRectMaskx82sbs6s">
                                                <rect id="SvgjsRect1798" width="220" height="81" x="-4"
                                                      y="-6" rx="0" ry="0" opacity="1" stroke-width="0"
                                                      stroke="none" stroke-dasharray="0" fill="#fff"></rect>
                                            </clipPath>
                                            <clipPath id="forecastMaskx82sbs6s"></clipPath>
                                            <clipPath id="nonForecastMaskx82sbs6s"></clipPath>
                                            <clipPath id="gridRectMarkerMaskx82sbs6s">
                                                <rect id="SvgjsRect1799" width="218" height="73" x="-2"
                                                      y="-2" rx="0" ry="0" opacity="1" stroke-width="0"
                                                      stroke="none" stroke-dasharray="0" fill="#fff"></rect>
                                            </clipPath>
                                            <linearGradient id="SvgjsLinearGradient1804" x1="0" y1="0"
                                                            x2="0" y2="1">
                                                <stop id="SvgjsStop1805" stop-opacity="0.2"
                                                      stop-color="var(--bs-secondary)" offset="1"></stop>
                                                <stop id="SvgjsStop1806" stop-opacity="0.1" stop-color=""
                                                      offset="1"></stop>
                                                <stop id="SvgjsStop1807" stop-opacity="0.1" stop-color=""
                                                      offset="1"></stop>
                                            </linearGradient>
                                        </defs>
                                        <line id="SvgjsLine1797" x1="0" y1="0" x2="0" y2="69"
                                              stroke="#b6b6b6" stroke-dasharray="3" stroke-linecap="butt"
                                              class="apexcharts-xcrosshairs" x="0" y="0" width="1"
                                              height="69" fill="#b1b9c4" filter="none" fill-opacity="0.9"
                                              stroke-width="1"></line>
                                        <g id="SvgjsG1810" class="apexcharts-grid">
                                            <g id="SvgjsG1811" class="apexcharts-gridlines-horizontal"
                                               style="display: none;">
                                                <line id="SvgjsLine1814" x1="0" y1="0" x2="214" y2="0"
                                                      stroke="#e0e0e0" stroke-dasharray="0"
                                                      stroke-linecap="butt"
                                                      class="apexcharts-gridline"></line>
                                                <line id="SvgjsLine1815" x1="0" y1="69" x2="214" y2="69"
                                                      stroke="#e0e0e0" stroke-dasharray="0"
                                                      stroke-linecap="butt"
                                                      class="apexcharts-gridline"></line>
                                            </g>
                                            <g id="SvgjsG1812" class="apexcharts-gridlines-vertical"
                                               style="display: none;"></g>
                                            <line id="SvgjsLine1817" x1="0" y1="69" x2="214" y2="69"
                                                  stroke="transparent" stroke-dasharray="0"
                                                  stroke-linecap="butt"></line>
                                            <line id="SvgjsLine1816" x1="0" y1="1" x2="0" y2="69"
                                                  stroke="transparent" stroke-dasharray="0"
                                                  stroke-linecap="butt"></line>
                                        </g>
                                        <g id="SvgjsG1813" class="apexcharts-grid-borders"
                                           style="display: none;"></g>
                                        <g id="SvgjsG1800"
                                           class="apexcharts-area-series apexcharts-plot-series">
                                            <g id="SvgjsG1801" class="apexcharts-series" zIndex="0"
                                               seriesName="customers" data:longestSeries="true" rel="1"
                                               data:realIndex="0">
                                                <path id="SvgjsPath1808"
                                                      d="M 0 69 L 0 46C 14.98 46 27.820000000000004 11.5 42.800000000000004 11.5C 57.78 11.5 70.62 65.16666666666666 85.60000000000001 65.16666666666666C 100.58000000000001 65.16666666666666 113.42 3.8333333333333144 128.4 3.8333333333333144C 143.38 3.8333333333333144 156.22000000000003 38.33333333333334 171.20000000000002 38.33333333333334C 186.18 38.33333333333334 199.02 19.166666666666657 214 19.166666666666657C 214 19.166666666666657 214 19.166666666666657 214 69M 214 19.166666666666657z"
                                                      fill="url(#SvgjsLinearGradient1804)" fill-opacity="1"
                                                      stroke-opacity="1" stroke-linecap="butt"
                                                      stroke-width="0" stroke-dasharray="0"
                                                      class="apexcharts-area" index="0"
                                                      clip-path="url(#gridRectMaskx82sbs6s)"
                                                      pathTo="M 0 69 L 0 46C 14.98 46 27.820000000000004 11.5 42.800000000000004 11.5C 57.78 11.5 70.62 65.16666666666666 85.60000000000001 65.16666666666666C 100.58000000000001 65.16666666666666 113.42 3.8333333333333144 128.4 3.8333333333333144C 143.38 3.8333333333333144 156.22000000000003 38.33333333333334 171.20000000000002 38.33333333333334C 186.18 38.33333333333334 199.02 19.166666666666657 214 19.166666666666657C 214 19.166666666666657 214 19.166666666666657 214 69M 214 19.166666666666657z"
                                                      pathFrom="M -1 184 L -1 184 L 42.800000000000004 184 L 85.60000000000001 184 L 128.4 184 L 171.20000000000002 184 L 214 184"></path>
                                                <path id="SvgjsPath1809"
                                                      d="M 0 46C 14.98 46 27.820000000000004 11.5 42.800000000000004 11.5C 57.78 11.5 70.62 65.16666666666666 85.60000000000001 65.16666666666666C 100.58000000000001 65.16666666666666 113.42 3.8333333333333144 128.4 3.8333333333333144C 143.38 3.8333333333333144 156.22000000000003 38.33333333333334 171.20000000000002 38.33333333333334C 186.18 38.33333333333334 199.02 19.166666666666657 214 19.166666666666657"
                                                      fill="none" fill-opacity="1"
                                                      stroke="var(--bs-secondary)" stroke-opacity="1"
                                                      stroke-linecap="butt" stroke-width="2"
                                                      stroke-dasharray="0" class="apexcharts-area" index="0"
                                                      clip-path="url(#gridRectMaskx82sbs6s)"
                                                      pathTo="M 0 46C 14.98 46 27.820000000000004 11.5 42.800000000000004 11.5C 57.78 11.5 70.62 65.16666666666666 85.60000000000001 65.16666666666666C 100.58000000000001 65.16666666666666 113.42 3.8333333333333144 128.4 3.8333333333333144C 143.38 3.8333333333333144 156.22000000000003 38.33333333333334 171.20000000000002 38.33333333333334C 186.18 38.33333333333334 199.02 19.166666666666657 214 19.166666666666657"
                                                      pathFrom="M -1 184 L -1 184 L 42.800000000000004 184 L 85.60000000000001 184 L 128.4 184 L 171.20000000000002 184 L 214 184"
                                                      fill-rule="evenodd"></path>
                                                <g id="SvgjsG1802"
                                                   class="apexcharts-series-markers-wrap apexcharts-hidden-element-shown"
                                                   data:realIndex="0">
                                                    <g class="apexcharts-series-markers">
                                                        <circle id="SvgjsCircle1832" r="0" cx="0" cy="0"
                                                                class="apexcharts-marker w8r63dn8g no-pointer-events"
                                                                stroke="#ffffff" fill="var(--bs-secondary)"
                                                                fill-opacity="1" stroke-width="2"
                                                                stroke-opacity="0.9"
                                                                default-marker-size="0"></circle>
                                                    </g>
                                                </g>
                                            </g>
                                            <g id="SvgjsG1803" class="apexcharts-datalabels"
                                               data:realIndex="0"></g>
                                        </g>
                                        <line id="SvgjsLine1818" x1="0" y1="0" x2="214" y2="0"
                                              stroke="#b6b6b6" stroke-dasharray="0" stroke-width="1"
                                              stroke-linecap="butt" class="apexcharts-ycrosshairs"></line>
                                        <line id="SvgjsLine1819" x1="0" y1="0" x2="214" y2="0"
                                              stroke-dasharray="0" stroke-width="0" stroke-linecap="butt"
                                              class="apexcharts-ycrosshairs-hidden"></line>
                                        <g id="SvgjsG1820" class="apexcharts-xaxis"
                                           transform="translate(0, 0)">
                                            <g id="SvgjsG1821" class="apexcharts-xaxis-texts-g"
                                               transform="translate(0, -4)"></g>
                                        </g>
                                        <g id="SvgjsG1829" class="apexcharts-yaxis-annotations"></g>
                                        <g id="SvgjsG1830" class="apexcharts-xaxis-annotations"></g>
                                        <g id="SvgjsG1831" class="apexcharts-point-annotations"></g>
                                    </g>
                                </svg>
                                <div class="apexcharts-tooltip apexcharts-theme-dark">
                                    <div class="apexcharts-tooltip-series-group" style="order: 1;"><span
                                            class="apexcharts-tooltip-marker"
                                            style="background-color: var(--bs-secondary);"></span>
                                        <div class="apexcharts-tooltip-text"
                                             style="font-family: inherit; font-size: 12px;">
                                            <div class="apexcharts-tooltip-y-group"><span
                                                    class="apexcharts-tooltip-text-y-label"></span><span
                                                    class="apexcharts-tooltip-text-y-value"></span></div>
                                            <div class="apexcharts-tooltip-goals-group"><span
                                                    class="apexcharts-tooltip-text-goals-label"></span><span
                                                    class="apexcharts-tooltip-text-goals-value"></span>
                                            </div>
                                            <div class="apexcharts-tooltip-z-group"><span
                                                    class="apexcharts-tooltip-text-z-label"></span><span
                                                    class="apexcharts-tooltip-text-z-value"></span></div>
                                        </div>
                                    </div>
                                </div>
                                <div
                                    class="apexcharts-yaxistooltip apexcharts-yaxistooltip-0 apexcharts-yaxistooltip-left apexcharts-theme-dark">
                                    <div class="apexcharts-yaxistooltip-text"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- -------------------------------------------- -->
                <!-- Projects -->
                <!-- -------------------------------------------- -->
                <div class="col-md-6">
                    <div class="card bg-danger-subtle overflow-hidden shadow-none">
                        <div class="card-body p-4">
                            <span class="text-dark">Projects</span>
                            <div class="hstack gap-6 align-items-end mt-1 mb-4">
                                <h5 class="card-title fw-semibold mb-0 fs-7 mt-1">78,298</h5>
                                <span class="fs-11 text-dark fw-semibold">+31.8%</span>
                            </div>
                            <div class="me-n3">
                                <div id="projects" style="min-height: 50px;">
                                    <div id="apexcharts166q071x"
                                         class="apexcharts-canvas apexcharts166q071x apexcharts-theme-light"
                                         style="width: 182px; height: 50px;">
                                        <svg id="SvgjsSvg1833" width="182" height="50"
                                             xmlns="http://www.w3.org/2000/svg" version="1.1"
                                             xmlns:xlink="http://www.w3.org/1999/xlink"
                                             xmlns:svgjs="http://svgjs.dev" class="apexcharts-svg"
                                             xmlns:data="ApexChartsNS" transform="translate(-10, 0)"
                                             style="background: transparent;">
                                            <foreignObject x="0" y="0" width="182" height="50">
                                                <div class="apexcharts-legend"
                                                     xmlns="http://www.w3.org/1999/xhtml"
                                                     style="max-height: 25px;"></div>
                                            </foreignObject>
                                            <g id="SvgjsG1889" class="apexcharts-yaxis" rel="0"
                                               transform="translate(-18, 0)"></g>
                                            <g id="SvgjsG1835" class="apexcharts-inner apexcharts-graphical"
                                               transform="translate(16.5125, 0)">
                                                <defs id="SvgjsDefs1834">
                                                    <linearGradient id="SvgjsLinearGradient1837" x1="0"
                                                                    y1="0" x2="0" y2="1">
                                                        <stop id="SvgjsStop1838" stop-opacity="0.4"
                                                              stop-color="rgba(216,227,240,0.4)"
                                                              offset="0"></stop>
                                                        <stop id="SvgjsStop1839" stop-opacity="0.5"
                                                              stop-color="rgba(190,209,230,0.5)"
                                                              offset="1"></stop>
                                                        <stop id="SvgjsStop1840" stop-opacity="0.5"
                                                              stop-color="rgba(190,209,230,0.5)"
                                                              offset="1"></stop>
                                                    </linearGradient>
                                                    <clipPath id="gridRectMask166q071x">
                                                        <rect id="SvgjsRect1842" width="185.99999999999997"
                                                              height="54" x="-14.5125" y="-2" rx="0" ry="0"
                                                              opacity="1" stroke-width="0" stroke="none"
                                                              stroke-dasharray="0" fill="#fff"></rect>
                                                    </clipPath>
                                                    <clipPath id="forecastMask166q071x"></clipPath>
                                                    <clipPath id="nonForecastMask166q071x"></clipPath>
                                                    <clipPath id="gridRectMarkerMask166q071x">
                                                        <rect id="SvgjsRect1843" width="160.975" height="54"
                                                              x="-2" y="-2" rx="0" ry="0" opacity="1"
                                                              stroke-width="0" stroke="none"
                                                              stroke-dasharray="0" fill="#fff"></rect>
                                                    </clipPath>
                                                </defs>
                                                <rect id="SvgjsRect1841" width="10.79203125" height="50"
                                                      x="0" y="0" rx="0" ry="0" opacity="1" stroke-width="0"
                                                      stroke-dasharray="3"
                                                      fill="url(#SvgjsLinearGradient1837)"
                                                      class="apexcharts-xcrosshairs" y2="50" filter="none"
                                                      fill-opacity="0.9"></rect>
                                                <g id="SvgjsG1867" class="apexcharts-grid">
                                                    <g id="SvgjsG1868"
                                                       class="apexcharts-gridlines-horizontal"
                                                       style="display: none;">
                                                        <line id="SvgjsLine1871" x1="-12.5125" y1="0"
                                                              x2="169.48749999999998" y2="0"
                                                              stroke="#e0e0e0" stroke-dasharray="0"
                                                              stroke-linecap="butt"
                                                              class="apexcharts-gridline"></line>
                                                        <line id="SvgjsLine1872" x1="-12.5125" y1="50"
                                                              x2="169.48749999999998" y2="50"
                                                              stroke="#e0e0e0" stroke-dasharray="0"
                                                              stroke-linecap="butt"
                                                              class="apexcharts-gridline"></line>
                                                    </g>
                                                    <g id="SvgjsG1869" class="apexcharts-gridlines-vertical"
                                                       style="display: none;"></g>
                                                    <line id="SvgjsLine1874" x1="0" y1="50" x2="156.975"
                                                          y2="50" stroke="transparent" stroke-dasharray="0"
                                                          stroke-linecap="butt"></line>
                                                    <line id="SvgjsLine1873" x1="0" y1="1" x2="0" y2="50"
                                                          stroke="transparent" stroke-dasharray="0"
                                                          stroke-linecap="butt"></line>
                                                </g>
                                                <g id="SvgjsG1870" class="apexcharts-grid-borders"
                                                   style="display: none;"></g>
                                                <g id="SvgjsG1844"
                                                   class="apexcharts-bar-series apexcharts-plot-series">
                                                    <g id="SvgjsG1845" class="apexcharts-series" rel="1"
                                                       seriesName="Project" data:realIndex="0">
                                                        <path id="SvgjsPath1850"
                                                              d="M -5.396015625 46.001 L -5.396015625 32.572428571428574 C -5.396015625 30.572428571428574 -3.3960156250000004 28.572428571428574 -1.3960156250000004 28.572428571428574 L 1.3960156250000004 28.572428571428574 C 3.3960156250000004 28.572428571428574 5.396015625 30.572428571428574 5.396015625 32.572428571428574 L 5.396015625 46.001 C 5.396015625 48.001 3.3960156250000004 50.001 1.3960156250000004 50.001 L -1.3960156250000004 50.001 C -3.3960156250000004 50.001 -5.396015625 48.001 -5.396015625 46.001 Z "
                                                              fill="var(--bs-white)" fill-opacity="1"
                                                              stroke-opacity="1" stroke-linecap="round"
                                                              stroke-width="0" stroke-dasharray="0"
                                                              class="apexcharts-bar-area" index="0"
                                                              clip-path="url(#gridRectMask166q071x)"
                                                              pathTo="M -5.396015625 46.001 L -5.396015625 32.572428571428574 C -5.396015625 30.572428571428574 -3.3960156250000004 28.572428571428574 -1.3960156250000004 28.572428571428574 L 1.3960156250000004 28.572428571428574 C 3.3960156250000004 28.572428571428574 5.396015625 30.572428571428574 5.396015625 32.572428571428574 L 5.396015625 46.001 C 5.396015625 48.001 3.3960156250000004 50.001 1.3960156250000004 50.001 L -1.3960156250000004 50.001 C -3.3960156250000004 50.001 -5.396015625 48.001 -5.396015625 46.001 Z "
                                                              pathFrom="M -5.396015625 50.001 L -5.396015625 50.001 L 5.396015625 50.001 L 5.396015625 50.001 L 5.396015625 50.001 L 5.396015625 50.001 L 5.396015625 50.001 L -5.396015625 50.001 Z"
                                                              cy="28.571428571428573" cx="5.396015625" j="0"
                                                              val="3" barHeight="21.428571428571427"
                                                              barWidth="10.79203125"></path>
                                                        <path id="SvgjsPath1852"
                                                              d="M 14.225859374999999 46.001 L 14.225859374999999 18.28671428571429 C 14.225859374999999 16.28671428571429 16.225859375 14.286714285714291 18.225859375 14.286714285714291 L 21.017890625 14.286714285714291 C 23.017890625 14.286714285714291 25.017890625 16.28671428571429 25.017890625 18.28671428571429 L 25.017890625 46.001 C 25.017890625 48.001 23.017890625 50.001 21.017890625 50.001 L 18.225859375 50.001 C 16.225859375 50.001 14.225859374999999 48.001 14.225859374999999 46.001 Z "
                                                              fill="var(--bs-white)" fill-opacity="1"
                                                              stroke-opacity="1" stroke-linecap="round"
                                                              stroke-width="0" stroke-dasharray="0"
                                                              class="apexcharts-bar-area" index="0"
                                                              clip-path="url(#gridRectMask166q071x)"
                                                              pathTo="M 14.225859374999999 46.001 L 14.225859374999999 18.28671428571429 C 14.225859374999999 16.28671428571429 16.225859375 14.286714285714291 18.225859375 14.286714285714291 L 21.017890625 14.286714285714291 C 23.017890625 14.286714285714291 25.017890625 16.28671428571429 25.017890625 18.28671428571429 L 25.017890625 46.001 C 25.017890625 48.001 23.017890625 50.001 21.017890625 50.001 L 18.225859375 50.001 C 16.225859375 50.001 14.225859374999999 48.001 14.225859374999999 46.001 Z "
                                                              pathFrom="M 14.225859374999999 50.001 L 14.225859374999999 50.001 L 25.017890625 50.001 L 25.017890625 50.001 L 25.017890625 50.001 L 25.017890625 50.001 L 25.017890625 50.001 L 14.225859374999999 50.001 Z"
                                                              cy="14.285714285714292" cx="25.017890625"
                                                              j="1" val="5" barHeight="35.71428571428571"
                                                              barWidth="10.79203125"></path>
                                                        <path id="SvgjsPath1854"
                                                              d="M 33.847734375 46.001 L 33.847734375 18.28671428571429 C 33.847734375 16.28671428571429 35.847734375 14.286714285714291 37.847734375 14.286714285714291 L 40.639765625 14.286714285714291 C 42.639765625 14.286714285714291 44.639765625 16.28671428571429 44.639765625 18.28671428571429 L 44.639765625 46.001 C 44.639765625 48.001 42.639765625 50.001 40.639765625 50.001 L 37.847734375 50.001 C 35.847734375 50.001 33.847734375 48.001 33.847734375 46.001 Z "
                                                              fill="var(--bs-white)" fill-opacity="1"
                                                              stroke-opacity="1" stroke-linecap="round"
                                                              stroke-width="0" stroke-dasharray="0"
                                                              class="apexcharts-bar-area" index="0"
                                                              clip-path="url(#gridRectMask166q071x)"
                                                              pathTo="M 33.847734375 46.001 L 33.847734375 18.28671428571429 C 33.847734375 16.28671428571429 35.847734375 14.286714285714291 37.847734375 14.286714285714291 L 40.639765625 14.286714285714291 C 42.639765625 14.286714285714291 44.639765625 16.28671428571429 44.639765625 18.28671428571429 L 44.639765625 46.001 C 44.639765625 48.001 42.639765625 50.001 40.639765625 50.001 L 37.847734375 50.001 C 35.847734375 50.001 33.847734375 48.001 33.847734375 46.001 Z "
                                                              pathFrom="M 33.847734375 50.001 L 33.847734375 50.001 L 44.639765625 50.001 L 44.639765625 50.001 L 44.639765625 50.001 L 44.639765625 50.001 L 44.639765625 50.001 L 33.847734375 50.001 Z"
                                                              cy="14.285714285714292" cx="44.639765625"
                                                              j="2" val="5" barHeight="35.71428571428571"
                                                              barWidth="10.79203125"></path>
                                                        <path id="SvgjsPath1856"
                                                              d="M 53.46960937499999 46.001 L 53.46960937499999 4.001000000000007 C 53.46960937499999 2.0010000000000074 55.46960937499999 0.0010000000000071054 57.46960937499999 0.0010000000000071054 L 60.261640624999984 0.0010000000000071054 C 62.261640624999984 0.0010000000000071054 64.26164062499998 2.001000000000007 64.26164062499998 4.001000000000007 L 64.26164062499998 46.001 C 64.26164062499998 48.001 62.261640624999984 50.001 60.261640624999984 50.001 L 57.46960937499999 50.001 C 55.46960937499999 50.001 53.46960937499999 48.001 53.46960937499999 46.001 Z "
                                                              fill="var(--bs-white)" fill-opacity="1"
                                                              stroke-opacity="1" stroke-linecap="round"
                                                              stroke-width="0" stroke-dasharray="0"
                                                              class="apexcharts-bar-area" index="0"
                                                              clip-path="url(#gridRectMask166q071x)"
                                                              pathTo="M 53.46960937499999 46.001 L 53.46960937499999 4.001000000000007 C 53.46960937499999 2.0010000000000074 55.46960937499999 0.0010000000000071054 57.46960937499999 0.0010000000000071054 L 60.261640624999984 0.0010000000000071054 C 62.261640624999984 0.0010000000000071054 64.26164062499998 2.001000000000007 64.26164062499998 4.001000000000007 L 64.26164062499998 46.001 C 64.26164062499998 48.001 62.261640624999984 50.001 60.261640624999984 50.001 L 57.46960937499999 50.001 C 55.46960937499999 50.001 53.46960937499999 48.001 53.46960937499999 46.001 Z "
                                                              pathFrom="M 53.46960937499999 50.001 L 53.46960937499999 50.001 L 64.26164062499998 50.001 L 64.26164062499998 50.001 L 64.26164062499998 50.001 L 64.26164062499998 50.001 L 64.26164062499998 50.001 L 53.46960937499999 50.001 Z"
                                                              cy="7.105427357601002e-15"
                                                              cx="64.26164062499998" j="3" val="7"
                                                              barHeight="49.99999999999999"
                                                              barWidth="10.79203125"></path>
                                                        <path id="SvgjsPath1858"
                                                              d="M 73.091484375 46.001 L 73.091484375 11.143857142857147 C 73.091484375 9.143857142857147 75.091484375 7.143857142857146 77.091484375 7.143857142857146 L 79.883515625 7.143857142857146 C 81.883515625 7.143857142857146 83.883515625 9.143857142857147 83.883515625 11.143857142857147 L 83.883515625 46.001 C 83.883515625 48.001 81.883515625 50.001 79.883515625 50.001 L 77.091484375 50.001 C 75.091484375 50.001 73.091484375 48.001 73.091484375 46.001 Z "
                                                              fill="var(--bs-white)" fill-opacity="1"
                                                              stroke-opacity="1" stroke-linecap="round"
                                                              stroke-width="0" stroke-dasharray="0"
                                                              class="apexcharts-bar-area" index="0"
                                                              clip-path="url(#gridRectMask166q071x)"
                                                              pathTo="M 73.091484375 46.001 L 73.091484375 11.143857142857147 C 73.091484375 9.143857142857147 75.091484375 7.143857142857146 77.091484375 7.143857142857146 L 79.883515625 7.143857142857146 C 81.883515625 7.143857142857146 83.883515625 9.143857142857147 83.883515625 11.143857142857147 L 83.883515625 46.001 C 83.883515625 48.001 81.883515625 50.001 79.883515625 50.001 L 77.091484375 50.001 C 75.091484375 50.001 73.091484375 48.001 73.091484375 46.001 Z "
                                                              pathFrom="M 73.091484375 50.001 L 73.091484375 50.001 L 83.883515625 50.001 L 83.883515625 50.001 L 83.883515625 50.001 L 83.883515625 50.001 L 83.883515625 50.001 L 73.091484375 50.001 Z"
                                                              cy="7.142857142857146" cx="83.883515625" j="4"
                                                              val="6" barHeight="42.857142857142854"
                                                              barWidth="10.79203125"></path>
                                                        <path id="SvgjsPath1860"
                                                              d="M 92.713359375 46.001 L 92.713359375 18.28671428571429 C 92.713359375 16.28671428571429 94.713359375 14.286714285714291 96.713359375 14.286714285714291 L 99.50539062499999 14.286714285714291 C 101.50539062499999 14.286714285714291 103.50539062499999 16.28671428571429 103.50539062499999 18.28671428571429 L 103.50539062499999 46.001 C 103.50539062499999 48.001 101.50539062499999 50.001 99.50539062499999 50.001 L 96.713359375 50.001 C 94.713359375 50.001 92.713359375 48.001 92.713359375 46.001 Z "
                                                              fill="var(--bs-white)" fill-opacity="1"
                                                              stroke-opacity="1" stroke-linecap="round"
                                                              stroke-width="0" stroke-dasharray="0"
                                                              class="apexcharts-bar-area" index="0"
                                                              clip-path="url(#gridRectMask166q071x)"
                                                              pathTo="M 92.713359375 46.001 L 92.713359375 18.28671428571429 C 92.713359375 16.28671428571429 94.713359375 14.286714285714291 96.713359375 14.286714285714291 L 99.50539062499999 14.286714285714291 C 101.50539062499999 14.286714285714291 103.50539062499999 16.28671428571429 103.50539062499999 18.28671428571429 L 103.50539062499999 46.001 C 103.50539062499999 48.001 101.50539062499999 50.001 99.50539062499999 50.001 L 96.713359375 50.001 C 94.713359375 50.001 92.713359375 48.001 92.713359375 46.001 Z "
                                                              pathFrom="M 92.713359375 50.001 L 92.713359375 50.001 L 103.50539062499999 50.001 L 103.50539062499999 50.001 L 103.50539062499999 50.001 L 103.50539062499999 50.001 L 103.50539062499999 50.001 L 92.713359375 50.001 Z"
                                                              cy="14.285714285714292"
                                                              cx="103.50539062499999" j="5" val="5"
                                                              barHeight="35.71428571428571"
                                                              barWidth="10.79203125"></path>
                                                        <path id="SvgjsPath1862"
                                                              d="M 112.33523437499998 46.001 L 112.33523437499998 32.572428571428574 C 112.33523437499998 30.572428571428574 114.33523437499998 28.572428571428574 116.33523437499998 28.572428571428574 L 119.12726562499998 28.572428571428574 C 121.12726562499998 28.572428571428574 123.12726562499998 30.572428571428574 123.12726562499998 32.572428571428574 L 123.12726562499998 46.001 C 123.12726562499998 48.001 121.12726562499998 50.001 119.12726562499998 50.001 L 116.33523437499998 50.001 C 114.33523437499998 50.001 112.33523437499998 48.001 112.33523437499998 46.001 Z "
                                                              fill="var(--bs-white)" fill-opacity="1"
                                                              stroke-opacity="1" stroke-linecap="round"
                                                              stroke-width="0" stroke-dasharray="0"
                                                              class="apexcharts-bar-area" index="0"
                                                              clip-path="url(#gridRectMask166q071x)"
                                                              pathTo="M 112.33523437499998 46.001 L 112.33523437499998 32.572428571428574 C 112.33523437499998 30.572428571428574 114.33523437499998 28.572428571428574 116.33523437499998 28.572428571428574 L 119.12726562499998 28.572428571428574 C 121.12726562499998 28.572428571428574 123.12726562499998 30.572428571428574 123.12726562499998 32.572428571428574 L 123.12726562499998 46.001 C 123.12726562499998 48.001 121.12726562499998 50.001 119.12726562499998 50.001 L 116.33523437499998 50.001 C 114.33523437499998 50.001 112.33523437499998 48.001 112.33523437499998 46.001 Z "
                                                              pathFrom="M 112.33523437499998 50.001 L 112.33523437499998 50.001 L 123.12726562499998 50.001 L 123.12726562499998 50.001 L 123.12726562499998 50.001 L 123.12726562499998 50.001 L 123.12726562499998 50.001 L 112.33523437499998 50.001 Z"
                                                              cy="28.571428571428573"
                                                              cx="123.12726562499998" j="6" val="3"
                                                              barHeight="21.428571428571427"
                                                              barWidth="10.79203125"></path>
                                                        <path id="SvgjsPath1864"
                                                              d="M 131.95710937500002 46.001 L 131.95710937500002 18.28671428571429 C 131.95710937500002 16.28671428571429 133.95710937500002 14.286714285714291 135.95710937500002 14.286714285714291 L 138.74914062500002 14.286714285714291 C 140.74914062500002 14.286714285714291 142.74914062500002 16.28671428571429 142.74914062500002 18.28671428571429 L 142.74914062500002 46.001 C 142.74914062500002 48.001 140.74914062500002 50.001 138.74914062500002 50.001 L 135.95710937500002 50.001 C 133.95710937500002 50.001 131.95710937500002 48.001 131.95710937500002 46.001 Z "
                                                              fill="var(--bs-white)" fill-opacity="1"
                                                              stroke-opacity="1" stroke-linecap="round"
                                                              stroke-width="0" stroke-dasharray="0"
                                                              class="apexcharts-bar-area" index="0"
                                                              clip-path="url(#gridRectMask166q071x)"
                                                              pathTo="M 131.95710937500002 46.001 L 131.95710937500002 18.28671428571429 C 131.95710937500002 16.28671428571429 133.95710937500002 14.286714285714291 135.95710937500002 14.286714285714291 L 138.74914062500002 14.286714285714291 C 140.74914062500002 14.286714285714291 142.74914062500002 16.28671428571429 142.74914062500002 18.28671428571429 L 142.74914062500002 46.001 C 142.74914062500002 48.001 140.74914062500002 50.001 138.74914062500002 50.001 L 135.95710937500002 50.001 C 133.95710937500002 50.001 131.95710937500002 48.001 131.95710937500002 46.001 Z "
                                                              pathFrom="M 131.95710937500002 50.001 L 131.95710937500002 50.001 L 142.74914062500002 50.001 L 142.74914062500002 50.001 L 142.74914062500002 50.001 L 142.74914062500002 50.001 L 142.74914062500002 50.001 L 131.95710937500002 50.001 Z"
                                                              cy="14.285714285714292"
                                                              cx="142.74914062500002" j="7" val="5"
                                                              barHeight="35.71428571428571"
                                                              barWidth="10.79203125"></path>
                                                        <path id="SvgjsPath1866"
                                                              d="M 151.578984375 46.001 L 151.578984375 32.572428571428574 C 151.578984375 30.572428571428574 153.578984375 28.572428571428574 155.578984375 28.572428571428574 L 158.371015625 28.572428571428574 C 160.371015625 28.572428571428574 162.371015625 30.572428571428574 162.371015625 32.572428571428574 L 162.371015625 46.001 C 162.371015625 48.001 160.371015625 50.001 158.371015625 50.001 L 155.578984375 50.001 C 153.578984375 50.001 151.578984375 48.001 151.578984375 46.001 Z "
                                                              fill="var(--bs-white)" fill-opacity="1"
                                                              stroke-opacity="1" stroke-linecap="round"
                                                              stroke-width="0" stroke-dasharray="0"
                                                              class="apexcharts-bar-area" index="0"
                                                              clip-path="url(#gridRectMask166q071x)"
                                                              pathTo="M 151.578984375 46.001 L 151.578984375 32.572428571428574 C 151.578984375 30.572428571428574 153.578984375 28.572428571428574 155.578984375 28.572428571428574 L 158.371015625 28.572428571428574 C 160.371015625 28.572428571428574 162.371015625 30.572428571428574 162.371015625 32.572428571428574 L 162.371015625 46.001 C 162.371015625 48.001 160.371015625 50.001 158.371015625 50.001 L 155.578984375 50.001 C 153.578984375 50.001 151.578984375 48.001 151.578984375 46.001 Z "
                                                              pathFrom="M 151.578984375 50.001 L 151.578984375 50.001 L 162.371015625 50.001 L 162.371015625 50.001 L 162.371015625 50.001 L 162.371015625 50.001 L 162.371015625 50.001 L 151.578984375 50.001 Z"
                                                              cy="28.571428571428573" cx="162.371015625"
                                                              j="8" val="3" barHeight="21.428571428571427"
                                                              barWidth="10.79203125"></path>
                                                        <g id="SvgjsG1847"
                                                           class="apexcharts-bar-goals-markers">
                                                            <g id="SvgjsG1849"
                                                               className="apexcharts-bar-goals-groups"
                                                               class="apexcharts-hidden-element-shown"
                                                               clip-path="url(#gridRectMarkerMask166q071x)"></g>
                                                            <g id="SvgjsG1851"
                                                               className="apexcharts-bar-goals-groups"
                                                               class="apexcharts-hidden-element-shown"
                                                               clip-path="url(#gridRectMarkerMask166q071x)"></g>
                                                            <g id="SvgjsG1853"
                                                               className="apexcharts-bar-goals-groups"
                                                               class="apexcharts-hidden-element-shown"
                                                               clip-path="url(#gridRectMarkerMask166q071x)"></g>
                                                            <g id="SvgjsG1855"
                                                               className="apexcharts-bar-goals-groups"
                                                               class="apexcharts-hidden-element-shown"
                                                               clip-path="url(#gridRectMarkerMask166q071x)"></g>
                                                            <g id="SvgjsG1857"
                                                               className="apexcharts-bar-goals-groups"
                                                               class="apexcharts-hidden-element-shown"
                                                               clip-path="url(#gridRectMarkerMask166q071x)"></g>
                                                            <g id="SvgjsG1859"
                                                               className="apexcharts-bar-goals-groups"
                                                               class="apexcharts-hidden-element-shown"
                                                               clip-path="url(#gridRectMarkerMask166q071x)"></g>
                                                            <g id="SvgjsG1861"
                                                               className="apexcharts-bar-goals-groups"
                                                               class="apexcharts-hidden-element-shown"
                                                               clip-path="url(#gridRectMarkerMask166q071x)"></g>
                                                            <g id="SvgjsG1863"
                                                               className="apexcharts-bar-goals-groups"
                                                               class="apexcharts-hidden-element-shown"
                                                               clip-path="url(#gridRectMarkerMask166q071x)"></g>
                                                            <g id="SvgjsG1865"
                                                               className="apexcharts-bar-goals-groups"
                                                               class="apexcharts-hidden-element-shown"
                                                               clip-path="url(#gridRectMarkerMask166q071x)"></g>
                                                        </g>
                                                        <g id="SvgjsG1848"
                                                           class="apexcharts-bar-shadows apexcharts-hidden-element-shown"></g>
                                                    </g>
                                                    <g id="SvgjsG1846"
                                                       class="apexcharts-datalabels apexcharts-hidden-element-shown"
                                                       data:realIndex="0"></g>
                                                </g>
                                                <line id="SvgjsLine1875" x1="-12.5125" y1="0"
                                                      x2="169.48749999999998" y2="0" stroke="#b6b6b6"
                                                      stroke-dasharray="0" stroke-width="1"
                                                      stroke-linecap="butt"
                                                      class="apexcharts-ycrosshairs"></line>
                                                <line id="SvgjsLine1876" x1="-12.5125" y1="0"
                                                      x2="169.48749999999998" y2="0" stroke-dasharray="0"
                                                      stroke-width="0" stroke-linecap="butt"
                                                      class="apexcharts-ycrosshairs-hidden"></line>
                                                <g id="SvgjsG1877" class="apexcharts-xaxis"
                                                   transform="translate(0, 0)">
                                                    <g id="SvgjsG1878" class="apexcharts-xaxis-texts-g"
                                                       transform="translate(0, -4)"></g>
                                                </g>
                                                <g id="SvgjsG1890" class="apexcharts-yaxis-annotations"></g>
                                                <g id="SvgjsG1891" class="apexcharts-xaxis-annotations"></g>
                                                <g id="SvgjsG1892" class="apexcharts-point-annotations"></g>
                                            </g>
                                        </svg>
                                        <div class="apexcharts-tooltip apexcharts-theme-dark">
                                            <div class="apexcharts-tooltip-title"
                                                 style="font-family: inherit; font-size: 12px;"></div>
                                            <div class="apexcharts-tooltip-series-group" style="order: 1;">
                                                            <span class="apexcharts-tooltip-marker"
                                                                  style="background-color: var(--bs-white);"></span>
                                                <div class="apexcharts-tooltip-text"
                                                     style="font-family: inherit; font-size: 12px;">
                                                    <div class="apexcharts-tooltip-y-group"><span
                                                            class="apexcharts-tooltip-text-y-label"></span><span
                                                            class="apexcharts-tooltip-text-y-value"></span>
                                                    </div>
                                                    <div class="apexcharts-tooltip-goals-group"><span
                                                            class="apexcharts-tooltip-text-goals-label"></span><span
                                                            class="apexcharts-tooltip-text-goals-value"></span>
                                                    </div>
                                                    <div class="apexcharts-tooltip-z-group"><span
                                                            class="apexcharts-tooltip-text-z-label"></span><span
                                                            class="apexcharts-tooltip-text-z-value"></span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div
                                            class="apexcharts-yaxistooltip apexcharts-yaxistooltip-0 apexcharts-yaxistooltip-left apexcharts-theme-dark">
                                            <div class="apexcharts-yaxistooltip-text"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-7">
            <!-- -------------------------------------------- -->
            <!-- Revenue Forecast -->
            <!-- -------------------------------------------- -->
            <div class="card">
                <div class="card-body">
                    <div class="d-md-flex align-items-center justify-content-between mb-4">
                        <div class="hstack align-items-center gap-3">
                      <span
                          class="d-flex align-items-center justify-content-center round-48 bg-primary-subtle rounded flex-shrink-0">
                        <iconify-icon icon="solar:layers-linear" class="fs-7 text-primary"></iconify-icon>
                      </span>
                            <div>
                                <h5 class="card-title">Revenue Forecast</h5>
                                <p class="card-subtitle mb-0">Overview of Profit</p>
                            </div>
                        </div>

                        <div class="hstack gap-9 mt-4 mt-md-0">
                            <div class="d-flex align-items-center gap-2">
                                            <span
                                                class="d-block flex-shrink-0 round-8 bg-primary rounded-circle"></span>
                                <span class="text-nowrap text-muted">2024</span>
                            </div>
                            <div class="d-flex align-items-center gap-2">
                                <span class="d-block flex-shrink-0 round-8 bg-danger rounded-circle"></span>
                                <span class="text-nowrap text-muted">2023</span>
                            </div>
                            <div class="d-flex align-items-center gap-2">
                                            <span
                                                class="d-block flex-shrink-0 round-8 bg-secondary rounded-circle"></span>
                                <span class="text-nowrap text-muted">2022</span>
                            </div>
                        </div>
                    </div>
                    <div style="height: 295px;" class="me-n7">
                        <div id="revenue-forecast" style="min-height: 315px;">
                            <div id="apexchartsvykqk73c"
                                 class="apexcharts-canvas apexchartsvykqk73c apexcharts-theme-light"
                                 style="width: 623px; height: 300px;">
                                <svg id="SvgjsSvg1896" width="623" height="300"
                                     xmlns="http://www.w3.org/2000/svg" version="1.1"
                                     xmlns:xlink="http://www.w3.org/1999/xlink"
                                     xmlns:svgjs="http://svgjs.dev"
                                     class="apexcharts-svg apexcharts-zoomable hovering-zoom"
                                     xmlns:data="ApexChartsNS" transform="translate(-10, 0)"
                                     style="background: transparent;">
                                    <foreignObject x="0" y="0" width="623" height="300">
                                        <div class="apexcharts-legend" xmlns="http://www.w3.org/1999/xhtml"
                                             style="max-height: 150px;"></div>
                                    </foreignObject>
                                    <rect id="SvgjsRect1901" width="0" height="0" x="0" y="0" rx="0" ry="0"
                                          opacity="1" stroke-width="0" stroke="none" stroke-dasharray="0"
                                          fill="#fefefe"></rect>
                                    <g id="SvgjsG1982" class="apexcharts-yaxis" rel="0"
                                       transform="translate(14.55999755859375, 0)">
                                        <g id="SvgjsG1983" class="apexcharts-yaxis-texts-g">
                                            <text id="SvgjsText1985" font-family="inherit" x="20" y="31.6"
                                                  text-anchor="end" dominant-baseline="auto"
                                                  font-size="11px" font-weight="400" fill="#adb0bb"
                                                  class="apexcharts-text apexcharts-yaxis-label "
                                                  style="font-family: inherit;">
                                                <tspan id="SvgjsTspan1986">120</tspan>
                                                <title>120</title></text>
                                            <text id="SvgjsText1988" font-family="inherit" x="20"
                                                  y="69.51566666666668" text-anchor="end"
                                                  dominant-baseline="auto" font-size="11px"
                                                  font-weight="400" fill="#adb0bb"
                                                  class="apexcharts-text apexcharts-yaxis-label "
                                                  style="font-family: inherit;">
                                                <tspan id="SvgjsTspan1989">100</tspan>
                                                <title>100</title></text>
                                            <text id="SvgjsText1991" font-family="inherit" x="20"
                                                  y="107.43133333333336" text-anchor="end"
                                                  dominant-baseline="auto" font-size="11px"
                                                  font-weight="400" fill="#adb0bb"
                                                  class="apexcharts-text apexcharts-yaxis-label "
                                                  style="font-family: inherit;">
                                                <tspan id="SvgjsTspan1992">80</tspan>
                                                <title>80</title></text>
                                            <text id="SvgjsText1994" font-family="inherit" x="20"
                                                  y="145.34700000000004" text-anchor="end"
                                                  dominant-baseline="auto" font-size="11px"
                                                  font-weight="400" fill="#adb0bb"
                                                  class="apexcharts-text apexcharts-yaxis-label "
                                                  style="font-family: inherit;">
                                                <tspan id="SvgjsTspan1995">60</tspan>
                                                <title>60</title></text>
                                            <text id="SvgjsText1997" font-family="inherit" x="20"
                                                  y="183.26266666666672" text-anchor="end"
                                                  dominant-baseline="auto" font-size="11px"
                                                  font-weight="400" fill="#adb0bb"
                                                  class="apexcharts-text apexcharts-yaxis-label "
                                                  style="font-family: inherit;">
                                                <tspan id="SvgjsTspan1998">40</tspan>
                                                <title>40</title></text>
                                            <text id="SvgjsText2000" font-family="inherit" x="20"
                                                  y="221.1783333333334" text-anchor="end"
                                                  dominant-baseline="auto" font-size="11px"
                                                  font-weight="400" fill="#adb0bb"
                                                  class="apexcharts-text apexcharts-yaxis-label "
                                                  style="font-family: inherit;">
                                                <tspan id="SvgjsTspan2001">20</tspan>
                                                <title>20</title></text>
                                            <text id="SvgjsText2003" font-family="inherit" x="20"
                                                  y="259.0940000000001" text-anchor="end"
                                                  dominant-baseline="auto" font-size="11px"
                                                  font-weight="400" fill="#adb0bb"
                                                  class="apexcharts-text apexcharts-yaxis-label "
                                                  style="font-family: inherit;">
                                                <tspan id="SvgjsTspan2004">0</tspan>
                                                <title>0</title></text>
                                        </g>
                                    </g>
                                    <g id="SvgjsG1898" class="apexcharts-inner apexcharts-graphical"
                                       transform="translate(44.55999755859375, 30)">
                                        <defs id="SvgjsDefs1897">
                                            <clipPath id="gridRectMaskvykqk73c">
                                                <rect id="SvgjsRect1903" width="562.0181274414062"
                                                      height="239.49400000000003" x="-4" y="-6" rx="0"
                                                      ry="0" opacity="1" stroke-width="0" stroke="none"
                                                      stroke-dasharray="0" fill="#fff"></rect>
                                            </clipPath>
                                            <clipPath id="forecastMaskvykqk73c"></clipPath>
                                            <clipPath id="nonForecastMaskvykqk73c"></clipPath>
                                            <clipPath id="gridRectMarkerMaskvykqk73c">
                                                <rect id="SvgjsRect1904" width="560.0181274414062"
                                                      height="231.49400000000003" x="-2" y="-2" rx="0"
                                                      ry="0" opacity="1" stroke-width="0" stroke="none"
                                                      stroke-dasharray="0" fill="#fff"></rect>
                                            </clipPath>
                                            <linearGradient id="SvgjsLinearGradient1909" x1="0" y1="0"
                                                            x2="0" y2="1">
                                                <stop id="SvgjsStop1910" stop-opacity="0.1"
                                                      stop-color="var(--bs-danger)" offset="1"></stop>
                                                <stop id="SvgjsStop1911" stop-opacity="0.01" stop-color=""
                                                      offset="1"></stop>
                                                <stop id="SvgjsStop1912" stop-opacity="0.01" stop-color=""
                                                      offset="1"></stop>
                                            </linearGradient>
                                            <linearGradient id="SvgjsLinearGradient1918" x1="0" y1="0"
                                                            x2="0" y2="1">
                                                <stop id="SvgjsStop1919" stop-opacity="0.1"
                                                      stop-color="var(--bs-secondary)" offset="1"></stop>
                                                <stop id="SvgjsStop1920" stop-opacity="0.01" stop-color=""
                                                      offset="1"></stop>
                                                <stop id="SvgjsStop1921" stop-opacity="0.01" stop-color=""
                                                      offset="1"></stop>
                                            </linearGradient>
                                            <linearGradient id="SvgjsLinearGradient1927" x1="0" y1="0"
                                                            x2="0" y2="1">
                                                <stop id="SvgjsStop1928" stop-opacity="0.1"
                                                      stop-color="var(--bs-primary)" offset="1"></stop>
                                                <stop id="SvgjsStop1929" stop-opacity="0.01" stop-color=""
                                                      offset="1"></stop>
                                                <stop id="SvgjsStop1930" stop-opacity="0.01" stop-color=""
                                                      offset="1"></stop>
                                            </linearGradient>
                                        </defs>
                                        <line id="SvgjsLine1902" x1="158.36232212611608" y1="0"
                                              x2="158.36232212611608" y2="227.49400000000003"
                                              stroke="#b6b6b6" stroke-dasharray="3" stroke-linecap="butt"
                                              class="apexcharts-xcrosshairs apexcharts-active"
                                              x="158.36232212611608" y="0" width="1"
                                              height="227.49400000000003" fill="#b1b9c4" filter="none"
                                              fill-opacity="0.9" stroke-width="1"></line>
                                        <g id="SvgjsG1933" class="apexcharts-grid">
                                            <g id="SvgjsG1934" class="apexcharts-gridlines-horizontal">
                                                <line id="SvgjsLine1946" x1="0" y1="37.915666666666674"
                                                      x2="556.0181274414062" y2="37.915666666666674"
                                                      stroke="rgba(0,0,0,0.05)" stroke-dasharray="0"
                                                      stroke-linecap="butt"
                                                      class="apexcharts-gridline"></line>
                                                <line id="SvgjsLine1947" x1="0" y1="75.83133333333335"
                                                      x2="556.0181274414062" y2="75.83133333333335"
                                                      stroke="rgba(0,0,0,0.05)" stroke-dasharray="0"
                                                      stroke-linecap="butt"
                                                      class="apexcharts-gridline"></line>
                                                <line id="SvgjsLine1948" x1="0" y1="113.74700000000001"
                                                      x2="556.0181274414062" y2="113.74700000000001"
                                                      stroke="rgba(0,0,0,0.05)" stroke-dasharray="0"
                                                      stroke-linecap="butt"
                                                      class="apexcharts-gridline"></line>
                                                <line id="SvgjsLine1949" x1="0" y1="151.6626666666667"
                                                      x2="556.0181274414062" y2="151.6626666666667"
                                                      stroke="rgba(0,0,0,0.05)" stroke-dasharray="0"
                                                      stroke-linecap="butt"
                                                      class="apexcharts-gridline"></line>
                                                <line id="SvgjsLine1950" x1="0" y1="189.57833333333338"
                                                      x2="556.0181274414062" y2="189.57833333333338"
                                                      stroke="rgba(0,0,0,0.05)" stroke-dasharray="0"
                                                      stroke-linecap="butt"
                                                      class="apexcharts-gridline"></line>
                                                <line id="SvgjsLine1951" x1="0" y1="227.49400000000006"
                                                      x2="556.0181274414062" y2="227.49400000000006"
                                                      stroke="rgba(0,0,0,0.05)" stroke-dasharray="0"
                                                      stroke-linecap="butt"
                                                      class="apexcharts-gridline"></line>
                                            </g>
                                            <g id="SvgjsG1935" class="apexcharts-gridlines-vertical">
                                                <line id="SvgjsLine1937" x1="0" y1="0" x2="0"
                                                      y2="227.49400000000003" stroke="rgba(0,0,0,0.05)"
                                                      stroke-dasharray="0" stroke-linecap="butt"
                                                      class="apexcharts-gridline"></line>
                                                <line id="SvgjsLine1938" x1="79.43116106305804" y1="0"
                                                      x2="79.43116106305804" y2="227.49400000000003"
                                                      stroke="rgba(0,0,0,0.05)" stroke-dasharray="0"
                                                      stroke-linecap="butt"
                                                      class="apexcharts-gridline"></line>
                                                <line id="SvgjsLine1939" x1="158.86232212611608" y1="0"
                                                      x2="158.86232212611608" y2="227.49400000000003"
                                                      stroke="rgba(0,0,0,0.05)" stroke-dasharray="0"
                                                      stroke-linecap="butt"
                                                      class="apexcharts-gridline"></line>
                                                <line id="SvgjsLine1940" x1="238.29348318917414" y1="0"
                                                      x2="238.29348318917414" y2="227.49400000000003"
                                                      stroke="rgba(0,0,0,0.05)" stroke-dasharray="0"
                                                      stroke-linecap="butt"
                                                      class="apexcharts-gridline"></line>
                                                <line id="SvgjsLine1941" x1="317.72464425223217" y1="0"
                                                      x2="317.72464425223217" y2="227.49400000000003"
                                                      stroke="rgba(0,0,0,0.05)" stroke-dasharray="0"
                                                      stroke-linecap="butt"
                                                      class="apexcharts-gridline"></line>
                                                <line id="SvgjsLine1942" x1="397.1558053152902" y1="0"
                                                      x2="397.1558053152902" y2="227.49400000000003"
                                                      stroke="rgba(0,0,0,0.05)" stroke-dasharray="0"
                                                      stroke-linecap="butt"
                                                      class="apexcharts-gridline"></line>
                                                <line id="SvgjsLine1943" x1="476.5869663783482" y1="0"
                                                      x2="476.5869663783482" y2="227.49400000000003"
                                                      stroke="rgba(0,0,0,0.05)" stroke-dasharray="0"
                                                      stroke-linecap="butt"
                                                      class="apexcharts-gridline"></line>
                                                <line id="SvgjsLine1944" x1="556.0181274414062" y1="0"
                                                      x2="556.0181274414062" y2="227.49400000000003"
                                                      stroke="rgba(0,0,0,0.05)" stroke-dasharray="0"
                                                      stroke-linecap="butt"
                                                      class="apexcharts-gridline"></line>
                                            </g>
                                            <line id="SvgjsLine1953" x1="0" y1="227.49400000000003"
                                                  x2="556.0181274414062" y2="227.49400000000003"
                                                  stroke="transparent" stroke-dasharray="0"
                                                  stroke-linecap="butt"></line>
                                            <line id="SvgjsLine1952" x1="0" y1="1" x2="0"
                                                  y2="227.49400000000003" stroke="transparent"
                                                  stroke-dasharray="0" stroke-linecap="butt"></line>
                                        </g>
                                        <g id="SvgjsG1936" class="apexcharts-grid-borders">
                                            <line id="SvgjsLine1945" x1="0" y1="0" x2="556.0181274414062"
                                                  y2="0" stroke="rgba(0,0,0,0.05)" stroke-dasharray="0"
                                                  stroke-linecap="butt" class="apexcharts-gridline"></line>
                                        </g>
                                        <g id="SvgjsG1905"
                                           class="apexcharts-area-series apexcharts-plot-series">
                                            <g id="SvgjsG1906" class="apexcharts-series" zIndex="0"
                                               seriesName="2023" data:longestSeries="true" rel="1"
                                               data:realIndex="0">
                                                <path id="SvgjsPath1913"
                                                      d="M 0 227.49400000000003 L 0 132.70483333333334C12.525057513530298, 129.71547815264526, 54.38104603599746, 107.76828963862387, 79.43116106305804, 113.74700000000003S132.4791978972111, 169.0462850909823, 158.86232212611608, 170.62050000000002S217.74277968380676, 134.26179748632993, 238.2934831891741, 123.22591666666669S291.3415200233272, 86.88446490901772, 317.72464425223217, 85.31025000000002S372.8427418307695, 121.00049785892568, 397.1558053152902, 113.74700000000003S459.0834462038954, 50.44836090553669, 476.5869663783482, 37.91566666666668S545.2362812410736, 5.146605955546165, 556.0181274414062, 2.842170943040401e-14 L 556.0181274414062 227.49400000000003 L 0 227.49400000000003M 0 132.70483333333334z"
                                                      fill="url(#SvgjsLinearGradient1909)" fill-opacity="1"
                                                      stroke-opacity="1" stroke-linecap="butt"
                                                      stroke-width="0" stroke-dasharray="0"
                                                      class="apexcharts-area" index="0"
                                                      clip-path="url(#gridRectMaskvykqk73c)"
                                                      pathTo="M 0 227.49400000000003 L 0 132.70483333333334C12.525057513530298, 129.71547815264526, 54.38104603599746, 107.76828963862387, 79.43116106305804, 113.74700000000003S132.4791978972111, 169.0462850909823, 158.86232212611608, 170.62050000000002S217.74277968380676, 134.26179748632993, 238.2934831891741, 123.22591666666669S291.3415200233272, 86.88446490901772, 317.72464425223217, 85.31025000000002S372.8427418307695, 121.00049785892568, 397.1558053152902, 113.74700000000003S459.0834462038954, 50.44836090553669, 476.5869663783482, 37.91566666666668S545.2362812410736, 5.146605955546165, 556.0181274414062, 2.842170943040401e-14 L 556.0181274414062 227.49400000000003 L 0 227.49400000000003M 0 132.70483333333334z"
                                                      pathFrom="M -1 227.49400000000003 L -1 227.49400000000003 L 79.43116106305804 227.49400000000003 L 158.86232212611608 227.49400000000003 L 238.2934831891741 227.49400000000003 L 317.72464425223217 227.49400000000003 L 397.1558053152902 227.49400000000003 L 476.5869663783482 227.49400000000003 L 556.0181274414062 227.49400000000003"></path>
                                                <path id="SvgjsPath1914"
                                                      d="M 0 132.70483333333334C12.525057513530298, 129.71547815264526, 54.38104603599746, 107.76828963862387, 79.43116106305804, 113.74700000000003S132.4791978972111, 169.0462850909823, 158.86232212611608, 170.62050000000002S217.74277968380676, 134.26179748632993, 238.2934831891741, 123.22591666666669S291.3415200233272, 86.88446490901772, 317.72464425223217, 85.31025000000002S372.8427418307695, 121.00049785892568, 397.1558053152902, 113.74700000000003S459.0834462038954, 50.44836090553669, 476.5869663783482, 37.91566666666668S545.2362812410736, 5.146605955546165, 556.0181274414062, 2.842170943040401e-14"
                                                      fill="none" fill-opacity="1" stroke="var(--bs-danger)"
                                                      stroke-opacity="1" stroke-linecap="butt"
                                                      stroke-width="2" stroke-dasharray="0"
                                                      class="apexcharts-area" index="0"
                                                      clip-path="url(#gridRectMaskvykqk73c)"
                                                      pathTo="M 0 132.70483333333334C12.525057513530298, 129.71547815264526, 54.38104603599746, 107.76828963862387, 79.43116106305804, 113.74700000000003S132.4791978972111, 169.0462850909823, 158.86232212611608, 170.62050000000002S217.74277968380676, 134.26179748632993, 238.2934831891741, 123.22591666666669S291.3415200233272, 86.88446490901772, 317.72464425223217, 85.31025000000002S372.8427418307695, 121.00049785892568, 397.1558053152902, 113.74700000000003S459.0834462038954, 50.44836090553669, 476.5869663783482, 37.91566666666668S545.2362812410736, 5.146605955546165, 556.0181274414062, 2.842170943040401e-14"
                                                      pathFrom="M -1 227.49400000000003 L -1 227.49400000000003 L 79.43116106305804 227.49400000000003 L 158.86232212611608 227.49400000000003 L 238.2934831891741 227.49400000000003 L 317.72464425223217 227.49400000000003 L 397.1558053152902 227.49400000000003 L 476.5869663783482 227.49400000000003 L 556.0181274414062 227.49400000000003"
                                                      fill-rule="evenodd"></path>
                                                <g id="SvgjsG1907"
                                                   class="apexcharts-series-markers-wrap apexcharts-hidden-element-shown"
                                                   data:realIndex="0">
                                                    <g class="apexcharts-series-markers">
                                                        <circle id="SvgjsCircle2008" r="6"
                                                                cx="158.86232212611608"
                                                                cy="170.62050000000002"
                                                                class="apexcharts-marker w0ikrppi no-pointer-events"
                                                                stroke="var(--bs-danger)"
                                                                fill="var(--bs-danger)" fill-opacity="1"
                                                                stroke-width="2" stroke-opacity="0.9"
                                                                default-marker-size="0"></circle>
                                                    </g>
                                                </g>
                                            </g>
                                            <g id="SvgjsG1915" class="apexcharts-series" zIndex="1"
                                               seriesName="2022" data:longestSeries="true" rel="2"
                                               data:realIndex="1">
                                                <path id="SvgjsPath1922"
                                                      d="M 0 227.49400000000003 L 0 161.14158333333336C12.525057513530298, 158.15222815264528, 53.04803683415305, 143.75796490901774, 79.43116106305804, 142.18375000000003S132.4791978972111, 153.2368815756844, 158.86232212611608, 151.6626666666667S211.91035896026912, 131.13061842431563, 238.2934831891741, 132.70483333333334S291.3415200233272, 162.71579824235107, 317.72464425223217, 161.14158333333336S370.7726810863852, 124.80013157568439, 397.1558053152902, 123.22591666666669S450.4816739674842, 148.54739190535352, 476.5869663783482, 151.6626666666667S542.9654812359743, 143.74138738065662, 556.0181274414062, 142.18375000000003 L 556.0181274414062 227.49400000000003 L 0 227.49400000000003M 0 161.14158333333336z"
                                                      fill="url(#SvgjsLinearGradient1918)" fill-opacity="1"
                                                      stroke-opacity="1" stroke-linecap="butt"
                                                      stroke-width="0" stroke-dasharray="0"
                                                      class="apexcharts-area" index="1"
                                                      clip-path="url(#gridRectMaskvykqk73c)"
                                                      pathTo="M 0 227.49400000000003 L 0 161.14158333333336C12.525057513530298, 158.15222815264528, 53.04803683415305, 143.75796490901774, 79.43116106305804, 142.18375000000003S132.4791978972111, 153.2368815756844, 158.86232212611608, 151.6626666666667S211.91035896026912, 131.13061842431563, 238.2934831891741, 132.70483333333334S291.3415200233272, 162.71579824235107, 317.72464425223217, 161.14158333333336S370.7726810863852, 124.80013157568439, 397.1558053152902, 123.22591666666669S450.4816739674842, 148.54739190535352, 476.5869663783482, 151.6626666666667S542.9654812359743, 143.74138738065662, 556.0181274414062, 142.18375000000003 L 556.0181274414062 227.49400000000003 L 0 227.49400000000003M 0 161.14158333333336z"
                                                      pathFrom="M -1 227.49400000000003 L -1 227.49400000000003 L 79.43116106305804 227.49400000000003 L 158.86232212611608 227.49400000000003 L 238.2934831891741 227.49400000000003 L 317.72464425223217 227.49400000000003 L 397.1558053152902 227.49400000000003 L 476.5869663783482 227.49400000000003 L 556.0181274414062 227.49400000000003"></path>
                                                <path id="SvgjsPath1923"
                                                      d="M 0 161.14158333333336C12.525057513530298, 158.15222815264528, 53.04803683415305, 143.75796490901774, 79.43116106305804, 142.18375000000003S132.4791978972111, 153.2368815756844, 158.86232212611608, 151.6626666666667S211.91035896026912, 131.13061842431563, 238.2934831891741, 132.70483333333334S291.3415200233272, 162.71579824235107, 317.72464425223217, 161.14158333333336S370.7726810863852, 124.80013157568439, 397.1558053152902, 123.22591666666669S450.4816739674842, 148.54739190535352, 476.5869663783482, 151.6626666666667S542.9654812359743, 143.74138738065662, 556.0181274414062, 142.18375000000003"
                                                      fill="none" fill-opacity="1"
                                                      stroke="var(--bs-secondary)" stroke-opacity="1"
                                                      stroke-linecap="butt" stroke-width="2"
                                                      stroke-dasharray="0" class="apexcharts-area" index="1"
                                                      clip-path="url(#gridRectMaskvykqk73c)"
                                                      pathTo="M 0 161.14158333333336C12.525057513530298, 158.15222815264528, 53.04803683415305, 143.75796490901774, 79.43116106305804, 142.18375000000003S132.4791978972111, 153.2368815756844, 158.86232212611608, 151.6626666666667S211.91035896026912, 131.13061842431563, 238.2934831891741, 132.70483333333334S291.3415200233272, 162.71579824235107, 317.72464425223217, 161.14158333333336S370.7726810863852, 124.80013157568439, 397.1558053152902, 123.22591666666669S450.4816739674842, 148.54739190535352, 476.5869663783482, 151.6626666666667S542.9654812359743, 143.74138738065662, 556.0181274414062, 142.18375000000003"
                                                      pathFrom="M -1 227.49400000000003 L -1 227.49400000000003 L 79.43116106305804 227.49400000000003 L 158.86232212611608 227.49400000000003 L 238.2934831891741 227.49400000000003 L 317.72464425223217 227.49400000000003 L 397.1558053152902 227.49400000000003 L 476.5869663783482 227.49400000000003 L 556.0181274414062 227.49400000000003"
                                                      fill-rule="evenodd"></path>
                                                <g id="SvgjsG1916"
                                                   class="apexcharts-series-markers-wrap apexcharts-hidden-element-shown"
                                                   data:realIndex="1">
                                                    <g class="apexcharts-series-markers">
                                                        <circle id="SvgjsCircle2009" r="6"
                                                                cx="158.86232212611608"
                                                                cy="151.6626666666667"
                                                                class="apexcharts-marker wwm2aj12j no-pointer-events"
                                                                stroke="var(--bs-secondary)"
                                                                fill="var(--bs-secondary)" fill-opacity="1"
                                                                stroke-width="2" stroke-opacity="0.9"
                                                                default-marker-size="0"></circle>
                                                    </g>
                                                </g>
                                            </g>
                                            <g id="SvgjsG1924" class="apexcharts-series" zIndex="2"
                                               seriesName="2024" data:longestSeries="true" rel="3"
                                               data:realIndex="2">
                                                <path id="SvgjsPath1931"
                                                      d="M 0 227.49400000000003 L 0 37.91566666666668C9.762774314901355, 43.74086945408147, 53.76388271090346, 80.75122280773688, 79.43116106305804, 85.31025000000002S134.7180542371707, 68.32644124534698, 158.86232212611608, 75.83133333333336S220.78996301472128, 139.1299724277967, 238.2934831891741, 151.6626666666667S291.24759056454616, 189.57833333333338, 317.72464425223217, 189.57833333333338S372.1056902882296, 145.68395630529054, 397.1558053152902, 151.6626666666667S450.93194793517284, 222.90168801594882, 476.5869663783482, 227.49400000000003S546.2553531265049, 185.92461945408147, 556.0181274414062, 180.09941666666668 L 556.0181274414062 227.49400000000003 L 0 227.49400000000003M 0 37.91566666666668z"
                                                      fill="url(#SvgjsLinearGradient1927)" fill-opacity="1"
                                                      stroke-opacity="1" stroke-linecap="butt"
                                                      stroke-width="0" stroke-dasharray="0"
                                                      class="apexcharts-area" index="2"
                                                      clip-path="url(#gridRectMaskvykqk73c)"
                                                      pathTo="M 0 227.49400000000003 L 0 37.91566666666668C9.762774314901355, 43.74086945408147, 53.76388271090346, 80.75122280773688, 79.43116106305804, 85.31025000000002S134.7180542371707, 68.32644124534698, 158.86232212611608, 75.83133333333336S220.78996301472128, 139.1299724277967, 238.2934831891741, 151.6626666666667S291.24759056454616, 189.57833333333338, 317.72464425223217, 189.57833333333338S372.1056902882296, 145.68395630529054, 397.1558053152902, 151.6626666666667S450.93194793517284, 222.90168801594882, 476.5869663783482, 227.49400000000003S546.2553531265049, 185.92461945408147, 556.0181274414062, 180.09941666666668 L 556.0181274414062 227.49400000000003 L 0 227.49400000000003M 0 37.91566666666668z"
                                                      pathFrom="M -1 227.49400000000003 L -1 227.49400000000003 L 79.43116106305804 227.49400000000003 L 158.86232212611608 227.49400000000003 L 238.2934831891741 227.49400000000003 L 317.72464425223217 227.49400000000003 L 397.1558053152902 227.49400000000003 L 476.5869663783482 227.49400000000003 L 556.0181274414062 227.49400000000003"></path>
                                                <path id="SvgjsPath1932"
                                                      d="M 0 37.91566666666668C9.762774314901355, 43.74086945408147, 53.76388271090346, 80.75122280773688, 79.43116106305804, 85.31025000000002S134.7180542371707, 68.32644124534698, 158.86232212611608, 75.83133333333336S220.78996301472128, 139.1299724277967, 238.2934831891741, 151.6626666666667S291.24759056454616, 189.57833333333338, 317.72464425223217, 189.57833333333338S372.1056902882296, 145.68395630529054, 397.1558053152902, 151.6626666666667S450.93194793517284, 222.90168801594882, 476.5869663783482, 227.49400000000003S546.2553531265049, 185.92461945408147, 556.0181274414062, 180.09941666666668"
                                                      fill="none" fill-opacity="1"
                                                      stroke="var(--bs-primary)" stroke-opacity="1"
                                                      stroke-linecap="butt" stroke-width="2"
                                                      stroke-dasharray="0" class="apexcharts-area" index="2"
                                                      clip-path="url(#gridRectMaskvykqk73c)"
                                                      pathTo="M 0 37.91566666666668C9.762774314901355, 43.74086945408147, 53.76388271090346, 80.75122280773688, 79.43116106305804, 85.31025000000002S134.7180542371707, 68.32644124534698, 158.86232212611608, 75.83133333333336S220.78996301472128, 139.1299724277967, 238.2934831891741, 151.6626666666667S291.24759056454616, 189.57833333333338, 317.72464425223217, 189.57833333333338S372.1056902882296, 145.68395630529054, 397.1558053152902, 151.6626666666667S450.93194793517284, 222.90168801594882, 476.5869663783482, 227.49400000000003S546.2553531265049, 185.92461945408147, 556.0181274414062, 180.09941666666668"
                                                      pathFrom="M -1 227.49400000000003 L -1 227.49400000000003 L 79.43116106305804 227.49400000000003 L 158.86232212611608 227.49400000000003 L 238.2934831891741 227.49400000000003 L 317.72464425223217 227.49400000000003 L 397.1558053152902 227.49400000000003 L 476.5869663783482 227.49400000000003 L 556.0181274414062 227.49400000000003"
                                                      fill-rule="evenodd"></path>
                                                <g id="SvgjsG1925"
                                                   class="apexcharts-series-markers-wrap apexcharts-hidden-element-shown"
                                                   data:realIndex="2">
                                                    <g class="apexcharts-series-markers">
                                                        <circle id="SvgjsCircle2010" r="6"
                                                                cx="158.86232212611608"
                                                                cy="75.83133333333336"
                                                                class="apexcharts-marker w4hqger78 no-pointer-events"
                                                                stroke="var(--bs-primary)"
                                                                fill="var(--bs-primary)" fill-opacity="1"
                                                                stroke-width="2" stroke-opacity="0.9"
                                                                default-marker-size="0"></circle>
                                                    </g>
                                                </g>
                                            </g>
                                            <g id="SvgjsG1908" class="apexcharts-datalabels"
                                               data:realIndex="0"></g>
                                            <g id="SvgjsG1917" class="apexcharts-datalabels"
                                               data:realIndex="1"></g>
                                            <g id="SvgjsG1926" class="apexcharts-datalabels"
                                               data:realIndex="2"></g>
                                        </g>
                                        <line id="SvgjsLine1954" x1="0" y1="0" x2="556.0181274414062" y2="0"
                                              stroke="#b6b6b6" stroke-dasharray="0" stroke-width="1"
                                              stroke-linecap="butt" class="apexcharts-ycrosshairs"></line>
                                        <line id="SvgjsLine1955" x1="0" y1="0" x2="556.0181274414062" y2="0"
                                              stroke-dasharray="0" stroke-width="0" stroke-linecap="butt"
                                              class="apexcharts-ycrosshairs-hidden"></line>
                                        <g id="SvgjsG1956" class="apexcharts-xaxis"
                                           transform="translate(0, 0)">
                                            <g id="SvgjsG1957" class="apexcharts-xaxis-texts-g"
                                               transform="translate(0, -4)">
                                                <text id="SvgjsText1959" font-family="inherit" x="0"
                                                      y="256.494" text-anchor="middle"
                                                      dominant-baseline="auto" font-size="12px"
                                                      font-weight="400" fill="#adb0bb"
                                                      class="apexcharts-text apexcharts-xaxis-label "
                                                      style="font-family: inherit;">
                                                    <tspan id="SvgjsTspan1960">Jan</tspan>
                                                    <title>Jan</title></text>
                                                <text id="SvgjsText1962" font-family="inherit"
                                                      x="79.43116106305806" y="256.494" text-anchor="middle"
                                                      dominant-baseline="auto" font-size="12px"
                                                      font-weight="400" fill="#adb0bb"
                                                      class="apexcharts-text apexcharts-xaxis-label "
                                                      style="font-family: inherit;">
                                                    <tspan id="SvgjsTspan1963">Feb</tspan>
                                                    <title>Feb</title></text>
                                                <text id="SvgjsText1965" font-family="inherit"
                                                      x="158.86232212611608" y="256.494"
                                                      text-anchor="middle" dominant-baseline="auto"
                                                      font-size="12px" font-weight="400" fill="#adb0bb"
                                                      class="apexcharts-text apexcharts-xaxis-label "
                                                      style="font-family: inherit;">
                                                    <tspan id="SvgjsTspan1966">Mar</tspan>
                                                    <title>Mar</title></text>
                                                <text id="SvgjsText1968" font-family="inherit"
                                                      x="238.2934831891741" y="256.494" text-anchor="middle"
                                                      dominant-baseline="auto" font-size="12px"
                                                      font-weight="400" fill="#adb0bb"
                                                      class="apexcharts-text apexcharts-xaxis-label "
                                                      style="font-family: inherit;">
                                                    <tspan id="SvgjsTspan1969">Apr</tspan>
                                                    <title>Apr</title></text>
                                                <text id="SvgjsText1971" font-family="inherit"
                                                      x="317.7246442522321" y="256.494" text-anchor="middle"
                                                      dominant-baseline="auto" font-size="12px"
                                                      font-weight="400" fill="#adb0bb"
                                                      class="apexcharts-text apexcharts-xaxis-label "
                                                      style="font-family: inherit;">
                                                    <tspan id="SvgjsTspan1972">May</tspan>
                                                    <title>May</title></text>
                                                <text id="SvgjsText1974" font-family="inherit"
                                                      x="397.15580531529014" y="256.494"
                                                      text-anchor="middle" dominant-baseline="auto"
                                                      font-size="12px" font-weight="400" fill="#adb0bb"
                                                      class="apexcharts-text apexcharts-xaxis-label "
                                                      style="font-family: inherit;">
                                                    <tspan id="SvgjsTspan1975">Jun</tspan>
                                                    <title>Jun</title></text>
                                                <text id="SvgjsText1977" font-family="inherit"
                                                      x="476.5869663783482" y="256.494" text-anchor="middle"
                                                      dominant-baseline="auto" font-size="12px"
                                                      font-weight="400" fill="#adb0bb"
                                                      class="apexcharts-text apexcharts-xaxis-label "
                                                      style="font-family: inherit;">
                                                    <tspan id="SvgjsTspan1978">July</tspan>
                                                    <title>July</title></text>
                                                <text id="SvgjsText1980" font-family="inherit"
                                                      x="556.0181274414064" y="256.494" text-anchor="middle"
                                                      dominant-baseline="auto" font-size="12px"
                                                      font-weight="400" fill="#adb0bb"
                                                      class="apexcharts-text apexcharts-xaxis-label "
                                                      style="font-family: inherit;">
                                                    <tspan id="SvgjsTspan1981">Aug</tspan>
                                                    <title>Aug</title></text>
                                            </g>
                                        </g>
                                        <g id="SvgjsG2005" class="apexcharts-yaxis-annotations"></g>
                                        <g id="SvgjsG2006" class="apexcharts-xaxis-annotations"></g>
                                        <g id="SvgjsG2007" class="apexcharts-point-annotations"></g>
                                        <rect id="SvgjsRect2011" width="0" height="0" x="0" y="0" rx="0"
                                              ry="0" opacity="1" stroke-width="0" stroke="none"
                                              stroke-dasharray="0" fill="#fefefe"
                                              class="apexcharts-zoom-rect"></rect>
                                        <rect id="SvgjsRect2012" width="0" height="0" x="0" y="0" rx="0"
                                              ry="0" opacity="1" stroke-width="0" stroke="none"
                                              stroke-dasharray="0" fill="#fefefe"
                                              class="apexcharts-selection-rect"></rect>
                                    </g>
                                </svg>
                                <div class="apexcharts-tooltip apexcharts-theme-dark apexcharts-active"
                                     style="left: 214.422px; top: 116.494px;">
                                    <div class="apexcharts-tooltip-title"
                                         style="font-family: inherit; font-size: 12px;">Mar
                                    </div>
                                    <div class="apexcharts-tooltip-series-group apexcharts-active"
                                         style="order: 1; display: flex;"><span
                                            class="apexcharts-tooltip-marker"
                                            style="background-color: var(--bs-danger);"></span>
                                        <div class="apexcharts-tooltip-text"
                                             style="font-family: inherit; font-size: 12px;">
                                            <div class="apexcharts-tooltip-y-group"><span
                                                    class="apexcharts-tooltip-text-y-label">2023: </span><span
                                                    class="apexcharts-tooltip-text-y-value">30</span></div>
                                            <div class="apexcharts-tooltip-goals-group"><span
                                                    class="apexcharts-tooltip-text-goals-label"></span><span
                                                    class="apexcharts-tooltip-text-goals-value"></span>
                                            </div>
                                            <div class="apexcharts-tooltip-z-group"><span
                                                    class="apexcharts-tooltip-text-z-label"></span><span
                                                    class="apexcharts-tooltip-text-z-value"></span></div>
                                        </div>
                                    </div>
                                    <div class="apexcharts-tooltip-series-group apexcharts-active"
                                         style="order: 2; display: flex;"><span
                                            class="apexcharts-tooltip-marker"
                                            style="background-color: var(--bs-secondary);"></span>
                                        <div class="apexcharts-tooltip-text"
                                             style="font-family: inherit; font-size: 12px;">
                                            <div class="apexcharts-tooltip-y-group"><span
                                                    class="apexcharts-tooltip-text-y-label">2022: </span><span
                                                    class="apexcharts-tooltip-text-y-value">40</span></div>
                                            <div class="apexcharts-tooltip-goals-group"><span
                                                    class="apexcharts-tooltip-text-goals-label"></span><span
                                                    class="apexcharts-tooltip-text-goals-value"></span>
                                            </div>
                                            <div class="apexcharts-tooltip-z-group"><span
                                                    class="apexcharts-tooltip-text-z-label"></span><span
                                                    class="apexcharts-tooltip-text-z-value"></span></div>
                                        </div>
                                    </div>
                                    <div class="apexcharts-tooltip-series-group apexcharts-active"
                                         style="order: 3; display: flex;"><span
                                            class="apexcharts-tooltip-marker"
                                            style="background-color: var(--bs-primary);"></span>
                                        <div class="apexcharts-tooltip-text"
                                             style="font-family: inherit; font-size: 12px;">
                                            <div class="apexcharts-tooltip-y-group"><span
                                                    class="apexcharts-tooltip-text-y-label">2024: </span><span
                                                    class="apexcharts-tooltip-text-y-value">80</span></div>
                                            <div class="apexcharts-tooltip-goals-group"><span
                                                    class="apexcharts-tooltip-text-goals-label"></span><span
                                                    class="apexcharts-tooltip-text-goals-value"></span>
                                            </div>
                                            <div class="apexcharts-tooltip-z-group"><span
                                                    class="apexcharts-tooltip-text-z-label"></span><span
                                                    class="apexcharts-tooltip-text-z-value"></span></div>
                                        </div>
                                    </div>
                                </div>
                                <div
                                    class="apexcharts-xaxistooltip apexcharts-xaxistooltip-bottom apexcharts-theme-dark apexcharts-active"
                                    style="left: 181.383px; top: 259.494px;">
                                    <div class="apexcharts-xaxistooltip-text"
                                         style="font-family: inherit; font-size: 12px; min-width: 19.356px;">
                                        Mar
                                    </div>
                                </div>
                                <div
                                    class="apexcharts-yaxistooltip apexcharts-yaxistooltip-0 apexcharts-yaxistooltip-left apexcharts-theme-dark">
                                    <div class="apexcharts-yaxistooltip-text"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-5">
            <!-- -------------------------------------------- -->
            <!-- Your Performance -->
            <!-- -------------------------------------------- -->
            <div class="card">
                <div class="card-body">
                    <h5 class="card-title fw-semibold">Your Performance</h5>
                    <p class="card-subtitle mb-0">Last check on 25 february</p>

                    <div class="row mt-4">
                        <div class="col-md-6">
                            <div class="vstack gap-9 mt-2">
                                <div class="hstack align-items-center gap-3">
                                    <div
                                        class="d-flex align-items-center justify-content-center round-48 rounded bg-primary-subtle">
                                        <iconify-icon icon="solar:shop-2-linear"
                                                      class="fs-7 text-primary"></iconify-icon>
                                    </div>
                                    <div>
                                        <h6 class="mb-0">64 new orders</h6>
                                        <span>Processing</span>
                                    </div>

                                </div>
                                <div class="hstack align-items-center gap-3">
                                    <div
                                        class="d-flex align-items-center justify-content-center round-48 rounded bg-danger-subtle">
                                        <iconify-icon icon="solar:filters-outline"
                                                      class="fs-7 text-danger"></iconify-icon>
                                    </div>
                                    <div>
                                        <h6 class="mb-0">4 orders</h6>
                                        <span>On hold</span>
                                    </div>

                                </div>
                                <div class="hstack align-items-center gap-3">
                                    <div
                                        class="d-flex align-items-center justify-content-center round-48 rounded bg-secondary-subtle">
                                        <iconify-icon icon="solar:pills-3-linear"
                                                      class="fs-7 text-secondary"></iconify-icon>
                                    </div>
                                    <div>
                                        <h6 class="mb-0">12 orders</h6>
                                        <span>Delivered</span>
                                    </div>

                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="text-center">
                                <div id="your-preformance" style="min-height: 108.7px;">
                                    <div id="apexchartsw6h7ajjkk"
                                         class="apexcharts-canvas apexchartsw6h7ajjkk apexcharts-theme-light"
                                         style="width: 184px; height: 108.7px;">
                                        <svg id="SvgjsSvg2013" width="184" height="108.69999999999999"
                                             xmlns="http://www.w3.org/2000/svg" version="1.1"
                                             xmlns:xlink="http://www.w3.org/1999/xlink"
                                             xmlns:svgjs="http://svgjs.dev" class="apexcharts-svg"
                                             xmlns:data="ApexChartsNS" transform="translate(0, 0)"
                                             style="background: transparent;">
                                            <foreignObject x="0" y="0" width="184"
                                                           height="108.69999999999999">
                                                <div class="apexcharts-legend"
                                                     xmlns="http://www.w3.org/1999/xhtml"></div>
                                            </foreignObject>
                                            <g id="SvgjsG2015" class="apexcharts-inner apexcharts-graphical"
                                               transform="translate(5, 10)">
                                                <defs id="SvgjsDefs2014">
                                                    <clipPath id="gridRectMaskw6h7ajjkk">
                                                        <rect id="SvgjsRect2016" width="182" height="290"
                                                              x="-4" y="-6" rx="0" ry="0" opacity="1"
                                                              stroke-width="0" stroke="none"
                                                              stroke-dasharray="0" fill="#fff"></rect>
                                                    </clipPath>
                                                    <clipPath id="forecastMaskw6h7ajjkk"></clipPath>
                                                    <clipPath id="nonForecastMaskw6h7ajjkk"></clipPath>
                                                    <clipPath id="gridRectMarkerMaskw6h7ajjkk">
                                                        <rect id="SvgjsRect2017" width="180" height="282"
                                                              x="-2" y="-2" rx="0" ry="0" opacity="1"
                                                              stroke-width="0" stroke="none"
                                                              stroke-dasharray="0" fill="#fff"></rect>
                                                    </clipPath>
                                                </defs>
                                                <g id="SvgjsG2018" class="apexcharts-pie">
                                                    <g id="SvgjsG2019" transform="translate(0, 0) scale(1)">
                                                        <circle id="SvgjsCircle2020" r="71.86829268292684"
                                                                cx="88" cy="88" fill="transparent"></circle>
                                                        <g id="SvgjsG2021" class="apexcharts-slices">
                                                            <g id="SvgjsG2022"
                                                               class="apexcharts-series apexcharts-pie-series"
                                                               seriesName="245" rel="1" data:realIndex="0">
                                                                <path id="SvgjsPath2023"
                                                                      d="M 8.146341463414629 87.99999999999999 A 79.85365853658537 79.85365853658537 0 0 1 23.397033180888343 41.063197170596155 L 29.85732986279951 45.75687745353654 A 71.86829268292684 71.86829268292684 0 0 0 16.131707317073165 87.99999999999999 L 8.146341463414629 87.99999999999999 z "
                                                                      fill="var(--bs-danger)"
                                                                      fill-opacity="1" stroke-opacity="1"
                                                                      stroke-linecap="butt" stroke-width="2"
                                                                      stroke-dasharray="0"
                                                                      class="apexcharts-pie-area apexcharts-donut-slice-0"
                                                                      index="0" j="0" data:angle="36"
                                                                      data:startAngle="-90"
                                                                      data:strokeWidth="2" data:value="20"
                                                                      data:pathOrig="M 8.146341463414629 87.99999999999999 A 79.85365853658537 79.85365853658537 0 0 1 23.397033180888343 41.063197170596155 L 29.85732986279951 45.75687745353654 A 71.86829268292684 71.86829268292684 0 0 0 16.131707317073165 87.99999999999999 L 8.146341463414629 87.99999999999999 z "
                                                                      stroke="var(--bs-card-bg)"></path>
                                                            </g>
                                                            <g id="SvgjsG2024"
                                                               class="apexcharts-series apexcharts-pie-series"
                                                               seriesName="45" rel="2" data:realIndex="1">
                                                                <path id="SvgjsPath2025"
                                                                      d="M 23.397033180888343 41.063197170596155 A 79.85365853658537 79.85365853658537 0 0 1 63.323862449181036 12.05465769877236 L 65.79147620426292 19.64919192889512 A 71.86829268292684 71.86829268292684 0 0 0 29.85732986279951 45.75687745353654 L 23.397033180888343 41.063197170596155 z "
                                                                      fill="var(--bs-warning)"
                                                                      fill-opacity="1" stroke-opacity="1"
                                                                      stroke-linecap="butt" stroke-width="2"
                                                                      stroke-dasharray="0"
                                                                      class="apexcharts-pie-area apexcharts-donut-slice-1"
                                                                      index="0" j="1" data:angle="36"
                                                                      data:startAngle="-54"
                                                                      data:strokeWidth="2" data:value="20"
                                                                      data:pathOrig="M 23.397033180888343 41.063197170596155 A 79.85365853658537 79.85365853658537 0 0 1 63.323862449181036 12.05465769877236 L 65.79147620426292 19.64919192889512 A 71.86829268292684 71.86829268292684 0 0 0 29.85732986279951 45.75687745353654 L 23.397033180888343 41.063197170596155 z "
                                                                      stroke="var(--bs-card-bg)"></path>
                                                            </g>
                                                            <g id="SvgjsG2026"
                                                               class="apexcharts-series apexcharts-pie-series"
                                                               seriesName="14" rel="3" data:realIndex="2">
                                                                <path id="SvgjsPath2027"
                                                                      d="M 63.323862449181036 12.05465769877236 A 79.85365853658537 79.85365853658537 0 0 1 112.67613755081898 12.054657698772374 L 110.20852379573708 19.649191928895135 A 71.86829268292684 71.86829268292684 0 0 0 65.79147620426292 19.64919192889512 L 63.323862449181036 12.05465769877236 z "
                                                                      fill="var(--bs-warning-bg-subtle)"
                                                                      fill-opacity="1" stroke-opacity="1"
                                                                      stroke-linecap="butt" stroke-width="2"
                                                                      stroke-dasharray="0"
                                                                      class="apexcharts-pie-area apexcharts-donut-slice-2"
                                                                      index="0" j="2" data:angle="36"
                                                                      data:startAngle="-18"
                                                                      data:strokeWidth="2" data:value="20"
                                                                      data:pathOrig="M 63.323862449181036 12.05465769877236 A 79.85365853658537 79.85365853658537 0 0 1 112.67613755081898 12.054657698772374 L 110.20852379573708 19.649191928895135 A 71.86829268292684 71.86829268292684 0 0 0 65.79147620426292 19.64919192889512 L 63.323862449181036 12.05465769877236 z "
                                                                      stroke="var(--bs-card-bg)"></path>
                                                            </g>
                                                            <g id="SvgjsG2028"
                                                               class="apexcharts-series apexcharts-pie-series"
                                                               seriesName="78" rel="4" data:realIndex="3">
                                                                <path id="SvgjsPath2029"
                                                                      d="M 112.67613755081898 12.054657698772374 A 79.85365853658537 79.85365853658537 0 0 1 152.60296681911166 41.06319717059617 L 146.1426701372005 45.756877453536546 A 71.86829268292684 71.86829268292684 0 0 0 110.20852379573708 19.649191928895135 L 112.67613755081898 12.054657698772374 z "
                                                                      fill="var(--bs-secondary-bg-subtle)"
                                                                      fill-opacity="1" stroke-opacity="1"
                                                                      stroke-linecap="butt" stroke-width="2"
                                                                      stroke-dasharray="0"
                                                                      class="apexcharts-pie-area apexcharts-donut-slice-3"
                                                                      index="0" j="3" data:angle="36"
                                                                      data:startAngle="18"
                                                                      data:strokeWidth="2" data:value="20"
                                                                      data:pathOrig="M 112.67613755081898 12.054657698772374 A 79.85365853658537 79.85365853658537 0 0 1 152.60296681911166 41.06319717059617 L 146.1426701372005 45.756877453536546 A 71.86829268292684 71.86829268292684 0 0 0 110.20852379573708 19.649191928895135 L 112.67613755081898 12.054657698772374 z "
                                                                      stroke="var(--bs-card-bg)"></path>
                                                            </g>
                                                            <g id="SvgjsG2030"
                                                               class="apexcharts-series apexcharts-pie-series"
                                                               seriesName="95" rel="5" data:realIndex="4">
                                                                <path id="SvgjsPath2031"
                                                                      d="M 152.60296681911166 41.06319717059617 A 79.85365853658537 79.85365853658537 0 0 1 167.8536573203446 87.98606290745849 L 159.86829158831014 87.98745661671263 A 71.86829268292684 71.86829268292684 0 0 0 146.1426701372005 45.756877453536546 L 152.60296681911166 41.06319717059617 z "
                                                                      fill="var(--bs-secondary)"
                                                                      fill-opacity="1" stroke-opacity="1"
                                                                      stroke-linecap="butt" stroke-width="2"
                                                                      stroke-dasharray="0"
                                                                      class="apexcharts-pie-area apexcharts-donut-slice-4"
                                                                      index="0" j="4" data:angle="36"
                                                                      data:startAngle="54"
                                                                      data:strokeWidth="2" data:value="20"
                                                                      data:pathOrig="M 152.60296681911166 41.06319717059617 A 79.85365853658537 79.85365853658537 0 0 1 167.8536573203446 87.98606290745849 L 159.86829158831014 87.98745661671263 A 71.86829268292684 71.86829268292684 0 0 0 146.1426701372005 45.756877453536546 L 152.60296681911166 41.06319717059617 z "
                                                                      stroke="var(--bs-card-bg)"></path>
                                                            </g>
                                                        </g>
                                                    </g>
                                                </g>
                                                <line id="SvgjsLine2032" x1="0" y1="0" x2="176" y2="0"
                                                      stroke="#b6b6b6" stroke-dasharray="0" stroke-width="1"
                                                      stroke-linecap="butt"
                                                      class="apexcharts-ycrosshairs"></line>
                                                <line id="SvgjsLine2033" x1="0" y1="0" x2="176" y2="0"
                                                      stroke-dasharray="0" stroke-width="0"
                                                      stroke-linecap="butt"
                                                      class="apexcharts-ycrosshairs-hidden"></line>
                                            </g>
                                        </svg>
                                        <div class="apexcharts-tooltip apexcharts-theme-dark">
                                            <div class="apexcharts-tooltip-series-group" style="order: 1;">
                                                            <span class="apexcharts-tooltip-marker"
                                                                  style="background-color: var(--bs-danger);"></span>
                                                <div class="apexcharts-tooltip-text"
                                                     style="font-family: inherit; font-size: 12px;">
                                                    <div class="apexcharts-tooltip-y-group"><span
                                                            class="apexcharts-tooltip-text-y-label"></span><span
                                                            class="apexcharts-tooltip-text-y-value"></span>
                                                    </div>
                                                    <div class="apexcharts-tooltip-goals-group"><span
                                                            class="apexcharts-tooltip-text-goals-label"></span><span
                                                            class="apexcharts-tooltip-text-goals-value"></span>
                                                    </div>
                                                    <div class="apexcharts-tooltip-z-group"><span
                                                            class="apexcharts-tooltip-text-z-label"></span><span
                                                            class="apexcharts-tooltip-text-z-value"></span>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="apexcharts-tooltip-series-group" style="order: 2;">
                                                            <span class="apexcharts-tooltip-marker"
                                                                  style="background-color: var(--bs-warning);"></span>
                                                <div class="apexcharts-tooltip-text"
                                                     style="font-family: inherit; font-size: 12px;">
                                                    <div class="apexcharts-tooltip-y-group"><span
                                                            class="apexcharts-tooltip-text-y-label"></span><span
                                                            class="apexcharts-tooltip-text-y-value"></span>
                                                    </div>
                                                    <div class="apexcharts-tooltip-goals-group"><span
                                                            class="apexcharts-tooltip-text-goals-label"></span><span
                                                            class="apexcharts-tooltip-text-goals-value"></span>
                                                    </div>
                                                    <div class="apexcharts-tooltip-z-group"><span
                                                            class="apexcharts-tooltip-text-z-label"></span><span
                                                            class="apexcharts-tooltip-text-z-value"></span>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="apexcharts-tooltip-series-group" style="order: 3;">
                                                            <span class="apexcharts-tooltip-marker"
                                                                  style="background-color: var(--bs-warning-bg-subtle);"></span>
                                                <div class="apexcharts-tooltip-text"
                                                     style="font-family: inherit; font-size: 12px;">
                                                    <div class="apexcharts-tooltip-y-group"><span
                                                            class="apexcharts-tooltip-text-y-label"></span><span
                                                            class="apexcharts-tooltip-text-y-value"></span>
                                                    </div>
                                                    <div class="apexcharts-tooltip-goals-group"><span
                                                            class="apexcharts-tooltip-text-goals-label"></span><span
                                                            class="apexcharts-tooltip-text-goals-value"></span>
                                                    </div>
                                                    <div class="apexcharts-tooltip-z-group"><span
                                                            class="apexcharts-tooltip-text-z-label"></span><span
                                                            class="apexcharts-tooltip-text-z-value"></span>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="apexcharts-tooltip-series-group" style="order: 4;">
                                                            <span class="apexcharts-tooltip-marker"
                                                                  style="background-color: var(--bs-secondary-bg-subtle);"></span>
                                                <div class="apexcharts-tooltip-text"
                                                     style="font-family: inherit; font-size: 12px;">
                                                    <div class="apexcharts-tooltip-y-group"><span
                                                            class="apexcharts-tooltip-text-y-label"></span><span
                                                            class="apexcharts-tooltip-text-y-value"></span>
                                                    </div>
                                                    <div class="apexcharts-tooltip-goals-group"><span
                                                            class="apexcharts-tooltip-text-goals-label"></span><span
                                                            class="apexcharts-tooltip-text-goals-value"></span>
                                                    </div>
                                                    <div class="apexcharts-tooltip-z-group"><span
                                                            class="apexcharts-tooltip-text-z-label"></span><span
                                                            class="apexcharts-tooltip-text-z-value"></span>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="apexcharts-tooltip-series-group" style="order: 5;">
                                                            <span class="apexcharts-tooltip-marker"
                                                                  style="background-color: var(--bs-secondary);"></span>
                                                <div class="apexcharts-tooltip-text"
                                                     style="font-family: inherit; font-size: 12px;">
                                                    <div class="apexcharts-tooltip-y-group"><span
                                                            class="apexcharts-tooltip-text-y-label"></span><span
                                                            class="apexcharts-tooltip-text-y-value"></span>
                                                    </div>
                                                    <div class="apexcharts-tooltip-goals-group"><span
                                                            class="apexcharts-tooltip-text-goals-label"></span><span
                                                            class="apexcharts-tooltip-text-goals-value"></span>
                                                    </div>
                                                    <div class="apexcharts-tooltip-z-group"><span
                                                            class="apexcharts-tooltip-text-z-label"></span><span
                                                            class="apexcharts-tooltip-text-z-value"></span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <h2 class="fs-8">275</h2>
                                <p class="mb-0 fs-2">
                                    Learn insigs how to manage all aspects of your
                                    startup.
                                </p>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
        <div class="col-lg-7">
            <div class="row">
                <div class="col-md-6">
                    <!-- -------------------------------------------- -->
                    <!-- Customers -->
                    <!-- -------------------------------------------- -->
                    <div class="card">
                        <div class="card-body">
                            <div class="d-flex align-items-start justify-content-between">
                                <div>
                                    <h5 class="card-title fw-semibold">Customers</h5>
                                    <p class="card-subtitle mb-0">Last 7 days</p>
                                </div>
                                <span class="fs-11 text-success fw-semibold lh-lg">+26.5%</span>
                            </div>
                            <div class="py-4 my-1">
                                <div id="customers-area" style="min-height: 100px;">
                                    <div id="apexcharts17sll3om"
                                         class="apexcharts-canvas apexcharts17sll3om apexcharts-theme-light"
                                         style="width: 251px; height: 100px;">
                                        <svg id="SvgjsSvg2036" width="251" height="100"
                                             xmlns="http://www.w3.org/2000/svg" version="1.1"
                                             xmlns:xlink="http://www.w3.org/1999/xlink"
                                             xmlns:svgjs="http://svgjs.dev" class="apexcharts-svg"
                                             xmlns:data="ApexChartsNS" transform="translate(0, 0)"
                                             style="background: transparent;">
                                            <foreignObject x="0" y="0" width="251" height="100">
                                                <div class="apexcharts-legend"
                                                     xmlns="http://www.w3.org/1999/xhtml"
                                                     style="max-height: 50px;"></div>
                                            </foreignObject>
                                            <rect id="SvgjsRect2040" width="0" height="0" x="0" y="0" rx="0"
                                                  ry="0" opacity="1" stroke-width="0" stroke="none"
                                                  stroke-dasharray="0" fill="#fefefe"></rect>
                                            <g id="SvgjsG2072" class="apexcharts-yaxis" rel="0"
                                               transform="translate(-18, 0)"></g>
                                            <g id="SvgjsG2038" class="apexcharts-inner apexcharts-graphical"
                                               transform="translate(0, 1)">
                                                <defs id="SvgjsDefs2037">
                                                    <clipPath id="gridRectMask17sll3om">
                                                        <rect id="SvgjsRect2042" width="257" height="110"
                                                              x="-4" y="-6" rx="0" ry="0" opacity="1"
                                                              stroke-width="0" stroke="none"
                                                              stroke-dasharray="0" fill="#fff"></rect>
                                                    </clipPath>
                                                    <clipPath id="forecastMask17sll3om"></clipPath>
                                                    <clipPath id="nonForecastMask17sll3om"></clipPath>
                                                    <clipPath id="gridRectMarkerMask17sll3om">
                                                        <rect id="SvgjsRect2043" width="255" height="102"
                                                              x="-2" y="-2" rx="0" ry="0" opacity="1"
                                                              stroke-width="0" stroke="none"
                                                              stroke-dasharray="0" fill="#fff"></rect>
                                                    </clipPath>
                                                </defs>
                                                <line id="SvgjsLine2041" x1="0" y1="0" x2="0" y2="98"
                                                      stroke="#b6b6b6" stroke-dasharray="3"
                                                      stroke-linecap="butt" class="apexcharts-xcrosshairs"
                                                      x="0" y="0" width="1" height="98" fill="#b1b9c4"
                                                      filter="none" fill-opacity="0.9"
                                                      stroke-width="1"></line>
                                                <g id="SvgjsG2053" class="apexcharts-grid">
                                                    <g id="SvgjsG2054"
                                                       class="apexcharts-gridlines-horizontal"
                                                       style="display: none;">
                                                        <line id="SvgjsLine2057" x1="0" y1="0" x2="251"
                                                              y2="0" stroke="#e0e0e0" stroke-dasharray="0"
                                                              stroke-linecap="butt"
                                                              class="apexcharts-gridline"></line>
                                                        <line id="SvgjsLine2058" x1="0" y1="98" x2="251"
                                                              y2="98" stroke="#e0e0e0" stroke-dasharray="0"
                                                              stroke-linecap="butt"
                                                              class="apexcharts-gridline"></line>
                                                    </g>
                                                    <g id="SvgjsG2055" class="apexcharts-gridlines-vertical"
                                                       style="display: none;"></g>
                                                    <line id="SvgjsLine2060" x1="0" y1="98" x2="251" y2="98"
                                                          stroke="transparent" stroke-dasharray="0"
                                                          stroke-linecap="butt"></line>
                                                    <line id="SvgjsLine2059" x1="0" y1="1" x2="0" y2="98"
                                                          stroke="transparent" stroke-dasharray="0"
                                                          stroke-linecap="butt"></line>
                                                </g>
                                                <g id="SvgjsG2056" class="apexcharts-grid-borders"
                                                   style="display: none;"></g>
                                                <g id="SvgjsG2044"
                                                   class="apexcharts-line-series apexcharts-plot-series">
                                                    <g id="SvgjsG2045" class="apexcharts-series" zIndex="0"
                                                       seriesName="Aprilx07x" data:longestSeries="true"
                                                       rel="1" data:realIndex="0">
                                                        <path id="SvgjsPath2048"
                                                              d="M 0 98C 14.641666666666666 98 27.19166666666667 32.66666666666667 41.833333333333336 32.66666666666667C 56.475 32.66666666666667 69.025 49 83.66666666666667 49C 98.30833333333334 49 110.85833333333333 35.93333333333334 125.5 35.93333333333334C 140.14166666666668 35.93333333333334 152.69166666666666 52.266666666666666 167.33333333333334 52.266666666666666C 181.975 52.266666666666666 194.525 16.33333333333333 209.16666666666666 16.33333333333333C 223.80833333333334 16.33333333333333 236.35833333333332 0 251 0"
                                                              fill="none" fill-opacity="1"
                                                              stroke="var(--bs-primary)" stroke-opacity="1"
                                                              stroke-linecap="butt" stroke-width="2"
                                                              stroke-dasharray="0" class="apexcharts-line"
                                                              index="0"
                                                              clip-path="url(#gridRectMask17sll3om)"
                                                              pathTo="M 0 98C 14.641666666666666 98 27.19166666666667 32.66666666666667 41.833333333333336 32.66666666666667C 56.475 32.66666666666667 69.025 49 83.66666666666667 49C 98.30833333333334 49 110.85833333333333 35.93333333333334 125.5 35.93333333333334C 140.14166666666668 35.93333333333334 152.69166666666666 52.266666666666666 167.33333333333334 52.266666666666666C 181.975 52.266666666666666 194.525 16.33333333333333 209.16666666666666 16.33333333333333C 223.80833333333334 16.33333333333333 236.35833333333332 0 251 0"
                                                              pathFrom="M -1 98 L -1 98 L 41.833333333333336 98 L 83.66666666666667 98 L 125.5 98 L 167.33333333333334 98 L 209.16666666666666 98 L 251 98"
                                                              fill-rule="evenodd"></path>
                                                        <g id="SvgjsG2046"
                                                           class="apexcharts-series-markers-wrap apexcharts-hidden-element-shown"
                                                           data:realIndex="0">
                                                            <g class="apexcharts-series-markers">
                                                                <circle id="SvgjsCircle2076" r="0" cx="0"
                                                                        cy="0"
                                                                        class="apexcharts-marker wgimiw7rd no-pointer-events"
                                                                        stroke="transparent"
                                                                        fill="var(--bs-primary)"
                                                                        fill-opacity="1" stroke-width="2"
                                                                        stroke-opacity="0.9"
                                                                        default-marker-size="0"></circle>
                                                            </g>
                                                        </g>
                                                    </g>
                                                    <g id="SvgjsG2049" class="apexcharts-series" zIndex="1"
                                                       seriesName="LastxWeek" data:longestSeries="true"
                                                       rel="2" data:realIndex="1">
                                                        <path id="SvgjsPath2052"
                                                              d="M 0 98C 14.641666666666666 98 27.19166666666667 71.86666666666667 41.833333333333336 71.86666666666667C 56.475 71.86666666666667 69.025 35.93333333333334 83.66666666666667 35.93333333333334C 98.30833333333334 35.93333333333334 110.85833333333333 55.53333333333333 125.5 55.53333333333333C 140.14166666666668 55.53333333333333 152.69166666666666 13.066666666666663 167.33333333333334 13.066666666666663C 181.975 13.066666666666663 194.525 45.733333333333334 209.16666666666666 45.733333333333334C 223.80833333333334 45.733333333333334 236.35833333333332 16.33333333333333 251 16.33333333333333"
                                                              fill="none" fill-opacity="1"
                                                              stroke="var(--bs-primary-bg-subtle)"
                                                              stroke-opacity="1" stroke-linecap="butt"
                                                              stroke-width="2" stroke-dasharray="0"
                                                              class="apexcharts-line" index="1"
                                                              clip-path="url(#gridRectMask17sll3om)"
                                                              pathTo="M 0 98C 14.641666666666666 98 27.19166666666667 71.86666666666667 41.833333333333336 71.86666666666667C 56.475 71.86666666666667 69.025 35.93333333333334 83.66666666666667 35.93333333333334C 98.30833333333334 35.93333333333334 110.85833333333333 55.53333333333333 125.5 55.53333333333333C 140.14166666666668 55.53333333333333 152.69166666666666 13.066666666666663 167.33333333333334 13.066666666666663C 181.975 13.066666666666663 194.525 45.733333333333334 209.16666666666666 45.733333333333334C 223.80833333333334 45.733333333333334 236.35833333333332 16.33333333333333 251 16.33333333333333"
                                                              pathFrom="M -1 98 L -1 98 L 41.833333333333336 98 L 83.66666666666667 98 L 125.5 98 L 167.33333333333334 98 L 209.16666666666666 98 L 251 98"
                                                              fill-rule="evenodd"></path>
                                                        <g id="SvgjsG2050"
                                                           class="apexcharts-series-markers-wrap apexcharts-hidden-element-shown"
                                                           data:realIndex="1">
                                                            <g class="apexcharts-series-markers">
                                                                <circle id="SvgjsCircle2077" r="0" cx="0"
                                                                        cy="0"
                                                                        class="apexcharts-marker wcfna8bngk no-pointer-events"
                                                                        stroke="transparent"
                                                                        fill="var(--bs-primary-bg-subtle)"
                                                                        fill-opacity="1" stroke-width="2"
                                                                        stroke-opacity="0.9"
                                                                        default-marker-size="0"></circle>
                                                            </g>
                                                        </g>
                                                    </g>
                                                    <g id="SvgjsG2047" class="apexcharts-datalabels"
                                                       data:realIndex="0"></g>
                                                    <g id="SvgjsG2051" class="apexcharts-datalabels"
                                                       data:realIndex="1"></g>
                                                </g>
                                                <line id="SvgjsLine2061" x1="0" y1="0" x2="251" y2="0"
                                                      stroke="#b6b6b6" stroke-dasharray="0" stroke-width="1"
                                                      stroke-linecap="butt"
                                                      class="apexcharts-ycrosshairs"></line>
                                                <line id="SvgjsLine2062" x1="0" y1="0" x2="251" y2="0"
                                                      stroke-dasharray="0" stroke-width="0"
                                                      stroke-linecap="butt"
                                                      class="apexcharts-ycrosshairs-hidden"></line>
                                                <g id="SvgjsG2063" class="apexcharts-xaxis"
                                                   transform="translate(0, 0)">
                                                    <g id="SvgjsG2064" class="apexcharts-xaxis-texts-g"
                                                       transform="translate(0, -4)"></g>
                                                </g>
                                                <g id="SvgjsG2073" class="apexcharts-yaxis-annotations"></g>
                                                <g id="SvgjsG2074" class="apexcharts-xaxis-annotations"></g>
                                                <g id="SvgjsG2075" class="apexcharts-point-annotations"></g>
                                            </g>
                                        </svg>
                                        <div class="apexcharts-tooltip apexcharts-theme-dark">
                                            <div class="apexcharts-tooltip-series-group" style="order: 1;">
                                                            <span class="apexcharts-tooltip-marker"
                                                                  style="background-color: var(--bs-primary);"></span>
                                                <div class="apexcharts-tooltip-text"
                                                     style="font-family: inherit; font-size: 12px;">
                                                    <div class="apexcharts-tooltip-y-group"><span
                                                            class="apexcharts-tooltip-text-y-label"></span><span
                                                            class="apexcharts-tooltip-text-y-value"></span>
                                                    </div>
                                                    <div class="apexcharts-tooltip-goals-group"><span
                                                            class="apexcharts-tooltip-text-goals-label"></span><span
                                                            class="apexcharts-tooltip-text-goals-value"></span>
                                                    </div>
                                                    <div class="apexcharts-tooltip-z-group"><span
                                                            class="apexcharts-tooltip-text-z-label"></span><span
                                                            class="apexcharts-tooltip-text-z-value"></span>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="apexcharts-tooltip-series-group" style="order: 2;">
                                                            <span class="apexcharts-tooltip-marker"
                                                                  style="background-color: var(--bs-primary-bg-subtle);"></span>
                                                <div class="apexcharts-tooltip-text"
                                                     style="font-family: inherit; font-size: 12px;">
                                                    <div class="apexcharts-tooltip-y-group"><span
                                                            class="apexcharts-tooltip-text-y-label"></span><span
                                                            class="apexcharts-tooltip-text-y-value"></span>
                                                    </div>
                                                    <div class="apexcharts-tooltip-goals-group"><span
                                                            class="apexcharts-tooltip-text-goals-label"></span><span
                                                            class="apexcharts-tooltip-text-goals-value"></span>
                                                    </div>
                                                    <div class="apexcharts-tooltip-z-group"><span
                                                            class="apexcharts-tooltip-text-z-label"></span><span
                                                            class="apexcharts-tooltip-text-z-value"></span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div
                                            class="apexcharts-yaxistooltip apexcharts-yaxistooltip-0 apexcharts-yaxistooltip-left apexcharts-theme-dark">
                                            <div class="apexcharts-yaxistooltip-text"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="d-flex flex-column align-items-center gap-2 w-100 mt-3">
                                <div class="d-flex align-items-center gap-2 w-100">
                                                <span
                                                    class="d-block flex-shrink-0 round-8 bg-primary rounded-circle"></span>
                                    <h6 class="fs-3 fw-normal text-muted mb-0">April 07 - April 14</h6>
                                    <h6 class="fs-3 fw-normal mb-0 ms-auto text-muted">6,380</h6>
                                </div>
                                <div class="d-flex align-items-center gap-2 w-100">
                                                <span
                                                    class="d-block flex-shrink-0 round-8 bg-light rounded-circle"></span>
                                    <h6 class="fs-3 fw-normal text-muted mb-0">Last Week</h6>
                                    <h6 class="fs-3 fw-normal mb-0 ms-auto text-muted">4,298</h6>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <!-- -------------------------------------------- -->
                    <!-- Sales Overview -->
                    <!-- -------------------------------------------- -->
                    <div class="card">
                        <div class="card-body">
                            <h5 class="card-title fw-semibold">Sales Overview</h5>
                            <p class="card-subtitle mb-1">Last 7 days</p>

                            <div class="position-relative labels-chart">
                                <span class="fs-11 label-1">0%</span>
                                <span class="fs-11 label-2">25%</span>
                                <span class="fs-11 label-3">50%</span>
                                <span class="fs-11 label-4">75%</span>
                                <div id="sales-overview" style="min-height: 210.75px;">
                                    <div id="apexcharts1yyx7qhp"
                                         class="apexcharts-canvas apexcharts1yyx7qhp apexcharts-theme-light"
                                         style="width: 251px; height: 210.75px;">
                                        <svg id="SvgjsSvg2078" width="251" height="210.75"
                                             xmlns="http://www.w3.org/2000/svg" version="1.1"
                                             xmlns:xlink="http://www.w3.org/1999/xlink"
                                             xmlns:svgjs="http://svgjs.dev" class="apexcharts-svg"
                                             xmlns:data="ApexChartsNS" transform="translate(0, 0)"
                                             style="background: transparent;">
                                            <foreignObject x="0" y="0" width="251" height="210.75">
                                                <div class="apexcharts-legend"
                                                     xmlns="http://www.w3.org/1999/xhtml"></div>
                                            </foreignObject>
                                            <g id="SvgjsG2080" class="apexcharts-inner apexcharts-graphical"
                                               transform="translate(23.5, 0)">
                                                <defs id="SvgjsDefs2079">
                                                    <clipPath id="gridRectMask1yyx7qhp">
                                                        <rect id="SvgjsRect2081" width="211" height="236"
                                                              x="-3" y="-4" rx="0" ry="0" opacity="1"
                                                              stroke-width="0" stroke="none"
                                                              stroke-dasharray="0" fill="#fff"></rect>
                                                    </clipPath>
                                                    <clipPath id="forecastMask1yyx7qhp"></clipPath>
                                                    <clipPath id="nonForecastMask1yyx7qhp"></clipPath>
                                                    <clipPath id="gridRectMarkerMask1yyx7qhp">
                                                        <rect id="SvgjsRect2082" width="210" height="232"
                                                              x="-2" y="-2" rx="0" ry="0" opacity="1"
                                                              stroke-width="0" stroke="none"
                                                              stroke-dasharray="0" fill="#fff"></rect>
                                                    </clipPath>
                                                </defs>
                                                <g id="SvgjsG2083" class="apexcharts-radialbar">
                                                    <g id="SvgjsG2084">
                                                        <g id="SvgjsG2085" class="apexcharts-tracks">
                                                            <g id="SvgjsG2086"
                                                               class="apexcharts-radialbar-track apexcharts-track"
                                                               rel="1">
                                                                <path id="apexcharts-radialbarTrack-0"
                                                                      d="M 103 26.496951219512184 A 76.50304878048782 76.50304878048782 0 1 1 26.496951219512184 103.00000000000001 "
                                                                      fill="none" fill-opacity="1"
                                                                      stroke="rgba(242,242,242,0.85)"
                                                                      stroke-opacity="1"
                                                                      stroke-linecap="round"
                                                                      stroke-width="9.043475609756099"
                                                                      stroke-dasharray="0"
                                                                      class="apexcharts-radialbar-area"
                                                                      data:pathOrig="M 103 26.496951219512184 A 76.50304878048782 76.50304878048782 0 1 1 26.496951219512184 103.00000000000001 "></path>
                                                            </g>
                                                            <g id="SvgjsG2088"
                                                               class="apexcharts-radialbar-track apexcharts-track"
                                                               rel="2">
                                                                <path id="apexcharts-radialbarTrack-1"
                                                                      d="M 103 40.820121951219505 A 62.179878048780495 62.179878048780495 0 1 1 40.820121951219505 103.00000000000001 "
                                                                      fill="none" fill-opacity="1"
                                                                      stroke="rgba(242,242,242,0.85)"
                                                                      stroke-opacity="1"
                                                                      stroke-linecap="round"
                                                                      stroke-width="9.043475609756099"
                                                                      stroke-dasharray="0"
                                                                      class="apexcharts-radialbar-area"
                                                                      data:pathOrig="M 103 40.820121951219505 A 62.179878048780495 62.179878048780495 0 1 1 40.820121951219505 103.00000000000001 "></path>
                                                            </g>
                                                            <g id="SvgjsG2090"
                                                               class="apexcharts-radialbar-track apexcharts-track"
                                                               rel="3">
                                                                <path id="apexcharts-radialbarTrack-2"
                                                                      d="M 103 55.14329268292683 A 47.85670731707317 47.85670731707317 0 1 1 55.14329268292683 103 "
                                                                      fill="none" fill-opacity="1"
                                                                      stroke="rgba(242,242,242,0.85)"
                                                                      stroke-opacity="1"
                                                                      stroke-linecap="round"
                                                                      stroke-width="9.043475609756099"
                                                                      stroke-dasharray="0"
                                                                      class="apexcharts-radialbar-area"
                                                                      data:pathOrig="M 103 55.14329268292683 A 47.85670731707317 47.85670731707317 0 1 1 55.14329268292683 103 "></path>
                                                            </g>
                                                        </g>
                                                        <g id="SvgjsG2092">
                                                            <g id="SvgjsG2094"
                                                               class="apexcharts-series apexcharts-radial-series"
                                                               seriesName="36x" rel="1" data:realIndex="0">
                                                                <path id="SvgjsPath2095"
                                                                      d="M 103 26.496951219512184 A 76.50304878048782 76.50304878048782 0 0 1 157.0958245741282 157.09582457412816 "
                                                                      fill="none" fill-opacity="0.85"
                                                                      stroke="var(--bs-primary)"
                                                                      stroke-opacity="1"
                                                                      stroke-linecap="round"
                                                                      stroke-width="9.323170731707318"
                                                                      stroke-dasharray="0"
                                                                      class="apexcharts-radialbar-area apexcharts-radialbar-slice-0"
                                                                      data:angle="135" data:value="50"
                                                                      index="0" j="0"
                                                                      data:pathOrig="M 103 26.496951219512184 A 76.50304878048782 76.50304878048782 0 0 1 157.0958245741282 157.09582457412816 "></path>
                                                            </g>
                                                            <g id="SvgjsG2096"
                                                               class="apexcharts-series apexcharts-radial-series"
                                                               seriesName="10x" rel="2" data:realIndex="1">
                                                                <path id="SvgjsPath2097"
                                                                      d="M 103 40.820121951219505 A 62.179878048780495 62.179878048780495 0 1 1 66.45158469358236 153.30457804962518 "
                                                                      fill="none" fill-opacity="0.85"
                                                                      stroke="var(--bs-secondary)"
                                                                      stroke-opacity="1"
                                                                      stroke-linecap="round"
                                                                      stroke-width="9.323170731707318"
                                                                      stroke-dasharray="0"
                                                                      class="apexcharts-radialbar-area apexcharts-radialbar-slice-1"
                                                                      data:angle="216" data:value="80"
                                                                      index="0" j="1"
                                                                      data:pathOrig="M 103 40.820121951219505 A 62.179878048780495 62.179878048780495 0 1 1 66.45158469358236 153.30457804962518 "></path>
                                                            </g>
                                                            <g id="SvgjsG2098"
                                                               class="apexcharts-series apexcharts-radial-series"
                                                               seriesName="36x" rel="3" data:realIndex="2">
                                                                <path id="SvgjsPath2099"
                                                                      d="M 103 55.14329268292683 A 47.85670731707317 47.85670731707317 0 0 1 150.26751183634718 95.51356159226675 "
                                                                      fill="none" fill-opacity="0.85"
                                                                      stroke="var(--bs-danger)"
                                                                      stroke-opacity="1"
                                                                      stroke-linecap="round"
                                                                      stroke-width="9.323170731707318"
                                                                      stroke-dasharray="0"
                                                                      class="apexcharts-radialbar-area apexcharts-radialbar-slice-2"
                                                                      data:angle="81" data:value="30"
                                                                      index="0" j="2"
                                                                      data:pathOrig="M 103 55.14329268292683 A 47.85670731707317 47.85670731707317 0 0 1 150.26751183634718 95.51356159226675 "></path>
                                                            </g>
                                                            <circle id="SvgjsCircle2093"
                                                                    r="42.33496951219514" cx="103" cy="103"
                                                                    class="apexcharts-radialbar-hollow"
                                                                    fill="transparent"></circle>
                                                        </g>
                                                    </g>
                                                </g>
                                                <line id="SvgjsLine2100" x1="0" y1="0" x2="206" y2="0"
                                                      stroke="#b6b6b6" stroke-dasharray="0" stroke-width="1"
                                                      stroke-linecap="butt"
                                                      class="apexcharts-ycrosshairs"></line>
                                                <line id="SvgjsLine2101" x1="0" y1="0" x2="206" y2="0"
                                                      stroke-dasharray="0" stroke-width="0"
                                                      stroke-linecap="butt"
                                                      class="apexcharts-ycrosshairs-hidden"></line>
                                            </g>
                                        </svg>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>

{{--        <div class="col-lg-8">--}}
{{--            <!-- -------------------------------------------- -->--}}
{{--            <!-- Revenue by Product -->--}}
{{--            <!-- -------------------------------------------- -->--}}
{{--            <div class="card">--}}
{{--                <div class="card-body">--}}
{{--                    <div class="d-flex flex-wrap gap-3 mb-2 justify-content-between align-items-center">--}}
{{--                        <h5 class="card-title fw-semibold mb-0">Revenue by Product</h5>--}}
{{--                        <select class="form-select w-auto fw-semibold">--}}
{{--                            <option value="1">Sep 2024</option>--}}
{{--                            <option value="2">Oct 2024</option>--}}
{{--                            <option value="3">Nov 2024</option>--}}
{{--                        </select>--}}
{{--                    </div>--}}

{{--                    <div class="table-responsive">--}}
{{--                        <ul class="nav nav-tabs theme-tab gap-3 flex-nowrap" role="tablist">--}}
{{--                            <li class="nav-item" role="presentation">--}}
{{--                                <a class="nav-link active" data-bs-toggle="tab" href="#app" role="tab"--}}
{{--                                   aria-selected="true">--}}
{{--                                    <div class="hstack gap-2">--}}
{{--                                        <iconify-icon icon="solar:widget-linear"--}}
{{--                                                      class="fs-4"></iconify-icon>--}}
{{--                                        <span>App</span>--}}
{{--                                    </div>--}}

{{--                                </a>--}}
{{--                            </li>--}}
{{--                            <li class="nav-item" role="presentation">--}}
{{--                                <a class="nav-link" data-bs-toggle="tab" href="#mobile" role="tab"--}}
{{--                                   aria-selected="false" tabindex="-1">--}}
{{--                                    <div class="hstack gap-2">--}}
{{--                                        <iconify-icon icon="solar:smartphone-line-duotone"--}}
{{--                                                      class="fs-4"></iconify-icon>--}}
{{--                                        <span>Mobile</span>--}}
{{--                                    </div>--}}
{{--                                </a>--}}
{{--                            </li>--}}
{{--                            <li class="nav-item" role="presentation">--}}
{{--                                <a class="nav-link" data-bs-toggle="tab" href="#saas" role="tab"--}}
{{--                                   aria-selected="false" tabindex="-1">--}}
{{--                                    <div class="hstack gap-2">--}}
{{--                                        <iconify-icon icon="solar:calculator-linear"--}}
{{--                                                      class="fs-4"></iconify-icon>--}}
{{--                                        <span>SaaS</span>--}}
{{--                                    </div>--}}
{{--                                </a>--}}
{{--                            </li>--}}
{{--                            <li class="nav-item" role="presentation">--}}
{{--                                <a class="nav-link" data-bs-toggle="tab" href="#other" role="tab"--}}
{{--                                   aria-selected="false" tabindex="-1">--}}
{{--                                    <div class="hstack gap-2">--}}
{{--                                        <iconify-icon icon="solar:folder-open-outline"--}}
{{--                                                      class="fs-4"></iconify-icon>--}}
{{--                                        <span>Others</span>--}}
{{--                                    </div>--}}
{{--                                </a>--}}
{{--                            </li>--}}
{{--                        </ul>--}}
{{--                    </div>--}}
{{--                    <div class="tab-content mb-n3">--}}
{{--                        <div class="tab-pane active" id="app" role="tabpanel">--}}
{{--                            <div class="table-responsive" data-simplebar="init">--}}
{{--                                <div class="simplebar-wrapper" style="margin: 0px;">--}}
{{--                                    <div class="simplebar-height-auto-observer-wrapper">--}}
{{--                                        <div class="simplebar-height-auto-observer"></div>--}}
{{--                                    </div>--}}
{{--                                    <div class="simplebar-mask">--}}
{{--                                        <div class="simplebar-offset" style="right: 0px; bottom: 0px;">--}}
{{--                                            <div class="simplebar-content-wrapper" tabindex="0"--}}
{{--                                                 role="region" aria-label="scrollable content"--}}
{{--                                                 style="height: auto; overflow: hidden;">--}}
{{--                                                <div class="simplebar-content" style="padding: 0px;">--}}
{{--                                                    <table--}}
{{--                                                        class="table text-nowrap align-middle table-custom mb-0 last-items-borderless">--}}
{{--                                                        <thead>--}}
{{--                                                        <tr>--}}
{{--                                                            <th scope="col"--}}
{{--                                                                class="text-dark fw-normal ps-0">Assigned--}}
{{--                                                            </th>--}}
{{--                                                            <th scope="col" class="text-dark fw-normal">--}}
{{--                                                                Progress--}}
{{--                                                            </th>--}}
{{--                                                            <th scope="col" class="text-dark fw-normal">--}}
{{--                                                                Priority--}}
{{--                                                            </th>--}}
{{--                                                            <th scope="col" class="text-dark fw-normal">--}}
{{--                                                                Budget--}}
{{--                                                            </th>--}}
{{--                                                        </tr>--}}
{{--                                                        </thead>--}}
{{--                                                        <tbody>--}}
{{--                                                        <tr>--}}
{{--                                                            <td class="ps-0">--}}
{{--                                                                <div--}}
{{--                                                                    class="d-flex align-items-center gap-6">--}}
{{--                                                                    <img--}}
{{--                                                                        src="../assets/images/products/dash-prd-1.jpg"--}}
{{--                                                                        alt="prd1" width="48"--}}
{{--                                                                        class="rounded">--}}
{{--                                                                    <div>--}}
{{--                                                                        <h6 class="mb-0">Minecraf App</h6>--}}
{{--                                                                        <span>Jason Roy</span>--}}
{{--                                                                    </div>--}}
{{--                                                                </div>--}}
{{--                                                            </td>--}}
{{--                                                            <td>--}}
{{--                                                                <span>73.2%</span>--}}
{{--                                                            </td>--}}
{{--                                                            <td>--}}
{{--                                                                            <span--}}
{{--                                                                                class="badge bg-success-subtle text-success">Low</span>--}}
{{--                                                            </td>--}}
{{--                                                            <td>--}}
{{--                                                                <span class="text-dark">$3.5k</span>--}}
{{--                                                            </td>--}}
{{--                                                        </tr>--}}
{{--                                                        <tr>--}}
{{--                                                            <td class="ps-0">--}}
{{--                                                                <div--}}
{{--                                                                    class="d-flex align-items-center gap-6">--}}
{{--                                                                    <img--}}
{{--                                                                        src="../assets/images/products/dash-prd-2.jpg"--}}
{{--                                                                        alt="prd1" width="48"--}}
{{--                                                                        class="rounded">--}}
{{--                                                                    <div>--}}
{{--                                                                        <h6 class="mb-0">Web App--}}
{{--                                                                            Project</h6>--}}
{{--                                                                        <span>Mathew Flintoff</span>--}}
{{--                                                                    </div>--}}
{{--                                                                </div>--}}
{{--                                                            </td>--}}
{{--                                                            <td>--}}
{{--                                                                <span>73.2%</span>--}}
{{--                                                            </td>--}}
{{--                                                            <td>--}}
{{--                                                                            <span--}}
{{--                                                                                class="badge bg-warning-subtle text-warning">Medium</span>--}}
{{--                                                            </td>--}}
{{--                                                            <td>--}}
{{--                                                                <span class="text-dark">$3.5k</span>--}}
{{--                                                            </td>--}}
{{--                                                        </tr>--}}
{{--                                                        <tr>--}}
{{--                                                            <td class="ps-0">--}}
{{--                                                                <div--}}
{{--                                                                    class="d-flex align-items-center gap-6">--}}
{{--                                                                    <img--}}
{{--                                                                        src="../assets/images/products/dash-prd-3.jpg"--}}
{{--                                                                        alt="prd1" width="48"--}}
{{--                                                                        class="rounded">--}}
{{--                                                                    <div>--}}
{{--                                                                        <h6 class="mb-0">Modernize--}}
{{--                                                                            Dashboard</h6>--}}
{{--                                                                        <span>Anil Kumar</span>--}}
{{--                                                                    </div>--}}
{{--                                                                </div>--}}
{{--                                                            </td>--}}
{{--                                                            <td>--}}
{{--                                                                <span>73.2%</span>--}}
{{--                                                            </td>--}}
{{--                                                            <td>--}}
{{--                                <span class="badge bg-secondary-subtle text-secondary">Very--}}
{{--                                  High</span>--}}
{{--                                                            </td>--}}
{{--                                                            <td>--}}
{{--                                                                <span class="text-dark">$3.5k</span>--}}
{{--                                                            </td>--}}
{{--                                                        </tr>--}}
{{--                                                        <tr>--}}
{{--                                                            <td class="ps-0">--}}
{{--                                                                <div--}}
{{--                                                                    class="d-flex align-items-center gap-6">--}}
{{--                                                                    <img--}}
{{--                                                                        src="../assets/images/products/dash-prd-4.jpg"--}}
{{--                                                                        alt="prd1" width="48"--}}
{{--                                                                        class="rounded">--}}
{{--                                                                    <div>--}}
{{--                                                                        <h6 class="mb-0">Dashboard Co</h6>--}}
{{--                                                                        <span>George Cruize</span>--}}
{{--                                                                    </div>--}}
{{--                                                                </div>--}}
{{--                                                            </td>--}}
{{--                                                            <td>--}}
{{--                                                                <span>73.2%</span>--}}
{{--                                                            </td>--}}
{{--                                                            <td>--}}
{{--                                                                            <span--}}
{{--                                                                                class="badge bg-danger-subtle text-danger">High</span>--}}
{{--                                                            </td>--}}
{{--                                                            <td>--}}
{{--                                                                <span class="text-dark">$3.5k</span>--}}
{{--                                                            </td>--}}
{{--                                                        </tr>--}}
{{--                                                        </tbody>--}}
{{--                                                    </table>--}}
{{--                                                </div>--}}
{{--                                            </div>--}}
{{--                                        </div>--}}
{{--                                    </div>--}}
{{--                                    <div class="simplebar-placeholder"--}}
{{--                                         style="width: 690px; height: 357px;"></div>--}}
{{--                                </div>--}}
{{--                                <div class="simplebar-track simplebar-horizontal"--}}
{{--                                     style="visibility: hidden;">--}}
{{--                                    <div class="simplebar-scrollbar"--}}
{{--                                         style="width: 0px; display: none; transform: translate3d(0px, 0px, 0px);"></div>--}}
{{--                                </div>--}}
{{--                                <div class="simplebar-track simplebar-vertical" style="visibility: hidden;">--}}
{{--                                    <div class="simplebar-scrollbar"--}}
{{--                                         style="height: 0px; display: none;"></div>--}}
{{--                                </div>--}}
{{--                            </div>--}}
{{--                        </div>--}}
{{--                        <div class="tab-pane" id="mobile" role="tabpanel">--}}
{{--                            <div class="table-responsive" data-simplebar="init">--}}
{{--                                <div class="simplebar-wrapper" style="margin: 0px;">--}}
{{--                                    <div class="simplebar-height-auto-observer-wrapper">--}}
{{--                                        <div class="simplebar-height-auto-observer"></div>--}}
{{--                                    </div>--}}
{{--                                    <div class="simplebar-mask">--}}
{{--                                        <div class="simplebar-offset" style="right: 0px; bottom: 0px;">--}}
{{--                                            <div class="simplebar-content-wrapper" tabindex="0"--}}
{{--                                                 role="region" aria-label="scrollable content"--}}
{{--                                                 style="height: auto; overflow: hidden;">--}}
{{--                                                <div class="simplebar-content" style="padding: 0px;">--}}
{{--                                                    <table--}}
{{--                                                        class="table text-nowrap align-middle table-custom mb-0 last-items-borderless">--}}
{{--                                                        <thead>--}}
{{--                                                        <tr>--}}
{{--                                                            <th scope="col"--}}
{{--                                                                class="text-dark fw-normal ps-0">Assigned--}}
{{--                                                            </th>--}}
{{--                                                            <th scope="col" class="text-dark fw-normal">--}}
{{--                                                                Progress--}}
{{--                                                            </th>--}}
{{--                                                            <th scope="col" class="text-dark fw-normal">--}}
{{--                                                                Priority--}}
{{--                                                            </th>--}}
{{--                                                            <th scope="col" class="text-dark fw-normal">--}}
{{--                                                                Budget--}}
{{--                                                            </th>--}}
{{--                                                        </tr>--}}
{{--                                                        </thead>--}}
{{--                                                        <tbody>--}}

{{--                                                        <tr>--}}
{{--                                                            <td class="ps-0">--}}
{{--                                                                <div--}}
{{--                                                                    class="d-flex align-items-center gap-6">--}}
{{--                                                                    <img--}}
{{--                                                                        src="../assets/images/products/dash-prd-2.jpg"--}}
{{--                                                                        alt="prd1" width="48"--}}
{{--                                                                        class="rounded">--}}
{{--                                                                    <div>--}}
{{--                                                                        <h6 class="mb-0">Web App--}}
{{--                                                                            Project</h6>--}}
{{--                                                                        <span>Mathew Flintoff</span>--}}
{{--                                                                    </div>--}}
{{--                                                                </div>--}}
{{--                                                            </td>--}}
{{--                                                            <td>--}}
{{--                                                                <span>73.2%</span>--}}
{{--                                                            </td>--}}
{{--                                                            <td>--}}
{{--                                                                            <span--}}
{{--                                                                                class="badge bg-warning-subtle text-warning">Medium</span>--}}
{{--                                                            </td>--}}
{{--                                                            <td>--}}
{{--                                                                <span class="text-dark">$3.5k</span>--}}
{{--                                                            </td>--}}
{{--                                                        </tr>--}}
{{--                                                        <tr>--}}
{{--                                                            <td class="ps-0">--}}
{{--                                                                <div--}}
{{--                                                                    class="d-flex align-items-center gap-6">--}}
{{--                                                                    <img--}}
{{--                                                                        src="../assets/images/products/dash-prd-3.jpg"--}}
{{--                                                                        alt="prd1" width="48"--}}
{{--                                                                        class="rounded">--}}
{{--                                                                    <div>--}}
{{--                                                                        <h6 class="mb-0">Modernize--}}
{{--                                                                            Dashboard</h6>--}}
{{--                                                                        <span>Anil Kumar</span>--}}
{{--                                                                    </div>--}}
{{--                                                                </div>--}}
{{--                                                            </td>--}}
{{--                                                            <td>--}}
{{--                                                                <span>73.2%</span>--}}
{{--                                                            </td>--}}
{{--                                                            <td>--}}
{{--                                <span class="badge bg-secondary-subtle text-secondary">Very--}}
{{--                                  High</span>--}}
{{--                                                            </td>--}}
{{--                                                            <td>--}}
{{--                                                                <span class="text-dark">$3.5k</span>--}}
{{--                                                            </td>--}}
{{--                                                        </tr>--}}
{{--                                                        <tr>--}}
{{--                                                            <td class="ps-0">--}}
{{--                                                                <div--}}
{{--                                                                    class="d-flex align-items-center gap-6">--}}
{{--                                                                    <img--}}
{{--                                                                        src="../assets/images/products/dash-prd-1.jpg"--}}
{{--                                                                        alt="prd1" width="48"--}}
{{--                                                                        class="rounded">--}}
{{--                                                                    <div>--}}
{{--                                                                        <h6 class="mb-0">Minecraf App</h6>--}}
{{--                                                                        <span>Jason Roy</span>--}}
{{--                                                                    </div>--}}
{{--                                                                </div>--}}
{{--                                                            </td>--}}
{{--                                                            <td>--}}
{{--                                                                <span>73.2%</span>--}}
{{--                                                            </td>--}}
{{--                                                            <td>--}}
{{--                                                                            <span--}}
{{--                                                                                class="badge bg-success-subtle text-success">Low</span>--}}
{{--                                                            </td>--}}
{{--                                                            <td>--}}
{{--                                                                <span class="text-dark">$3.5k</span>--}}
{{--                                                            </td>--}}
{{--                                                        </tr>--}}
{{--                                                        <tr>--}}
{{--                                                            <td class="ps-0">--}}
{{--                                                                <div--}}
{{--                                                                    class="d-flex align-items-center gap-6">--}}
{{--                                                                    <img--}}
{{--                                                                        src="../assets/images/products/dash-prd-4.jpg"--}}
{{--                                                                        alt="prd1" width="48"--}}
{{--                                                                        class="rounded">--}}
{{--                                                                    <div>--}}
{{--                                                                        <h6 class="mb-0">Dashboard Co</h6>--}}
{{--                                                                        <span>George Cruize</span>--}}
{{--                                                                    </div>--}}
{{--                                                                </div>--}}
{{--                                                            </td>--}}
{{--                                                            <td>--}}
{{--                                                                <span>73.2%</span>--}}
{{--                                                            </td>--}}
{{--                                                            <td>--}}
{{--                                                                            <span--}}
{{--                                                                                class="badge bg-danger-subtle text-danger">High</span>--}}
{{--                                                            </td>--}}
{{--                                                            <td>--}}
{{--                                                                <span class="text-dark">$3.5k</span>--}}
{{--                                                            </td>--}}
{{--                                                        </tr>--}}
{{--                                                        </tbody>--}}
{{--                                                    </table>--}}
{{--                                                </div>--}}
{{--                                            </div>--}}
{{--                                        </div>--}}
{{--                                    </div>--}}
{{--                                    <div class="simplebar-placeholder"--}}
{{--                                         style="width: 0px; height: 0px;"></div>--}}
{{--                                </div>--}}
{{--                                <div class="simplebar-track simplebar-horizontal"--}}
{{--                                     style="visibility: hidden;">--}}
{{--                                    <div class="simplebar-scrollbar"--}}
{{--                                         style="width: 0px; display: none;"></div>--}}
{{--                                </div>--}}
{{--                                <div class="simplebar-track simplebar-vertical" style="visibility: hidden;">--}}
{{--                                    <div class="simplebar-scrollbar"--}}
{{--                                         style="height: 0px; display: none;"></div>--}}
{{--                                </div>--}}
{{--                            </div>--}}
{{--                        </div>--}}
{{--                        <div class="tab-pane" id="saas" role="tabpanel">--}}
{{--                            <div class="table-responsive" data-simplebar="init">--}}
{{--                                <div class="simplebar-wrapper" style="margin: 0px;">--}}
{{--                                    <div class="simplebar-height-auto-observer-wrapper">--}}
{{--                                        <div class="simplebar-height-auto-observer"></div>--}}
{{--                                    </div>--}}
{{--                                    <div class="simplebar-mask">--}}
{{--                                        <div class="simplebar-offset" style="right: 0px; bottom: 0px;">--}}
{{--                                            <div class="simplebar-content-wrapper" tabindex="0"--}}
{{--                                                 role="region" aria-label="scrollable content"--}}
{{--                                                 style="height: auto; overflow: hidden;">--}}
{{--                                                <div class="simplebar-content" style="padding: 0px;">--}}
{{--                                                    <table--}}
{{--                                                        class="table text-nowrap align-middle table-custom mb-0 last-items-borderless">--}}
{{--                                                        <thead>--}}
{{--                                                        <tr>--}}
{{--                                                            <th scope="col"--}}
{{--                                                                class="text-dark fw-normal ps-0">Assigned--}}
{{--                                                            </th>--}}
{{--                                                            <th scope="col" class="text-dark fw-normal">--}}
{{--                                                                Progress--}}
{{--                                                            </th>--}}
{{--                                                            <th scope="col" class="text-dark fw-normal">--}}
{{--                                                                Priority--}}
{{--                                                            </th>--}}
{{--                                                            <th scope="col" class="text-dark fw-normal">--}}
{{--                                                                Budget--}}
{{--                                                            </th>--}}
{{--                                                        </tr>--}}
{{--                                                        </thead>--}}
{{--                                                        <tbody>--}}
{{--                                                        <tr>--}}
{{--                                                            <td class="ps-0">--}}
{{--                                                                <div--}}
{{--                                                                    class="d-flex align-items-center gap-6">--}}
{{--                                                                    <img--}}
{{--                                                                        src="../assets/images/products/dash-prd-2.jpg"--}}
{{--                                                                        alt="prd1" width="48"--}}
{{--                                                                        class="rounded">--}}
{{--                                                                    <div>--}}
{{--                                                                        <h6 class="mb-0">Web App--}}
{{--                                                                            Project</h6>--}}
{{--                                                                        <span>Mathew Flintoff</span>--}}
{{--                                                                    </div>--}}
{{--                                                                </div>--}}
{{--                                                            </td>--}}
{{--                                                            <td>--}}
{{--                                                                <span>73.2%</span>--}}
{{--                                                            </td>--}}
{{--                                                            <td>--}}
{{--                                                                            <span--}}
{{--                                                                                class="badge bg-warning-subtle text-warning">Medium</span>--}}
{{--                                                            </td>--}}
{{--                                                            <td>--}}
{{--                                                                <span class="text-dark">$3.5k</span>--}}
{{--                                                            </td>--}}
{{--                                                        </tr>--}}
{{--                                                        <tr>--}}
{{--                                                            <td class="ps-0">--}}
{{--                                                                <div--}}
{{--                                                                    class="d-flex align-items-center gap-6">--}}
{{--                                                                    <img--}}
{{--                                                                        src="../assets/images/products/dash-prd-1.jpg"--}}
{{--                                                                        alt="prd1" width="48"--}}
{{--                                                                        class="rounded">--}}
{{--                                                                    <div>--}}
{{--                                                                        <h6 class="mb-0">Minecraf App</h6>--}}
{{--                                                                        <span>Jason Roy</span>--}}
{{--                                                                    </div>--}}
{{--                                                                </div>--}}
{{--                                                            </td>--}}
{{--                                                            <td>--}}
{{--                                                                <span>73.2%</span>--}}
{{--                                                            </td>--}}
{{--                                                            <td>--}}
{{--                                                                            <span--}}
{{--                                                                                class="badge bg-success-subtle text-success">Low</span>--}}
{{--                                                            </td>--}}
{{--                                                            <td>--}}
{{--                                                                <span class="text-dark">$3.5k</span>--}}
{{--                                                            </td>--}}
{{--                                                        </tr>--}}

{{--                                                        <tr>--}}
{{--                                                            <td class="ps-0">--}}
{{--                                                                <div--}}
{{--                                                                    class="d-flex align-items-center gap-6">--}}
{{--                                                                    <img--}}
{{--                                                                        src="../assets/images/products/dash-prd-3.jpg"--}}
{{--                                                                        alt="prd1" width="48"--}}
{{--                                                                        class="rounded">--}}
{{--                                                                    <div>--}}
{{--                                                                        <h6 class="mb-0">Modernize--}}
{{--                                                                            Dashboard</h6>--}}
{{--                                                                        <span>Anil Kumar</span>--}}
{{--                                                                    </div>--}}
{{--                                                                </div>--}}
{{--                                                            </td>--}}
{{--                                                            <td>--}}
{{--                                                                <span>73.2%</span>--}}
{{--                                                            </td>--}}
{{--                                                            <td>--}}
{{--                                <span class="badge bg-secondary-subtle text-secondary">Very--}}
{{--                                  High</span>--}}
{{--                                                            </td>--}}
{{--                                                            <td>--}}
{{--                                                                <span class="text-dark">$3.5k</span>--}}
{{--                                                            </td>--}}
{{--                                                        </tr>--}}
{{--                                                        <tr>--}}
{{--                                                            <td class="ps-0">--}}
{{--                                                                <div--}}
{{--                                                                    class="d-flex align-items-center gap-6">--}}
{{--                                                                    <img--}}
{{--                                                                        src="../assets/images/products/dash-prd-4.jpg"--}}
{{--                                                                        alt="prd1" width="48"--}}
{{--                                                                        class="rounded">--}}
{{--                                                                    <div>--}}
{{--                                                                        <h6 class="mb-0">Dashboard Co</h6>--}}
{{--                                                                        <span>George Cruize</span>--}}
{{--                                                                    </div>--}}
{{--                                                                </div>--}}
{{--                                                            </td>--}}
{{--                                                            <td>--}}
{{--                                                                <span>73.2%</span>--}}
{{--                                                            </td>--}}
{{--                                                            <td>--}}
{{--                                                                            <span--}}
{{--                                                                                class="badge bg-danger-subtle text-danger">High</span>--}}
{{--                                                            </td>--}}
{{--                                                            <td>--}}
{{--                                                                <span class="text-dark">$3.5k</span>--}}
{{--                                                            </td>--}}
{{--                                                        </tr>--}}
{{--                                                        </tbody>--}}
{{--                                                    </table>--}}
{{--                                                </div>--}}
{{--                                            </div>--}}
{{--                                        </div>--}}
{{--                                    </div>--}}
{{--                                    <div class="simplebar-placeholder"--}}
{{--                                         style="width: 0px; height: 0px;"></div>--}}
{{--                                </div>--}}
{{--                                <div class="simplebar-track simplebar-horizontal"--}}
{{--                                     style="visibility: hidden;">--}}
{{--                                    <div class="simplebar-scrollbar"--}}
{{--                                         style="width: 0px; display: none;"></div>--}}
{{--                                </div>--}}
{{--                                <div class="simplebar-track simplebar-vertical" style="visibility: hidden;">--}}
{{--                                    <div class="simplebar-scrollbar"--}}
{{--                                         style="height: 0px; display: none;"></div>--}}
{{--                                </div>--}}
{{--                            </div>--}}
{{--                        </div>--}}

{{--                        <div class="tab-pane" id="other" role="tabpanel">--}}
{{--                            <div class="table-responsive" data-simplebar="init">--}}
{{--                                <div class="simplebar-wrapper" style="margin: 0px;">--}}
{{--                                    <div class="simplebar-height-auto-observer-wrapper">--}}
{{--                                        <div class="simplebar-height-auto-observer"></div>--}}
{{--                                    </div>--}}
{{--                                    <div class="simplebar-mask">--}}
{{--                                        <div class="simplebar-offset" style="right: 0px; bottom: 0px;">--}}
{{--                                            <div class="simplebar-content-wrapper" tabindex="0"--}}
{{--                                                 role="region" aria-label="scrollable content"--}}
{{--                                                 style="height: auto; overflow: hidden;">--}}
{{--                                                <div class="simplebar-content" style="padding: 0px;">--}}
{{--                                                    <table--}}
{{--                                                        class="table text-nowrap align-middle table-custom mb-0 last-items-borderless">--}}
{{--                                                        <thead>--}}
{{--                                                        <tr>--}}
{{--                                                            <th scope="col"--}}
{{--                                                                class="text-dark fw-normal ps-0">Assigned--}}
{{--                                                            </th>--}}
{{--                                                            <th scope="col" class="text-dark fw-normal">--}}
{{--                                                                Progress--}}
{{--                                                            </th>--}}
{{--                                                            <th scope="col" class="text-dark fw-normal">--}}
{{--                                                                Priority--}}
{{--                                                            </th>--}}
{{--                                                            <th scope="col" class="text-dark fw-normal">--}}
{{--                                                                Budget--}}
{{--                                                            </th>--}}
{{--                                                        </tr>--}}
{{--                                                        </thead>--}}
{{--                                                        <tbody>--}}
{{--                                                        <tr>--}}
{{--                                                            <td class="ps-0">--}}
{{--                                                                <div--}}
{{--                                                                    class="d-flex align-items-center gap-6">--}}
{{--                                                                    <img--}}
{{--                                                                        src="../assets/images/products/dash-prd-1.jpg"--}}
{{--                                                                        alt="prd1" width="48"--}}
{{--                                                                        class="rounded">--}}
{{--                                                                    <div>--}}
{{--                                                                        <h6 class="mb-0">Minecraf App</h6>--}}
{{--                                                                        <span>Jason Roy</span>--}}
{{--                                                                    </div>--}}
{{--                                                                </div>--}}
{{--                                                            </td>--}}
{{--                                                            <td>--}}
{{--                                                                <span>73.2%</span>--}}
{{--                                                            </td>--}}
{{--                                                            <td>--}}
{{--                                                                            <span--}}
{{--                                                                                class="badge bg-success-subtle text-success">Low</span>--}}
{{--                                                            </td>--}}
{{--                                                            <td>--}}
{{--                                                                <span class="text-dark">$3.5k</span>--}}
{{--                                                            </td>--}}
{{--                                                        </tr>--}}
{{--                                                        <tr>--}}
{{--                                                            <td class="ps-0">--}}
{{--                                                                <div--}}
{{--                                                                    class="d-flex align-items-center gap-6">--}}
{{--                                                                    <img--}}
{{--                                                                        src="../assets/images/products/dash-prd-3.jpg"--}}
{{--                                                                        alt="prd1" width="48"--}}
{{--                                                                        class="rounded">--}}
{{--                                                                    <div>--}}
{{--                                                                        <h6 class="mb-0">Modernize--}}
{{--                                                                            Dashboard</h6>--}}
{{--                                                                        <span>Anil Kumar</span>--}}
{{--                                                                    </div>--}}
{{--                                                                </div>--}}
{{--                                                            </td>--}}
{{--                                                            <td>--}}
{{--                                                                <span>73.2%</span>--}}
{{--                                                            </td>--}}
{{--                                                            <td>--}}
{{--                                <span class="badge bg-secondary-subtle text-secondary">Very--}}
{{--                                  High</span>--}}
{{--                                                            </td>--}}
{{--                                                            <td>--}}
{{--                                                                <span class="text-dark">$3.5k</span>--}}
{{--                                                            </td>--}}
{{--                                                        </tr>--}}
{{--                                                        <tr>--}}
{{--                                                            <td class="ps-0">--}}
{{--                                                                <div--}}
{{--                                                                    class="d-flex align-items-center gap-6">--}}
{{--                                                                    <img--}}
{{--                                                                        src="../assets/images/products/dash-prd-2.jpg"--}}
{{--                                                                        alt="prd1" width="48"--}}
{{--                                                                        class="rounded">--}}
{{--                                                                    <div>--}}
{{--                                                                        <h6 class="mb-0">Web App--}}
{{--                                                                            Project</h6>--}}
{{--                                                                        <span>Mathew Flintoff</span>--}}
{{--                                                                    </div>--}}
{{--                                                                </div>--}}
{{--                                                            </td>--}}
{{--                                                            <td>--}}
{{--                                                                <span>73.2%</span>--}}
{{--                                                            </td>--}}
{{--                                                            <td>--}}
{{--                                                                            <span--}}
{{--                                                                                class="badge bg-warning-subtle text-warning">Medium</span>--}}
{{--                                                            </td>--}}
{{--                                                            <td>--}}
{{--                                                                <span class="text-dark">$3.5k</span>--}}
{{--                                                            </td>--}}
{{--                                                        </tr>--}}

{{--                                                        <tr>--}}
{{--                                                            <td class="ps-0">--}}
{{--                                                                <div--}}
{{--                                                                    class="d-flex align-items-center gap-6">--}}
{{--                                                                    <img--}}
{{--                                                                        src="../assets/images/products/dash-prd-4.jpg"--}}
{{--                                                                        alt="prd1" width="48"--}}
{{--                                                                        class="rounded">--}}
{{--                                                                    <div>--}}
{{--                                                                        <h6 class="mb-0">Dashboard Co</h6>--}}
{{--                                                                        <span>George Cruize</span>--}}
{{--                                                                    </div>--}}
{{--                                                                </div>--}}
{{--                                                            </td>--}}
{{--                                                            <td>--}}
{{--                                                                <span>73.2%</span>--}}
{{--                                                            </td>--}}
{{--                                                            <td>--}}
{{--                                                                            <span--}}
{{--                                                                                class="badge bg-danger-subtle text-danger">High</span>--}}
{{--                                                            </td>--}}
{{--                                                            <td>--}}
{{--                                                                <span class="text-dark">$3.5k</span>--}}
{{--                                                            </td>--}}
{{--                                                        </tr>--}}
{{--                                                        </tbody>--}}
{{--                                                    </table>--}}
{{--                                                </div>--}}
{{--                                            </div>--}}
{{--                                        </div>--}}
{{--                                    </div>--}}
{{--                                    <div class="simplebar-placeholder"--}}
{{--                                         style="width: 0px; height: 0px;"></div>--}}
{{--                                </div>--}}
{{--                                <div class="simplebar-track simplebar-horizontal"--}}
{{--                                     style="visibility: hidden;">--}}
{{--                                    <div class="simplebar-scrollbar"--}}
{{--                                         style="width: 0px; display: none;"></div>--}}
{{--                                </div>--}}
{{--                                <div class="simplebar-track simplebar-vertical" style="visibility: hidden;">--}}
{{--                                    <div class="simplebar-scrollbar"--}}
{{--                                         style="height: 0px; display: none;"></div>--}}
{{--                                </div>--}}
{{--                            </div>--}}
{{--                        </div>--}}
{{--                    </div>--}}


{{--                </div>--}}
{{--            </div>--}}
{{--        </div>--}}
{{--        <div class="col-lg-4">--}}
{{--            <!-- -------------------------------------------- -->--}}
{{--            <!-- Total settlements -->--}}
{{--            <!-- -------------------------------------------- -->--}}
{{--            <div class="card bg-primary-subtle">--}}
{{--                <div class="card-body">--}}
{{--                    <div class="hstack align-items-center gap-3 mb-4">--}}
{{--                    <span--}}
{{--                        class="d-flex align-items-center justify-content-center round-48 bg-white rounded flex-shrink-0">--}}
{{--                      <iconify-icon icon="solar:box-linear" class="fs-7 text-primary"></iconify-icon>--}}
{{--                    </span>--}}
{{--                        <div>--}}
{{--                            <p class="mb-1 text-dark">Total settlements</p>--}}
{{--                            <h4 class="mb-0">$122,580</h4>--}}
{{--                        </div>--}}
{{--                    </div>--}}
{{--                    <div style="height: 270px;">--}}
{{--                        <div id="settlements" style="min-height: 315px;">--}}
{{--                            <div id="apexchartsvg3gg2gf"--}}
{{--                                 class="apexcharts-canvas apexchartsvg3gg2gf apexcharts-theme-light"--}}
{{--                                 style="width: 300px; height: 300px;">--}}
{{--                                <svg id="SvgjsSvg2103" width="300" height="300"--}}
{{--                                     xmlns="http://www.w3.org/2000/svg" version="1.1"--}}
{{--                                     xmlns:xlink="http://www.w3.org/1999/xlink"--}}
{{--                                     xmlns:svgjs="http://svgjs.dev"--}}
{{--                                     class="apexcharts-svg apexcharts-zoomable" xmlns:data="ApexChartsNS"--}}
{{--                                     transform="translate(0, 0)" style="background: transparent;">--}}
{{--                                    <foreignObject x="0" y="0" width="300" height="300">--}}
{{--                                        <div class="apexcharts-legend" xmlns="http://www.w3.org/1999/xhtml"--}}
{{--                                             style="max-height: 150px;"></div>--}}
{{--                                    </foreignObject>--}}
{{--                                    <rect id="SvgjsRect2108" width="0" height="0" x="0" y="0" rx="0" ry="0"--}}
{{--                                          opacity="1" stroke-width="0" stroke="none" stroke-dasharray="0"--}}
{{--                                          fill="#fefefe"></rect>--}}
{{--                                    <g id="SvgjsG2179" class="apexcharts-yaxis" rel="0"--}}
{{--                                       transform="translate(-18, 0)"></g>--}}
{{--                                    <g id="SvgjsG2105" class="apexcharts-inner apexcharts-graphical"--}}
{{--                                       transform="translate(12, 30)">--}}
{{--                                        <defs id="SvgjsDefs2104">--}}
{{--                                            <clipPath id="gridRectMaskvg3gg2gf">--}}
{{--                                                <rect id="SvgjsRect2110" width="284"--}}
{{--                                                      height="231.66266666666667" x="-4" y="-6" rx="0"--}}
{{--                                                      ry="0" opacity="1" stroke-width="0" stroke="none"--}}
{{--                                                      stroke-dasharray="0" fill="#fff"></rect>--}}
{{--                                            </clipPath>--}}
{{--                                            <clipPath id="forecastMaskvg3gg2gf"></clipPath>--}}
{{--                                            <clipPath id="nonForecastMaskvg3gg2gf"></clipPath>--}}
{{--                                            <clipPath id="gridRectMarkerMaskvg3gg2gf">--}}
{{--                                                <rect id="SvgjsRect2111" width="282"--}}
{{--                                                      height="223.66266666666667" x="-2" y="-2" rx="0"--}}
{{--                                                      ry="0" opacity="1" stroke-width="0" stroke="none"--}}
{{--                                                      stroke-dasharray="0" fill="#fff"></rect>--}}
{{--                                            </clipPath>--}}
{{--                                        </defs>--}}
{{--                                        <line id="SvgjsLine2109" x1="0" y1="0" x2="0"--}}
{{--                                              y2="219.66266666666667" stroke="#b6b6b6" stroke-dasharray="3"--}}
{{--                                              stroke-linecap="butt" class="apexcharts-xcrosshairs" x="0"--}}
{{--                                              y="0" width="1" height="219.66266666666667" fill="#b1b9c4"--}}
{{--                                              filter="none" fill-opacity="0.9" stroke-width="1"></line>--}}
{{--                                        <g id="SvgjsG2117" class="apexcharts-grid">--}}
{{--                                            <g id="SvgjsG2118" class="apexcharts-gridlines-horizontal">--}}
{{--                                                <line id="SvgjsLine2122" x1="0" y1="73.2208888888889"--}}
{{--                                                      x2="278" y2="73.2208888888889"--}}
{{--                                                      stroke="var(--bs-primary-bg-subtle)"--}}
{{--                                                      stroke-dasharray="4" stroke-linecap="butt"--}}
{{--                                                      class="apexcharts-gridline"></line>--}}
{{--                                                <line id="SvgjsLine2123" x1="0" y1="146.4417777777778"--}}
{{--                                                      x2="278" y2="146.4417777777778"--}}
{{--                                                      stroke="var(--bs-primary-bg-subtle)"--}}
{{--                                                      stroke-dasharray="4" stroke-linecap="butt"--}}
{{--                                                      class="apexcharts-gridline"></line>--}}
{{--                                            </g>--}}
{{--                                            <g id="SvgjsG2119" class="apexcharts-gridlines-vertical"></g>--}}
{{--                                            <line id="SvgjsLine2126" x1="0" y1="219.66266666666667" x2="278"--}}
{{--                                                  y2="219.66266666666667" stroke="transparent"--}}
{{--                                                  stroke-dasharray="0" stroke-linecap="butt"></line>--}}
{{--                                            <line id="SvgjsLine2125" x1="0" y1="1" x2="0"--}}
{{--                                                  y2="219.66266666666667" stroke="transparent"--}}
{{--                                                  stroke-dasharray="0" stroke-linecap="butt"></line>--}}
{{--                                        </g>--}}
{{--                                        <g id="SvgjsG2120" class="apexcharts-grid-borders">--}}
{{--                                            <line id="SvgjsLine2121" x1="0" y1="0" x2="278" y2="0"--}}
{{--                                                  stroke="var(--bs-primary-bg-subtle)" stroke-dasharray="4"--}}
{{--                                                  stroke-linecap="butt" class="apexcharts-gridline"></line>--}}
{{--                                            <line id="SvgjsLine2124" x1="0" y1="219.66266666666667" x2="278"--}}
{{--                                                  y2="219.66266666666667"--}}
{{--                                                  stroke="var(--bs-primary-bg-subtle)" stroke-dasharray="4"--}}
{{--                                                  stroke-linecap="butt" class="apexcharts-gridline"></line>--}}
{{--                                        </g>--}}
{{--                                        <g id="SvgjsG2112"--}}
{{--                                           class="apexcharts-line-series apexcharts-plot-series">--}}
{{--                                            <g id="SvgjsG2113" class="apexcharts-series" zIndex="0"--}}
{{--                                               seriesName="settlements" data:longestSeries="true" rel="1"--}}
{{--                                               data:realIndex="0">--}}
{{--                                                <path id="SvgjsPath2116"--}}
{{--                                                      d="M 0 161.08595555555556C 6.486666666666667 161.08595555555556 12.046666666666667 161.08595555555556 18.533333333333335 161.08595555555556C 25.020000000000003 161.08595555555556 30.580000000000002 102.50924444444443 37.06666666666667 102.50924444444443C 43.553333333333335 102.50924444444443 49.11333333333334 102.50924444444443 55.6 102.50924444444443C 62.08666666666667 102.50924444444443 67.64666666666668 175.73013333333333 74.13333333333334 175.73013333333333C 80.62 175.73013333333333 86.18 175.73013333333333 92.66666666666667 175.73013333333333C 99.15333333333334 175.73013333333333 104.71333333333334 205.0184888888889 111.2 205.0184888888889C 117.68666666666667 205.0184888888889 123.24666666666668 205.0184888888889 129.73333333333335 205.0184888888889C 136.22000000000003 205.0184888888889 141.78 175.73013333333333 148.26666666666668 175.73013333333333C 154.75333333333336 175.73013333333333 160.31333333333333 175.73013333333333 166.8 175.73013333333333C 173.2866666666667 175.73013333333333 178.84666666666666 73.22088888888888 185.33333333333334 73.22088888888888C 191.82000000000002 73.22088888888888 197.38 73.22088888888888 203.86666666666667 73.22088888888888C 210.35333333333335 73.22088888888888 215.91333333333333 190.3743111111111 222.4 190.3743111111111C 228.88666666666668 190.3743111111111 234.44666666666666 190.3743111111111 240.93333333333334 190.3743111111111C 247.42000000000002 190.3743111111111 252.98000000000002 14.64417777777777 259.4666666666667 14.64417777777777C 265.9533333333334 14.64417777777777 271.5133333333333 14.64417777777777 278 14.64417777777777"--}}
{{--                                                      fill="none" fill-opacity="1"--}}
{{--                                                      stroke="var(--bs-primary)" stroke-opacity="1"--}}
{{--                                                      stroke-linecap="butt" stroke-width="2"--}}
{{--                                                      stroke-dasharray="0" class="apexcharts-line" index="0"--}}
{{--                                                      clip-path="url(#gridRectMaskvg3gg2gf)"--}}
{{--                                                      pathTo="M 0 161.08595555555556C 6.486666666666667 161.08595555555556 12.046666666666667 161.08595555555556 18.533333333333335 161.08595555555556C 25.020000000000003 161.08595555555556 30.580000000000002 102.50924444444443 37.06666666666667 102.50924444444443C 43.553333333333335 102.50924444444443 49.11333333333334 102.50924444444443 55.6 102.50924444444443C 62.08666666666667 102.50924444444443 67.64666666666668 175.73013333333333 74.13333333333334 175.73013333333333C 80.62 175.73013333333333 86.18 175.73013333333333 92.66666666666667 175.73013333333333C 99.15333333333334 175.73013333333333 104.71333333333334 205.0184888888889 111.2 205.0184888888889C 117.68666666666667 205.0184888888889 123.24666666666668 205.0184888888889 129.73333333333335 205.0184888888889C 136.22000000000003 205.0184888888889 141.78 175.73013333333333 148.26666666666668 175.73013333333333C 154.75333333333336 175.73013333333333 160.31333333333333 175.73013333333333 166.8 175.73013333333333C 173.2866666666667 175.73013333333333 178.84666666666666 73.22088888888888 185.33333333333334 73.22088888888888C 191.82000000000002 73.22088888888888 197.38 73.22088888888888 203.86666666666667 73.22088888888888C 210.35333333333335 73.22088888888888 215.91333333333333 190.3743111111111 222.4 190.3743111111111C 228.88666666666668 190.3743111111111 234.44666666666666 190.3743111111111 240.93333333333334 190.3743111111111C 247.42000000000002 190.3743111111111 252.98000000000002 14.64417777777777 259.4666666666667 14.64417777777777C 265.9533333333334 14.64417777777777 271.5133333333333 14.64417777777777 278 14.64417777777777"--}}
{{--                                                      pathFrom="M -1 219.66266666666667 L -1 219.66266666666667 L 18.533333333333335 219.66266666666667 L 37.06666666666667 219.66266666666667 L 55.6 219.66266666666667 L 74.13333333333334 219.66266666666667 L 92.66666666666667 219.66266666666667 L 111.2 219.66266666666667 L 129.73333333333335 219.66266666666667 L 148.26666666666668 219.66266666666667 L 166.8 219.66266666666667 L 185.33333333333334 219.66266666666667 L 203.86666666666667 219.66266666666667 L 222.4 219.66266666666667 L 240.93333333333334 219.66266666666667 L 259.4666666666667 219.66266666666667 L 278 219.66266666666667"--}}
{{--                                                      fill-rule="evenodd"></path>--}}
{{--                                                <g id="SvgjsG2114"--}}
{{--                                                   class="apexcharts-series-markers-wrap apexcharts-hidden-element-shown"--}}
{{--                                                   data:realIndex="0">--}}
{{--                                                    <g class="apexcharts-series-markers">--}}
{{--                                                        <circle id="SvgjsCircle2183" r="0" cx="0" cy="0"--}}
{{--                                                                class="apexcharts-marker wjevpjxxg no-pointer-events"--}}
{{--                                                                stroke="var(--bs-primary)"--}}
{{--                                                                fill="var(--bs-primary)" fill-opacity="1"--}}
{{--                                                                stroke-width="3" stroke-opacity="0.9"--}}
{{--                                                                default-marker-size="0"></circle>--}}
{{--                                                    </g>--}}
{{--                                                </g>--}}
{{--                                            </g>--}}
{{--                                            <g id="SvgjsG2115" class="apexcharts-datalabels"--}}
{{--                                               data:realIndex="0"></g>--}}
{{--                                        </g>--}}
{{--                                        <line id="SvgjsLine2127" x1="0" y1="0" x2="278" y2="0"--}}
{{--                                              stroke="#b6b6b6" stroke-dasharray="0" stroke-width="1"--}}
{{--                                              stroke-linecap="butt" class="apexcharts-ycrosshairs"></line>--}}
{{--                                        <line id="SvgjsLine2128" x1="0" y1="0" x2="278" y2="0"--}}
{{--                                              stroke-dasharray="0" stroke-width="0" stroke-linecap="butt"--}}
{{--                                              class="apexcharts-ycrosshairs-hidden"></line>--}}
{{--                                        <g id="SvgjsG2129" class="apexcharts-xaxis"--}}
{{--                                           transform="translate(0, 0)">--}}
{{--                                            <g id="SvgjsG2130" class="apexcharts-xaxis-texts-g"--}}
{{--                                               transform="translate(0, -10)">--}}
{{--                                                <text id="SvgjsText2132" font-family="inherit" x="0"--}}
{{--                                                      y="242.66266666666667" text-anchor="end"--}}
{{--                                                      dominant-baseline="auto" font-size="12px"--}}
{{--                                                      font-weight="600" fill="#adb0bb"--}}
{{--                                                      class="apexcharts-text apexcharts-xaxis-label "--}}
{{--                                                      style="font-family: inherit;"--}}
{{--                                                      transform="rotate(0 1.2194976806640625 237.1626739501953)">--}}
{{--                                                    <tspan id="SvgjsTspan2133">1W</tspan>--}}
{{--                                                    <title>1W</title></text>--}}
{{--                                                <text id="SvgjsText2135" font-family="inherit"--}}
{{--                                                      x="18.53333333333334" y="242.66266666666667"--}}
{{--                                                      text-anchor="end" dominant-baseline="auto"--}}
{{--                                                      font-size="12px" font-weight="600" fill="#adb0bb"--}}
{{--                                                      class="apexcharts-text apexcharts-xaxis-label "--}}
{{--                                                      style="font-family: inherit;"--}}
{{--                                                      transform="rotate(0 1 -1)">--}}
{{--                                                    <tspan id="SvgjsTspan2136"></tspan>--}}
{{--                                                    <title></title></text>--}}
{{--                                                <text id="SvgjsText2138" font-family="inherit"--}}
{{--                                                      x="37.06666666666668" y="242.66266666666667"--}}
{{--                                                      text-anchor="end" dominant-baseline="auto"--}}
{{--                                                      font-size="12px" font-weight="600" fill="#adb0bb"--}}
{{--                                                      class="apexcharts-text apexcharts-xaxis-label "--}}
{{--                                                      style="font-family: inherit;"--}}
{{--                                                      transform="rotate(0 38.28772735595703 237.1626739501953)">--}}
{{--                                                    <tspan id="SvgjsTspan2139">3W</tspan>--}}
{{--                                                    <title>3W</title></text>--}}
{{--                                                <text id="SvgjsText2141" font-family="inherit"--}}
{{--                                                      x="55.60000000000001" y="242.66266666666667"--}}
{{--                                                      text-anchor="end" dominant-baseline="auto"--}}
{{--                                                      font-size="12px" font-weight="600" fill="#adb0bb"--}}
{{--                                                      class="apexcharts-text apexcharts-xaxis-label "--}}
{{--                                                      style="font-family: inherit;"--}}
{{--                                                      transform="rotate(0 1 -1)">--}}
{{--                                                    <tspan id="SvgjsTspan2142"></tspan>--}}
{{--                                                    <title></title></text>--}}
{{--                                                <text id="SvgjsText2144" font-family="inherit"--}}
{{--                                                      x="74.13333333333334" y="242.66266666666667"--}}
{{--                                                      text-anchor="end" dominant-baseline="auto"--}}
{{--                                                      font-size="12px" font-weight="600" fill="#adb0bb"--}}
{{--                                                      class="apexcharts-text apexcharts-xaxis-label "--}}
{{--                                                      style="font-family: inherit;"--}}
{{--                                                      transform="rotate(0 75.35308074951172 237.1626739501953)">--}}
{{--                                                    <tspan id="SvgjsTspan2145">5W</tspan>--}}
{{--                                                    <title>5W</title></text>--}}
{{--                                                <text id="SvgjsText2147" font-family="inherit"--}}
{{--                                                      x="92.66666666666667" y="242.66266666666667"--}}
{{--                                                      text-anchor="end" dominant-baseline="auto"--}}
{{--                                                      font-size="12px" font-weight="600" fill="#adb0bb"--}}
{{--                                                      class="apexcharts-text apexcharts-xaxis-label "--}}
{{--                                                      style="font-family: inherit;"--}}
{{--                                                      transform="rotate(0 1 -1)">--}}
{{--                                                    <tspan id="SvgjsTspan2148"></tspan>--}}
{{--                                                    <title></title></text>--}}
{{--                                                <text id="SvgjsText2150" font-family="inherit" x="111.2"--}}
{{--                                                      y="242.66266666666667" text-anchor="end"--}}
{{--                                                      dominant-baseline="auto" font-size="12px"--}}
{{--                                                      font-weight="600" fill="#adb0bb"--}}
{{--                                                      class="apexcharts-text apexcharts-xaxis-label "--}}
{{--                                                      style="font-family: inherit;"--}}
{{--                                                      transform="rotate(0 112.4239273071289 237.1626739501953)">--}}
{{--                                                    <tspan id="SvgjsTspan2151">7W</tspan>--}}
{{--                                                    <title>7W</title></text>--}}
{{--                                                <text id="SvgjsText2153" font-family="inherit"--}}
{{--                                                      x="129.73333333333332" y="242.66266666666667"--}}
{{--                                                      text-anchor="end" dominant-baseline="auto"--}}
{{--                                                      font-size="12px" font-weight="600" fill="#adb0bb"--}}
{{--                                                      class="apexcharts-text apexcharts-xaxis-label "--}}
{{--                                                      style="font-family: inherit;"--}}
{{--                                                      transform="rotate(0 1 -1)">--}}
{{--                                                    <tspan id="SvgjsTspan2154"></tspan>--}}
{{--                                                    <title></title></text>--}}
{{--                                                <text id="SvgjsText2156" font-family="inherit"--}}
{{--                                                      x="148.26666666666665" y="242.66266666666667"--}}
{{--                                                      text-anchor="end" dominant-baseline="auto"--}}
{{--                                                      font-size="12px" font-weight="600" fill="#adb0bb"--}}
{{--                                                      class="apexcharts-text apexcharts-xaxis-label "--}}
{{--                                                      style="font-family: inherit;"--}}
{{--                                                      transform="rotate(0 149.48784637451172 237.1626739501953)">--}}
{{--                                                    <tspan id="SvgjsTspan2157">9W</tspan>--}}
{{--                                                    <title>9W</title></text>--}}
{{--                                                <text id="SvgjsText2159" font-family="inherit"--}}
{{--                                                      x="166.79999999999998" y="242.66266666666667"--}}
{{--                                                      text-anchor="end" dominant-baseline="auto"--}}
{{--                                                      font-size="12px" font-weight="600" fill="#adb0bb"--}}
{{--                                                      class="apexcharts-text apexcharts-xaxis-label "--}}
{{--                                                      style="font-family: inherit;"--}}
{{--                                                      transform="rotate(0 1 -1)">--}}
{{--                                                    <tspan id="SvgjsTspan2160"></tspan>--}}
{{--                                                    <title></title></text>--}}
{{--                                                <text id="SvgjsText2162" font-family="inherit"--}}
{{--                                                      x="185.33333333333331" y="242.66266666666667"--}}
{{--                                                      text-anchor="end" dominant-baseline="auto"--}}
{{--                                                      font-size="12px" font-weight="600" fill="#adb0bb"--}}
{{--                                                      class="apexcharts-text apexcharts-xaxis-label "--}}
{{--                                                      style="font-family: inherit;"--}}
{{--                                                      transform="rotate(0 186.55357360839844 237.1626739501953)">--}}
{{--                                                    <tspan id="SvgjsTspan2163">11W</tspan>--}}
{{--                                                    <title>11W</title></text>--}}
{{--                                                <text id="SvgjsText2165" font-family="inherit"--}}
{{--                                                      x="203.86666666666665" y="242.66266666666667"--}}
{{--                                                      text-anchor="end" dominant-baseline="auto"--}}
{{--                                                      font-size="12px" font-weight="600" fill="#adb0bb"--}}
{{--                                                      class="apexcharts-text apexcharts-xaxis-label "--}}
{{--                                                      style="font-family: inherit;"--}}
{{--                                                      transform="rotate(0 1 -1)">--}}
{{--                                                    <tspan id="SvgjsTspan2166"></tspan>--}}
{{--                                                    <title></title></text>--}}
{{--                                                <text id="SvgjsText2168" font-family="inherit"--}}
{{--                                                      x="222.39999999999998" y="242.66266666666667"--}}
{{--                                                      text-anchor="end" dominant-baseline="auto"--}}
{{--                                                      font-size="12px" font-weight="600" fill="#adb0bb"--}}
{{--                                                      class="apexcharts-text apexcharts-xaxis-label "--}}
{{--                                                      style="font-family: inherit;"--}}
{{--                                                      transform="rotate(0 223.6218032836914 237.1626739501953)">--}}
{{--                                                    <tspan id="SvgjsTspan2169">13W</tspan>--}}
{{--                                                    <title>13W</title></text>--}}
{{--                                                <text id="SvgjsText2171" font-family="inherit"--}}
{{--                                                      x="240.9333333333333" y="242.66266666666667"--}}
{{--                                                      text-anchor="end" dominant-baseline="auto"--}}
{{--                                                      font-size="12px" font-weight="600" fill="#adb0bb"--}}
{{--                                                      class="apexcharts-text apexcharts-xaxis-label "--}}
{{--                                                      style="font-family: inherit;"--}}
{{--                                                      transform="rotate(0 1 -1)">--}}
{{--                                                    <tspan id="SvgjsTspan2172"></tspan>--}}
{{--                                                    <title></title></text>--}}
{{--                                                <text id="SvgjsText2174" font-family="inherit"--}}
{{--                                                      x="259.4666666666667" y="242.66266666666667"--}}
{{--                                                      text-anchor="end" dominant-baseline="auto"--}}
{{--                                                      font-size="12px" font-weight="600" fill="#adb0bb"--}}
{{--                                                      class="apexcharts-text apexcharts-xaxis-label "--}}
{{--                                                      style="font-family: inherit;"--}}
{{--                                                      transform="rotate(0 260.6871643066406 237.1626739501953)">--}}
{{--                                                    <tspan id="SvgjsTspan2175">15W</tspan>--}}
{{--                                                    <title>15W</title></text>--}}
{{--                                                <text id="SvgjsText2177" font-family="inherit"--}}
{{--                                                      x="278.00000000000006" y="242.66266666666667"--}}
{{--                                                      text-anchor="end" dominant-baseline="auto"--}}
{{--                                                      font-size="12px" font-weight="600" fill="#adb0bb"--}}
{{--                                                      class="apexcharts-text apexcharts-xaxis-label "--}}
{{--                                                      style="font-family: inherit;"--}}
{{--                                                      transform="rotate(0 1 -1)">--}}
{{--                                                    <tspan id="SvgjsTspan2178"></tspan>--}}
{{--                                                    <title></title></text>--}}
{{--                                            </g>--}}
{{--                                        </g>--}}
{{--                                        <g id="SvgjsG2180" class="apexcharts-yaxis-annotations"></g>--}}
{{--                                        <g id="SvgjsG2181" class="apexcharts-xaxis-annotations"></g>--}}
{{--                                        <g id="SvgjsG2182" class="apexcharts-point-annotations"></g>--}}
{{--                                        <rect id="SvgjsRect2184" width="0" height="0" x="0" y="0" rx="0"--}}
{{--                                              ry="0" opacity="1" stroke-width="0" stroke="none"--}}
{{--                                              stroke-dasharray="0" fill="#fefefe"--}}
{{--                                              class="apexcharts-zoom-rect"></rect>--}}
{{--                                        <rect id="SvgjsRect2185" width="0" height="0" x="0" y="0" rx="0"--}}
{{--                                              ry="0" opacity="1" stroke-width="0" stroke="none"--}}
{{--                                              stroke-dasharray="0" fill="#fefefe"--}}
{{--                                              class="apexcharts-selection-rect"></rect>--}}
{{--                                    </g>--}}
{{--                                </svg>--}}
{{--                                <div class="apexcharts-tooltip apexcharts-theme-dark">--}}
{{--                                    <div class="apexcharts-tooltip-title"--}}
{{--                                         style="font-family: inherit; font-size: 12px;"></div>--}}
{{--                                    <div class="apexcharts-tooltip-series-group" style="order: 1;"><span--}}
{{--                                            class="apexcharts-tooltip-marker"--}}
{{--                                            style="background-color: var(--bs-primary);"></span>--}}
{{--                                        <div class="apexcharts-tooltip-text"--}}
{{--                                             style="font-family: inherit; font-size: 12px;">--}}
{{--                                            <div class="apexcharts-tooltip-y-group"><span--}}
{{--                                                    class="apexcharts-tooltip-text-y-label"></span><span--}}
{{--                                                    class="apexcharts-tooltip-text-y-value"></span></div>--}}
{{--                                            <div class="apexcharts-tooltip-goals-group"><span--}}
{{--                                                    class="apexcharts-tooltip-text-goals-label"></span><span--}}
{{--                                                    class="apexcharts-tooltip-text-goals-value"></span>--}}
{{--                                            </div>--}}
{{--                                            <div class="apexcharts-tooltip-z-group"><span--}}
{{--                                                    class="apexcharts-tooltip-text-z-label"></span><span--}}
{{--                                                    class="apexcharts-tooltip-text-z-value"></span></div>--}}
{{--                                        </div>--}}
{{--                                    </div>--}}
{{--                                </div>--}}
{{--                                <div--}}
{{--                                    class="apexcharts-xaxistooltip apexcharts-xaxistooltip-bottom apexcharts-theme-dark">--}}
{{--                                    <div class="apexcharts-xaxistooltip-text"--}}
{{--                                         style="font-family: inherit; font-size: 12px;"></div>--}}
{{--                                </div>--}}
{{--                                <div--}}
{{--                                    class="apexcharts-yaxistooltip apexcharts-yaxistooltip-0 apexcharts-yaxistooltip-left apexcharts-theme-dark">--}}
{{--                                    <div class="apexcharts-yaxistooltip-text"></div>--}}
{{--                                </div>--}}
{{--                            </div>--}}
{{--                        </div>--}}
{{--                    </div>--}}
{{--                    <div class="row mt-4 mb-2">--}}
{{--                        <div class="col-md-6 text-center">--}}
{{--                            <p class="mb-1 text-dark lh-lg">Total balance</p>--}}
{{--                            <h4 class="mb-0">$122,580</h4>--}}
{{--                        </div>--}}
{{--                        <div class="col-md-6 text-center mt-3 mt-md-0">--}}
{{--                            <p class="mb-1 text-dark lh-lg">Withdrawals</p>--}}
{{--                            <h4 class="mb-0">$31,640</h4>--}}
{{--                        </div>--}}
{{--                    </div>--}}
{{--                </div>--}}
{{--            </div>--}}
{{--        </div>--}}
    </div>
@endsection

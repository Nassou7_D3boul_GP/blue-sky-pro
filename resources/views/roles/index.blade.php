@extends('layouts.appDash')

@section('content')
    <div class="row">
        <div class="col-lg-12 margin-tb mb-4">
            <div class="pull-left">
                <h5>Role Management
                    <div class="float-end">
{{--                        @can('role-create')--}}
{{--                            <a class="btn btn-success" href="{{ route('roles.create') }}"> Create New Role</a>--}}
                            <a href="{{ route('roles.create') }}" class="btn bg-success-subtle text-success btn-sm" title="View Code"
                                {{--                            data-bs-toggle="modal" data-bs-target="#view-code2-modal"--}}
                            >
                                <iconify-icon icon="solar:add-circle-broken" width="30" height="30"></iconify-icon>
                                Create New Role
                            </a>
{{--                        @endcan--}}
                    </div>
                </h5>
            </div>
        </div>
    </div>

    @if ($message = Session::get('success'))
        <div class="alert alert-success">
            <p>{{ $message }}</p>
        </div>
    @endif

    <table class="table text-nowrap customize-table mb-0 align-middle table-hover">
        <tr>
            <th>
                <h6 class="fs-4 fw-semibold mb-0">Name</h6>
            </th>
            <th width="280px">
                <h6 class="fs-4 fw-semibold mb-0 text-center">Action</h6>
            </th>
        </tr>
        @foreach ($roles as $key => $role)
            <tr>
                <td>{{ $role->name }}</td>
                <td>
                    <form action="{{ route('roles.destroy', $role->id) }}" method="POST">
                        <a class="btn btn-info" href="{{ route('roles.show', $role->id) }}">Show</a>
{{--                        @can('role-edit')--}}
                            <a class="btn btn-primary" href="{{ route('roles.edit', $role->id) }}">Edit</a>
{{--                        @endcan--}}


                        @csrf
                        @method('DELETE')
{{--                        @can('product-delete')--}}
                            <button type="submit" class="btn btn-danger">Delete</button>
{{--                        @endcan--}}
                    </form>
                </td>
            </tr>
        @endforeach
    </table>

    @if(!isset($paginate))
        <div class="d-flex justify-content-center my-4">
            {!! $roles->links() !!}
        </div>
    @endif

{{--    {!! $roles->render() !!}--}}
@endsection

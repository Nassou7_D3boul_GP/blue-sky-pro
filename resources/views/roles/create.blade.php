@extends('layouts.appDash')

@section('content')
    <div class="row">
        <div class="col-lg-12 margin-tb mb-4">
            <div class="pull-left">
                <h2>
                    <div class="float-end">
                        <a class="btn bg-danger-subtle text-danger" href="{{ route('roles.index') }}"> Back</a>
                    </div>
                </h2>
            </div>
        </div>
    </div>

    @if (count($errors) > 0)
        <div class="alert alert-danger">
            <strong>Whoops!</strong> There were some problems with your input.<br><br>
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif

    <form action="{{ route('roles.store') }}" method="POST">
        @csrf




        <div class="card">
            <div class="card-header text-bg-primary">
                <h5 class="mb-0 text-white">Create New Role</h5>
            </div>
            <div class="px-4 py-3 border-bottom">
                <h4 class="card-title mb-0"></h4>
            </div>
            <div class="card-body p-4 border-bottom">
                <div class="row">
                    <div class="col-lg-6 ">
                        <div class="mb-4">
                            <label class="form-label">Name:</label>
                            <div class="input-group">
                                <input type="text" name="name" class="form-control" placeholder="Name">
                            </div>
                        </div>
                        <div>
                            <label class="form-label">Permission:</label>
                            <br>
                                @foreach ($permission as $value)
                                    <label>
                                        <input type="checkbox" name="permission[]" value="{{ $value->name }}" class="name">
                                        {{ $value->name }}</label>
                                    <br />
                                @endforeach
                        </div>
                    </div>
                </div>
                <div class="col-12">
                    <div class="d-flex justify-content-center gap-3">
                        <button class="btn btn-primary" type="submit">Submit</button>
                    </div>
                </div>
            </div>
        </div>

    </form>


@endsection

@extends('layouts.appDash')


@section('content')
    <div class="row">
        <div class="col-lg-12 margin-tb mb-4">
            <div class="pull-left">
                <h2>
                    <div class="float-end">
                        <a class="btn bg-danger-subtle text-danger" href="{{ route('roles.index') }}"> Back</a>
                    </div>
                </h2>
            </div>
        </div>
    </div>

    <div class="card">
        <div class="card-header text-bg-primary">
            <h5 class="mb-0 text-white">View Role</h5>
        </div>
        <form class="form-horizontal">
            <div class="form-body">
                <div class="card-body">
                </div>
                <hr class="m-0">
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group row">
                                <label class="form-label text-end col-md-3"> Name:</label>
                                <div class="col-md-9">
                                    <p class="form-control-static">{{ $role->name }}</p>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group row">
                                <label class="form-label text-end col-md-3">Permission:</label>
                                <div class="col-md-9">
                                    @if (!empty($rolePermissions))
                                        @foreach ($rolePermissions as $v)
                                            <p class="form-control-static">{{ $v->name }},</p>

                                        @endforeach
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
                <hr class="m-0">
                <div class="card-body">
                </div>
            </div>
        </form>
    </div>

@endsection
